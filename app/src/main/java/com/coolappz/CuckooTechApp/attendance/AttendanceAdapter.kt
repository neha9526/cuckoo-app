package com.coolappz.CuckooTechApp.attendance

import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.Toast
import com.coolappz.CuckooTechApp.map.MapsActivity
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.R.id.locationName
import com.coolappz.CuckooTechApp.R.id.showDetailAddress
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.attendanceDatabse.AttendanceEntity
import com.coolappz.CuckooTechApp.profile.ProfileActivity
import com.coolappz.CuckooTechApp.utility.CommonConstants
import com.coolappz.CuckooTechApp.utility.CuckooUtil
import com.coolappz.CuckooTechApp.utility.DateHelper
import kotlinx.android.synthetic.main.adapter_attendance.view.*
import kotlinx.android.synthetic.main.content_dashboard_drawer.*

/**
 * Created by admin on 22-01-2018.
 */
public open class AttendanceAdapter(private var attendanceList: List<AttendanceEntity>,
                                    var context: Context, var appType: String) :
        RecyclerView.Adapter<AttendanceAdapter.MyViewHolder>() {

    private var expandable: Boolean = true
    private var expand: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        val viewHolder = LayoutInflater.from(parent!!.context).inflate(R.layout.adapter_attendance,
                parent, false)
        return MyViewHolder(viewHolder)
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        holder!!.date.text = DateHelper.getFormattedDay(attendanceList[position].punchDateTime)
        holder.month.text = DateHelper.getFormattedMonth(attendanceList[position].punchDateTime)
        holder.year.text = DateHelper.getFormattedYear(attendanceList[position].punchDateTime)
        holder.location.text = attendanceList[position].locationName
        holder.time.text = DateHelper.getFormattedTime(attendanceList[position].punchDateTime)
        val latString: String = String.format("%.6f", attendanceList[position].latitude)
        holder.latitude.text = latString
        val longString: String = String.format("%.6f", attendanceList[position].longitude)
        holder.longitude.text = longString
        holder.status.text = attendanceList[position].syncStatus


        if (attendanceList[position].isValid == 1) {
            holder.status.setTextColor(context.resources.getColor(R.color.green))
        } else {
            holder.status.setTextColor(context.resources.getColor(R.color.red))
        }
        if (attendanceList[position].empImage != "") {
            holder.profileIcon.visibility = View.VISIBLE
        } else {
            holder.profileIcon.visibility = View.GONE
        }
        holder.profileIcon.setOnClickListener()
        {
            val intent = Intent(context, ProfileActivity::class.java)
            intent.putExtra("EncodedImage", attendanceList[position].empImage)
            context.startActivity(intent)
        }

        holder.locationIcon.setOnClickListener()
        {
            if (!CuckooUtil.Util.isNetworkAvailable(context)) {
                Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show()
            } else {
                val intent = Intent(context, MapsActivity::class.java)
                intent.putExtra("Latitude", attendanceList[position].latitude)
                intent.putExtra("Longitude", attendanceList[position].longitude)
                context.startActivity(intent)
            }

        }

        var toggleText = false

        holder.location.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                holder.location.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val lineCount = holder.location.lineCount
                Log.i("TAG", "Using holder line count ==> " + lineCount)
                val count = attendanceList[position].locationName
                Log.i("TAG", "Using position line count ==>" + count)
                if (lineCount < 3) {
                    holder.showDetailAddress.visibility = View.GONE
                } else {
                    holder.showDetailAddress.visibility = View.VISIBLE
                }
            }
        })

        // holder.location.setOnClickListener(null)

        holder.showDetailAddress.setOnClickListener {
            if (toggleText) {
                holder.location.maxLines = 2
                holder.showDetailAddress.text = "show more"
                toggleText = false
            } else {
                holder.location.maxLines = 10
                holder.showDetailAddress.text = "show less"
                toggleText = true
            }
        }
    }


    override fun getItemCount(): Int {
        return attendanceList.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var date = itemView.tvDate
        var month = itemView.tvMonth
        var year = itemView.tvYear
        var time = itemView.time
        var location = itemView.locationName
        var status = itemView.status
        var locationIcon = itemView.mapIcon
        var latitude = itemView.latitude
        var longitude = itemView.longitude
        var profileIcon = itemView.profileView
        var showDetailAddress = itemView.showDetailAddress
    }
}