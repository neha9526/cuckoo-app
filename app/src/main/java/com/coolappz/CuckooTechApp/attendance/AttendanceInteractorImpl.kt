package com.coolappz.CuckooTechApp.attendance

import android.content.Context
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.network.Api
import com.coolappz.CuckooTechApp.network.ApiFail
import com.coolappz.CuckooTechApp.network.ApiSuccess
import com.coolappz.CuckooTechApp.network.HttpErrorResponse
import com.coolappz.CuckooTechApp.network.requestpojo.BulkSyncRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.BulkAttendanceResponse
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.BulkSyncResponse
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by admin on 10-03-2018.
 */
class AttendanceInteractorImpl: AttendanceInteractor {
    override fun bulkPunchSync(context: Context, bulkSyncRequestPojo: BulkSyncRequestPojo,
                               interfaceInteractor: InterfaceInteractor<BulkAttendanceResponse>) {
        Api.userManagement().employeeBulkPunch(bulkSyncRequestPojo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiSuccess<BulkSyncResponse>() {
                    override fun call(response:BulkSyncResponse) {
                        when {
                            response.response!!.isSucceeded!! -> {
                                interfaceInteractor.successApi(response.response)

                            }
                            else -> {
                                interfaceInteractor.failApi(response.response.responseMessage!!)
                            }
                        }
                    }

                }, object : ApiFail() {
                    override fun noNetworkError() {
                        interfaceInteractor.noInternet()
                    }

                    override fun unknownError(e: Throwable) {
                        interfaceInteractor.failApi(context.getString(R.string.unknown_error_msg))
                    }

                    override fun httpStatus(response: HttpErrorResponse) {
                        interfaceInteractor.failApi(context.getString(R.string.http_status_error_msg))
                    }

                })
    }
}