package com.coolappz.CuckooTechApp.attendance

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.BulkSyncRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.BulkAttendanceResponse
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor

/**
 * Created by admin on 10-03-2018.
 */
interface AttendanceInteractor {
    fun bulkPunchSync(context: Context,bulkSyncRequestPojo: BulkSyncRequestPojo,
                      interfaceInteractor:InterfaceInteractor<BulkAttendanceResponse>)
}