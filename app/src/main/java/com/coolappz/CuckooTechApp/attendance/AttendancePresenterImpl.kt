package com.coolappz.CuckooTechApp.attendance

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.BulkSyncRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.BulkAttendanceResponse
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter

/**
 * Created by admin on 07-02-2018.
 */
class AttendancePresenterImpl(var view: AttendanceView<BulkAttendanceResponse>?):
        MvpBasePresenter<AttendanceView<BulkAttendanceResponse>>(), AttendancePresenter,InterfaceInteractor<BulkAttendanceResponse> {

    private  var attendanceInteractor=AttendanceInteractorImpl()

    override fun onBulkPunchSync(context: Context, bulkSyncRequestPojo: BulkSyncRequestPojo) {
        if(view!=null)
        {
            view!!.showLoading()
            attendanceInteractor.bulkPunchSync(context,bulkSyncRequestPojo,this)
        }
    }



    override fun onDestroyView() {
        view=null
    }


    override fun successApi(response: BulkAttendanceResponse) {
        if(view!=null)
        {
            view?.hideLoading()
            view?.success(response)
        }
    }

    override fun failApi(message: String) {
        if(view!=null)
        {
            view?.hideLoading()
            view?.fail(message)
        }
    }

    override fun noInternet() {
        if(view!=null)
        {
            view?.hideLoading()
            view?.noInternet()
        }
    }

    override fun tokenExpire() {}
}
