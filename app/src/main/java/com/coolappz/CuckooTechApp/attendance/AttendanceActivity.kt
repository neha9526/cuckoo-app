package com.coolappz.CuckooTechApp.attendance

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.about_us.AboutUSActivity
import com.coolappz.CuckooTechApp.dashboard.DashboardDrawerActivity
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.attendanceDatabse.AttendanceEntity
import com.coolappz.CuckooTechApp.data_base.loginDatabase.LoginDetails
import com.coolappz.CuckooTechApp.faq.FaqActivity
import com.coolappz.CuckooTechApp.network.requestpojo.BulkSyncRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.BulkAttendanceResponse
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.BulkPunchPojo
import com.coolappz.CuckooTechApp.utility.*
import com.coolappz.CuckooTechApp.utility.CuckooUtil.Util.isNetworkAvailable
import kotlinx.android.synthetic.main.activity_base_app_compat.*
import kotlinx.android.synthetic.main.layout_attendance.*
import java.util.*
import kotlin.collections.ArrayList

class AttendanceActivity : BaseAppCompatActivity<AttendanceView<BulkAttendanceResponse>, AttendancePresenterImpl>(),
        AttendanceView<BulkAttendanceResponse> {

    override val layoutIdForInjection: Int = R.layout.layout_attendance

    override fun createPresenter(): AttendancePresenterImpl {
        presenter = AttendancePresenterImpl(this)
        return presenter
    }

    private var fromStr: String = ""
    private var toStr: String = ""
    private var toMindate: Calendar? = null
    private var fromMaxdate: Calendar? = null
    private var attendanceList: List<AttendanceEntity> = ArrayList()
    private var sortedAttendanceList: List<AttendanceEntity> = ArrayList()
    private lateinit var syncPunchList: List<AttendanceEntity>
    private var bulkPunchList: ArrayList<BulkPunchPojo> = ArrayList()
    private lateinit var loginDetails: LoginDetails
    private val databaseHandler = DatabaseHandler(this)
    private lateinit var recyclerView: RecyclerView
    private lateinit var fromAttendance: TextView
    private lateinit var toAttendance: TextView
    private var lastRecordDate: String? = ""
    private var recentRecordDate: String? = ""
    private var punchListSize: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val mMonth = DateHelper.getMonth(DateHelper.todayCalender.timeInMillis).toInt() - 1
        val mYear = DateHelper.getYear(DateHelper.todayCalender.timeInMillis).toInt()
        val mDay = DateHelper.getDay(DateHelper.todayCalender.timeInMillis).toInt()
        syncPunchList = ArrayList()
        loginDetails = databaseHandler.fetchFromDatabase()!!
        screenSubTitle.text = getString(R.string.attendance_list)
        fromAttendance = findViewById(R.id.attendanceFrom) as TextView
        toAttendance = findViewById(R.id.attendanceTo) as TextView
        attendanceList = databaseHandler.getAllAttendanceDetails()
        syncPunchList = databaseHandler.getBulkPunchDetails()
        totalCount.text = databaseHandler.getAllAttendanceDetails().size.toString()
        validCount.text = databaseHandler.getCountOfValid().toString()
        invalidCount.text = databaseHandler.getCountOfInvalid().toString()
        offlineCount.text = databaseHandler.getCountOfOffline().toString()

        if (syncPunchList.isNotEmpty()) {
            syncImg.visibility = View.VISIBLE
            sycStatus.visibility = View.VISIBLE
        } else {
            baseEmpCodeLayout.visibility = View.VISIBLE
            baseEmpCode.text = loginDetails.employeeCode
        }

        if (databaseHandler.getMaxDate() != null) {
            lastRecordDate = DateHelper.dateFilterFormat(databaseHandler.getMinDate().punchDateTime)
            fromStr = databaseHandler.getMinDate().punchDateTime
            recentRecordDate = DateHelper.dateFilterFormat(databaseHandler.getMaxDate().punchDateTime)
            toStr = databaseHandler.getMaxDate().punchDateTime
        }

        if (fromAttendance.text.isNullOrEmpty() || toAttendance.text.isNullOrEmpty()) {
            fromAttendance.text = lastRecordDate
            toAttendance.text = recentRecordDate
        }

        attendanceGo.setOnClickListener()
        {
            if (attendanceFrom.text.isNotEmpty() && attendanceTo.text.isNotEmpty()) {
                sortedAttendanceList = databaseHandler.sortByGivenDate(fromStr, toStr)
                if (sortedAttendanceList.isNotEmpty()) {
                    recyclerView.adapter = AttendanceAdapter(sortedAttendanceList, this, loginDetails.appType!!)
                    Toast.makeText(this, getString(R.string.attendance_list_updated), Toast.LENGTH_SHORT).show()
                } else {
                    recyclerView.adapter = AttendanceAdapter(sortedAttendanceList, this, loginDetails.appType!!)
                    Toast.makeText(this, getString(R.string.data_not_available_for_dates), Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, getString(R.string.enter_from_to), Toast.LENGTH_SHORT).show()
            }
        }

        syncImg.setOnClickListener()
        {
            var user = databaseHandler.fetchFromDatabase()!!
            val appType = user.appType.toString()
            if (isNetworkAvailable(this)) {
                syncPunchList = databaseHandler.getBulkPunchDetails()
                punchListSize = syncPunchList.size
                if (syncPunchList.isNotEmpty()) {
                    showLoading()
                    for ((index, item) in syncPunchList.withIndex()) {
                        bulkPunchList.add(BulkPunchPojo(syncPunchList[index].punchDateTime,
                                syncPunchList[index].latitude,
                                syncPunchList[index].longitude,
                                syncPunchList[index].empImage,
                                syncPunchList[index].isValid,
                                syncPunchList[index].syncStatus,
                                syncPunchList[index].attendanceId,
                                syncPunchList[index].locationName))
                        if (index == 90) {
                            break
                        }
                    }
                    val bulkSyncRequestPojo = BulkSyncRequestPojo(loginDetails.employeeCode, loginDetails.companyCode,
                            loginDetails.mobileNo, loginDetails.macAddress!!,
                            appType, loginDetails.tokenNumber!!,
                            bulkPunchList)
                    presenter.onBulkPunchSync(this, bulkSyncRequestPojo)
                } else {
                    Toast.makeText(this, getString(R.string.data_sync_already), Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, getString(R.string.need_connection_for_sync),
                        Toast.LENGTH_SHORT).show()
            }
        }


        recyclerView = attendanceRecyclerView
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.reverseLayout = true
        linearLayoutManager.stackFromEnd = true
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = AttendanceAdapter(attendanceList, this, loginDetails.appType!!)

        checkIn.setOnClickListener {
            val intent = Intent(this, DashboardDrawerActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }
        attendance_aboutUs.setOnClickListener {
            val intent = Intent(this, AboutUSActivity::class.java)
            startActivity(intent)
        }
        attendance_faq.setOnClickListener {
            val intent = Intent(this, FaqActivity::class.java)
            startActivity(intent)
        }

        attendanceFrom.setOnClickListener {
            if (!attendanceTo.text.isNullOrEmpty()) {
                fromMaxdate = DateHelper.getDateCalender(toStr)
            }
            DateDialogFragment(getString(R.string.select_date), mYear, mMonth, mDay,
                    fromMaxdate, null,
                    object : InterfaceOnDateClick {
                        override fun onDateClick(date: Int, month: Int, year: Int) {
                            fromStr = DateHelper.getYearMonthDateFormat(month, year, date) + " " + Constants.Constants.AM_MIN
                            attendanceFrom.text = DateHelper.getDateFilterFormat(month, year, date)
                        }
                    }).show(supportFragmentManager, "DatePicker")
        }

        attendanceTo.setOnClickListener {
            if (!attendanceFrom.text.isNullOrEmpty()) {
                toMindate = DateHelper.getDateCalender(fromStr)
            }
            DateDialogFragment(getString(R.string.select_date), mYear, mMonth, mDay,
                    DateHelper.todayCalender, toMindate,
                    object : InterfaceOnDateClick {
                        override fun onDateClick(date: Int, month: Int, year: Int) {
                            toStr = DateHelper.getYearMonthDateFormat(month, year, date) + " " + Constants.Constants.PM_MAX
                            attendanceTo.text = DateHelper.getDateFilterFormat(month, year, date)
                        }
                    }).show(supportFragmentManager, "DatePicker")

        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }


    override fun noInternet() {
        Toast.makeText(this, getString(R.string.poor_net_connection), Toast.LENGTH_SHORT).show()
    }

    fun syncAgain() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(this.getString(R.string.sync_again))
        builder.setCancelable(true)
        builder.setPositiveButton("Ok") { dialogInterface, i
            ->
            dialogInterface.dismiss()
        }
        val alert = builder.create()
        if (!alert.isShowing)
            alert.show()
    }

    override fun fail(message: String?) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, getString(R.string.punch_serevr_error), Toast.LENGTH_LONG).show()
        }
    }

    override fun showLoading() {
        progressLayout.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    override fun hideLoading() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        progressLayout.visibility = View.GONE
    }

    override fun success(response: BulkAttendanceResponse?) {
        var user = databaseHandler.fetchFromDatabase()!!
        val appType = user.appType.toString()
        if (response!!.appType != null)
            if (appType != response.appType.toString()) {
                Log.e("TAG", "Response app type " + response.appType.toString())
                databaseHandler.updateAppType(response.appType.toString(), loginDetails.employeeCode!!)
            }
        databaseHandler.updateSyncStatus(response!!.bulkEmployeePunch!!)
        attendanceList = databaseHandler.getAllAttendanceDetails()
        recyclerView.adapter = AttendanceAdapter(attendanceList, this, loginDetails.appType!!)
        offlineCount.text = databaseHandler.getCountOfOffline().toString()
        validCount.text = databaseHandler.getCountOfValid().toString()
        invalidCount.text = databaseHandler.getCountOfInvalid().toString()
        attendanceTo.text = recentRecordDate
        attendanceFrom.text = lastRecordDate
        Toast.makeText(this, response.responseMessage, Toast.LENGTH_LONG).show()
        if (punchListSize > 90) {
            syncAgain()
        } else {
            syncImg.visibility = View.GONE
            sycStatus.visibility = View.GONE
            baseEmpCodeLayout.visibility = View.VISIBLE
            baseEmpCode.text = loginDetails.employeeCode
        }
    }
}