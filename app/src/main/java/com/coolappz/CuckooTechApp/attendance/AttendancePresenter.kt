package com.coolappz.CuckooTechApp.attendance

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.BulkSyncRequestPojo

/**
 * Created by admin on 07-02-2018.
 */
interface AttendancePresenter {
    fun onBulkPunchSync(context: Context,bulkSyncRequestPojo: BulkSyncRequestPojo)

    fun onDestroyView()
}