package com.coolappz.CuckooTechApp.cryptographic;

import android.util.Log;

import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class TrippleDe {
    //    public static String ALGO = "DESede/CBC/PKCS7Padding";
       private static String encryptionAlgo= "DESede/ECB/PKCS5Padding";
        String aesEncryptionAlgorithm = "DESede";
        private final String characterEncoding = "UTF-8";
        private final String encryptionKey = "cryptapikey";

    public  String _encrypt(String message) throws Exception {

        Cipher cipher = Cipher.getInstance(encryptionAlgo);
        cipher.init(Cipher.ENCRYPT_MODE, getSecreteKey(encryptionKey));

        byte[] plainTextBytes = message.getBytes(characterEncoding);
        byte[] buf = cipher.doFinal(plainTextBytes);
        byte[] base64Bytes =  android.util.Base64.encode(buf,  android.util.Base64.DEFAULT);
        String base64EncryptedString = new String(base64Bytes,characterEncoding);
        Log.e("encrypted pass",base64EncryptedString);
        return base64EncryptedString.replace("\n","");
    }

    public String _decrypt(String encryptedText, String secretKey) throws Exception {

        byte[] message =  android.util.Base64.decode(encryptedText.getBytes(),  android.util.Base64.DEFAULT);
        Cipher decipher = Cipher.getInstance(encryptionAlgo);
        decipher.init(Cipher.DECRYPT_MODE, getSecreteKey(secretKey));
        byte[] plainText = decipher.doFinal(message);

        return new String(plainText, characterEncoding);
    }

    private SecretKey getSecreteKey(String secretKey) throws Exception {
        MessageDigest md = MessageDigest.getInstance("md5");
        byte[] digestOfPassword = md.digest(secretKey.getBytes(characterEncoding));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        for (int j = 0, k = 16; j < 8;) {
            keyBytes[k++] = keyBytes[j++];
        }
        SecretKey key = new SecretKeySpec(keyBytes, aesEncryptionAlgorithm);
        return key;
    }
}