package com.coolappz.CuckooTechApp.cryptographic;

import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Nivrutti Pawar on 2018/03/17.
 */

public class TrippleDes {
    private final String characterEncoding = "UTF-8";
    private final String key = "cryptapikey";


    public String encrypt(String message) throws Exception {
        String aesEncryptionAlgorithm = "DESede";
        String cipherTransformation = "DESede/ECB/PKCS5Padding";
        final MessageDigest md = MessageDigest.getInstance("md5");
        final byte[] digestOfPassword = md.digest((key.getBytes(characterEncoding)));
        final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

        for (int j = 0, k = 16; j < 8;) {
            keyBytes[k++] = keyBytes[j++];
        }


        final SecretKey key = new SecretKeySpec(keyBytes,0,24, aesEncryptionAlgorithm);
        final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
        final Cipher cipher = Cipher.getInstance(cipherTransformation);
        cipher.init(Cipher.ENCRYPT_MODE, key, iv);
        final byte[] plainTextBytes = message.getBytes(characterEncoding);
        final byte[] cipherText = cipher.doFinal(plainTextBytes);


        byte[] base64Bytes =  android.util.Base64.encode(cipherText,  android.util.Base64.DEFAULT);
        String base64EncryptedString = new String(base64Bytes);
        return base64EncryptedString;

    }

    public String decrypt(byte[] message) throws Exception {
        final MessageDigest md = MessageDigest.getInstance("md5");
        final byte[] digestOfPassword = md.digest("cryptapikey"
                .getBytes(characterEncoding));
        final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        for (int j = 0, k = 16; j < 8;) {
            keyBytes[k++] = keyBytes[j++];
        }

        final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
        final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
        final Cipher decipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        decipher.init(Cipher.DECRYPT_MODE, key, iv);
        final byte[] plainText = decipher.doFinal(message);

        return new String(plainText, characterEncoding);
    }

}
