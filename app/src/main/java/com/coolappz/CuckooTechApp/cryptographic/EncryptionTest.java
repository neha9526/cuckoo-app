package com.coolappz.CuckooTechApp.cryptographic;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

/**
 * Created by admin on 03-04-2018.
 */

public class EncryptionTest {
    static IvParameterSpec iv;
     String key="cryptapikey";
    public  String encrypt(String toEncrypt) throws Exception {
        // create a binary key from the argument key (seed)
        SecureRandom sr = new SecureRandom(key.getBytes());
        KeyGenerator kg = KeyGenerator.getInstance("DES");
        kg.init(sr);
        SecretKey sk = kg.generateKey();

        // create an instance of cipher
        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");

        // generate an initialization vector (IV)
        SecureRandom secureRandom = new SecureRandom();
        byte[] ivspec = new byte[cipher.getBlockSize()];
        secureRandom.nextBytes(ivspec);
        iv = new IvParameterSpec(ivspec);

        // initialize the cipher with the key and IV
        cipher.init(Cipher.ENCRYPT_MODE, sk, iv);

        // enctypt!
        byte[] encrypted = cipher.doFinal(toEncrypt.getBytes());
        final String encryptedText=Base64.encode(encrypted);
        return encryptedText;
    }
}
