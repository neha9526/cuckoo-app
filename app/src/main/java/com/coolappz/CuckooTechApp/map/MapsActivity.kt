package com.coolappz.CuckooTechApp.map


import android.annotation.SuppressLint
import android.content.Intent
import android.location.Geocoder
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.Toast
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.dashboard.FetchAddressIntentService
import com.coolappz.CuckooTechApp.utility.Constants
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_maps.*


class MapsActivity : AppCompatActivity(),
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{
    override fun onConnected(p0: Bundle?) {}

    override fun onConnectionSuspended(p0: Int) {}

    override fun onConnectionFailed(p0: ConnectionResult) {}

    private  var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private lateinit var mMap: GoogleMap
    private lateinit var mapToolbar:Toolbar
    private lateinit var resultReceiver: AddressResultReceiver
    private val handler: Handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        mapToolbar=findViewById(R.id.mapToolbar) as Toolbar
        setSupportActionBar(mapToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        mapToolbar.setNavigationOnClickListener {
            finish()
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        resultReceiver = AddressResultReceiver(handler)
        val intent= intent
        latitude=intent.getDoubleExtra("Latitude", 0.0)
        longitude=intent.getDoubleExtra("Longitude", 0.0)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fetchAddressButtonHander(latitude,longitude)

    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val location = LatLng(latitude,longitude)
        mMap.addMarker(MarkerOptions().position(location)).showInfoWindow()
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location,16.0f))
        mMap.setMinZoomPreference(6.0f);
        mMap.setMaxZoomPreference(21.0f);
       // mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location),1)
    }
    @SuppressLint("MissingPermission")
    private fun fetchAddressButtonHander(lat:Double,long:Double) {
        if (!Geocoder.isPresent()) {
            Log.e("GeoCoder==>", "Not present")
            Toast.makeText(this,
                    R.string.no_geocoder_available,
                    Toast.LENGTH_LONG).show()
            return
        } else if (lat!=0.0 && long!=0.0) {
            startIntentService()
        }

    }
    private fun startIntentService() {
        Log.e("StartIntentService", "Started")
        val intent = Intent(this, FetchAddressIntentService::class.java).apply {
            putExtra(Constants.Constants.RECEIVER, resultReceiver)
            putExtra(Constants.Constants.LOCATION_LATTITUDE, latitude)
            putExtra(Constants.Constants.LOCATION_LONGITUDE,longitude)
        }
        startService(intent)
    }
    internal inner class AddressResultReceiver(handler: Handler) : ResultReceiver(handler) {
        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {

            val addressOutput = resultData?.getString(Constants.Constants.RESULT_DATA_KEY) ?: ""
            Log.e("Received Output====>", addressOutput)
            mapLocationName.text=addressOutput
        }
    }
}
