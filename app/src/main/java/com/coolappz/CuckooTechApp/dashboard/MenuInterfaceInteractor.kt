package com.coolappz.CuckooTechApp.dashboard

import com.coolappz.CuckooTechApp.network.responsepojo.MenuResponse

interface MenuInterfaceInteractor {

    fun onFetchedMenuDetails(menuResponse: MenuResponse)
}