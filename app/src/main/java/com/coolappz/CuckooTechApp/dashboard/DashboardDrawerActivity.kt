package com.coolappz.CuckooTechApp.dashboard

import android.Manifest
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Point
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatButton
import android.util.Base64
import android.util.Log
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.about_us.AboutUSActivity
import com.coolappz.CuckooTechApp.attendance.AttendanceActivity
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.attendanceDatabse.AttendanceEntity
import com.coolappz.CuckooTechApp.data_base.loginDatabase.LoginDetails
import com.coolappz.CuckooTechApp.data_base.menu_master.MenuEntity
import com.coolappz.CuckooTechApp.expense.claim_expense.ExpenseActivity
import com.coolappz.CuckooTechApp.faq.FaqActivity
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeAttendanceRequest
import com.coolappz.CuckooTechApp.network.requestpojo.MenuRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.MenuResponse
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.SingleAttendanceResponse
import com.coolappz.CuckooTechApp.utility.*
import com.coolappz.CuckooTechApp.utility.CommonConstants.utility.APP_TYPE_1
import com.coolappz.CuckooTechApp.utility.CommonConstants.utility.APP_TYPE_2
import com.coolappz.CuckooTechApp.utility.CommonConstants.utility.isAutomaticTimeZoneEnable
import com.coolappz.CuckooTechApp.utility.CommonConstants.utility.isTimeAutomatic
import com.coolappz.CuckooTechApp.utility.CuckooUtil.Util.isNetworkAvailable
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import kotlinx.android.synthetic.main.activity_dashboard_drawer.*
import kotlinx.android.synthetic.main.app_bar_dashboard_drawer.*
import kotlinx.android.synthetic.main.content_dashboard_drawer.*
import kotlinx.android.synthetic.main.custom_tab.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.io.ByteArrayOutputStream
import java.util.*


class DashboardDrawerActivity : MvpActivity<DashboardView<SingleAttendanceResponse>, DashboardPresenterImpl>(),
        DashboardView<SingleAttendanceResponse>,
        NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private val CAMERA_REQUEST = 200
    private val REQUEST_ID_MULTIPLE_PERMISSIONS = 1
    private var latitude: Double? = null
    private var longitude: Double? = null
    private var location: Location? = null
    private var isGPSEnabled: Boolean = false
    private var isNetworkEnabled: Boolean = false
    private lateinit var locationManager: LocationManager
    private lateinit var databaseHandler: DatabaseHandler
    private var addressLine: String = ""
    private lateinit var loginDetails: LoginDetails
    private var encodedImage: String = ""
    private lateinit var popUps: PopUps
    private lateinit var appType: String
    private val listPermissionsNeeded = ArrayList<String>()
    private var headerView: View? = null
    private lateinit var relativeLayout: RelativeLayout
    private var lastRecordDate: String? = null
    private var lastPunchStatus: String? = ""
    private val INTERVAL = (1000 * 10).toLong()
    private val FASTEST_INTERVAL = (1000 * 5).toLong()
    private lateinit var mLocationRequest: LocationRequest
    private lateinit var mGoogleApiClient: GoogleApiClient
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private var moodLayout: RelativeLayout? = null
    private var attendanceLayout: RelativeLayout? = null
    private var expenseLayout: RelativeLayout? = null
    private var attendanceLog: AppCompatButton? = null
    private var expenseLog: AppCompatButton? = null
    private var expandable: Boolean = true
    private var expand: Boolean = false
    private var TAG: String = "DashboardDrawerActivity"

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        Log.i(TAG, "onConnected called")
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallBack(), null)
    }


    override fun onConnectionSuspended(p0: Int) {
        Log.i(TAG, "onConnectionSuspended called")
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.i(TAG, "onConnectionFailed called")
    }

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG, "onCreate called")
        setContentView(R.layout.activity_dashboard_drawer)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        dashboardTitle.text = getString(R.string.cuckoo_title)
        empCodeLayout.visibility = VISIBLE
        markAttendance.isEnabled = false
        popUps = PopUps(this)
        databaseHandler = DatabaseHandler(this)
        loginDetails = databaseHandler.fetchFromDatabase()!!
        headerEmployeeCode.text = loginDetails.employeeCode
        appType = loginDetails.appType!!
        relativeLayout = findViewById(R.id.dashboardRelativeLayout) as RelativeLayout


        locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        listPermissionsNeeded.add(Manifest.permission.CAMERA)

        // Google client set up
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
        createLocationRequest()


        //General set up of version name,dashboard drawer layout etc
        tvVersionName.text = getString(R.string.version_name) + " " + CuckooUtil.Util.getVersionName(this)
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        databaseHandler.deleteAttendance()
//        updateLastMark()


//      Initial set up of Tablayout for menus
        menuSetUp()
        Log.e("TAG", "Menu List " + databaseHandler.fetchMenuDetails())
        tabMenuUpdate(databaseHandler.fetchMenuDetails())
        defaultMood.setImageResource(Preferences.getEmojiName(applicationContext))

        //mood layout listener setup
        moodLayout!!.setOnClickListener {
            emojiPopUp()
        }
        expenseLog!!.setOnClickListener()
        {
            val intent = Intent(this, ExpenseActivity::class.java)
            startActivity(intent)
        }

        attendanceLog!!.setOnClickListener()
        {
            val intent = Intent(this, AttendanceActivity::class.java)
            startActivity(intent)
        }
        markAttendance.setOnClickListener { it ->
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (!CommonConstants.utility.isSimSupport(this) && !isNetworkAvailable(this)) {
                popUps.enableSimCard(this)
            } else if (!isGPSEnabled && !isNetworkEnabled) {
                popUps.displayLocationSettingsRequest(this)
            } else {
                // Using local appType and get updated appType from database. When user do Sync function and if change there menuList.
                var user = databaseHandler.fetchFromDatabase()!!
                val appType = user.appType.toString()
                Log.e("TAG", "2. App Type when user click on mark attendence ==> " + appType)
                Log.i(TAG, "Lat in setOnClickListener ==>" + latitude)
                Log.i(TAG, "Lat in setOnClickListener ==>" + longitude)

                // Below get location lat lon code suggested by Kaniya
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    LocationServices.getFusedLocationProviderClient(this).lastLocation
                            .addOnSuccessListener {
                                if (it != null) {
                                    latitude = it.latitude
                                    longitude = it.longitude
                                    Log.i(TAG, "latitude new way ==>" + latitude)
                                    Log.i(TAG, "longitude new way ==>" + longitude)
                                    val geocoder = Geocoder(applicationContext, Locale.getDefault())
                                    val addresses: List<Address>
                                    try {
                                        addresses = geocoder.getFromLocation(latitude!!, longitude!!, 1)
                                        addressLine = addresses[0].getAddressLine(0)
                                        Log.i(TAG, "Address new way ==>" + addressLine)
                                    } catch (e: Exception) {
                                    }
                                    onMarkAttendance(listPermissionsNeeded, appType)
                                }
                            }
                } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                    ActivityCompat.requestPermissions(this, listPermissionsNeeded.toTypedArray
                    (), REQUEST_ID_MULTIPLE_PERMISSIONS)
                } else {
                    popUps.createGpsCameraAlert(this)
                }
            }
        }

        syncTab.setOnClickListener()
        {
            presenter.onMenuSyncRequest(this, MenuRequestPojo(loginDetails.employeeCode, loginDetails.companyCode))
        }
        aboutUsTab.setOnClickListener()
        {
            val intent = Intent(this, AboutUSActivity::class.java)
            startActivity(intent)
        }
        faqDashboardTab.setOnClickListener() {
            val intent = Intent(this, FaqActivity::class.java)
            startActivity(intent)
        }


        nav_view.setNavigationItemSelectedListener(this)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS && grantResults.isNotEmpty()) {
            val cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED
            val coarsePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED
            val finePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED

            if (cameraPermission && coarsePermission && finePermission) {
                Log.i(TAG, "Permission granted called")
                mGoogleApiClient.connect()
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toTypedArray
                (), REQUEST_ID_MULTIPLE_PERMISSIONS)
            } else {
                popUps.createGpsCameraAlert(this)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "onResume called")
        updateLastMark()
        if (!isTimeAutomatic(this) || !isAutomaticTimeZoneEnable(this)) {
            popUps.autoTimePopUp(this)
        }
    }

    override fun onPause() {
        super.onPause()
        Log.i(TAG, "onPause called")
        if (!mGoogleApiClient.isConnected) {
            mGoogleApiClient.connect()
        }
    }


    @SuppressLint("MissingPermission")
    override fun onStart() {
        super.onStart()
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        if (!hasPermissions(listPermissionsNeeded)) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toTypedArray(), REQUEST_ID_MULTIPLE_PERMISSIONS)
        } else if (!isGPSEnabled && !isNetworkEnabled) {
            popUps.displayLocationSettingsRequest(this)
        } else {
            Log.i(TAG, "mGoogleApiClient connection has been called")
            mGoogleApiClient.connect()
        }

    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, "onStop called")
        mFusedLocationClient.removeLocationUpdates(mLocationCallBack())
        mGoogleApiClient.disconnect()

    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            finish()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home_menu -> {
            }
            R.id.attendance_menu -> {
                val intent = Intent(this, AttendanceActivity::class.java)
                startActivity(intent)
            }
            R.id.claim_menu -> {
                val intent = Intent(this, ExpenseActivity::class.java)
                startActivity(intent)
            }
            R.id.faq_menu -> {
                val intent = Intent(this, FaqActivity::class.java)
                startActivity(intent)
            }
            R.id.about_us_menu -> {
                val intent = Intent(this, AboutUSActivity::class.java)
                startActivity(intent)
            }

        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    override fun noInternet() {
        Toast.makeText(this, getString(R.string.poor_net_connection), Toast.LENGTH_LONG).show()
        Log.i(TAG, "Inside no internet connection")
        offlinePunch()
    }

    override fun fail(message: String?) {
        offlinePunch()
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, getString(R.string.punch_serevr_error), Toast.LENGTH_LONG).show()
        }
    }


    override fun success(response: SingleAttendanceResponse) {
        showSnackbar(response.responseMessage!!)
        var user = databaseHandler.fetchFromDatabase()!!
        val appType = user.appType.toString()
        if (response.appType != null)
            if (appType != response.appType.toString()) {
                Log.e("TAG", "Response app type " + response.appType.toString())
                databaseHandler.updateAppType(response.appType.toString(), loginDetails.employeeCode!!)
            }
        databaseHandler.addAttendanceDetails(AttendanceEntity(DateHelper.getDateTimeNew(System.currentTimeMillis()),
                addressLine, longitude!!, latitude!!, response.syncStatus!!, encodedImage, response.isValid!!))
        updateLastMark()

    }


    override fun onMenuSyncSuccessful(response: MenuResponse) {
        Toast.makeText(this, getString(R.string.sync_success), Toast.LENGTH_SHORT).show()
        for (menu in response.response!!.menuList!!) {
            if (databaseHandler.fetchMenus().contains(menu.moduleName)) {
                databaseHandler.updateMenuFlag(menu.moduleName!!, menu.isActive!!)
            } else {
                databaseHandler.insertMenuDetails(MenuEntity(menu.moduleName!!, menu.isActive!!))
            }
        }
        menuTabLayout.removeAllTabs()
        tabMenuUpdate(databaseHandler.fetchMenuDetails())
        // Update appType when user got updated menuList.
        var user = databaseHandler.fetchFromDatabase()!!
        val appType = user.appType.toString()
        Log.e("TAG", "1. App Type when user click sync ==> " + appType)
        if (appType != response.response.appType.toString()) {
            Log.e("TAG", "Response app type " + response.response.appType.toString())
            databaseHandler.updateAppType(response.response.appType.toString(), loginDetails.employeeCode!!)
            //  appType = loginDetails.appType!!
        }
    }

    override fun showLoading() {
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        punchingProgressBar.visibility = VISIBLE
    }


    override fun hideLoading() {
        punchingProgressBar.visibility = GONE
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

    }

    override fun createPresenter(): DashboardPresenterImpl {
        presenter = DashboardPresenterImpl(this)
        return presenter
    }

    private fun punchDetails() {
        // Using local appType and get updated appType from database when user do Sync function.
        var user = databaseHandler.fetchFromDatabase()!!
        val appType = user.appType.toString()
        Log.e("TAG", "3. App Type when user submit punchDetails ==> " + appType)
        if (appType == APP_TYPE_1 || appType == APP_TYPE_2) {
            encodedImage = ""
        }
        if (isNetworkAvailable(this)) {
            // add condition latitude and longitude not null and not zero
            // if lat long not available then show user fetching location please wait
            if (latitude != null && longitude != null && !latitude!!.equals(0.toDouble()) && !longitude!!.equals(0.toDouble())) {
                val employeeAttendanceRequest = EmployeeAttendanceRequest(loginDetails.employeeCode,
                        loginDetails.companyCode, loginDetails.mobileNo, DateHelper.getDateTime(System.currentTimeMillis()),
                        loginDetails.macAddress, latitude.toString(), longitude.toString(), encodedImage,
                        databaseHandler.fetchFromDatabase()!!.tokenNumber,
                        appType, 0, addressLine)

                presenter.onPunchRequest(this, employeeAttendanceRequest)
            } else {
                Log.i(TAG, "No location found in punchDetails")
                Toast.makeText(this, R.string.try_later, Toast.LENGTH_SHORT).show()
            }

        } else {
            // add condition latitude and longitude not null and not zero
            // if lat long not available then show user fetching location please wait
            offlinePunch()
        }

    }

    private fun offlinePunch() {
        if (latitude != null && longitude != null && !latitude!!.equals(0.toDouble()) && !longitude!!.equals(0.toDouble())) {
            hideLoading()
            databaseHandler.addAttendanceDetails(AttendanceEntity(DateHelper.getDateTimeNew(System.currentTimeMillis()),
                    addressLine, longitude!!, latitude!!, getString(R.string.unsync), encodedImage, -1))
            val responseSnackbar = Snackbar.make(relativeLayout, getString(R.string.punch_successful), Snackbar.LENGTH_INDEFINITE)
            responseSnackbar.setAction("Ok", {
                responseSnackbar.dismiss()
            })
            val snackBarView = responseSnackbar.view
            val snackTextView = snackBarView.findViewById(android.support.design.R.id.snackbar_text) as TextView
            snackTextView.setTextColor(Color.WHITE)
            snackBarView.setBackgroundColor(resources.getColor(R.color.snackbar_color))
            responseSnackbar.setActionTextColor(Color.WHITE)
            responseSnackbar.show()
            updateLastMark()
        } else {
            Toast.makeText(this, R.string.try_later, Toast.LENGTH_SHORT).show()
        }
    }


    private fun onMarkAttendance(permissionList: ArrayList<String>, appType: String) {
        if (hasPermissions(permissionList)) {
            // add condition of lat long not 0
            Log.i(TAG, "Lat in onMarkAttendance ==>" + latitude)
            Log.i(TAG, "Lon in onMarkAttendance ==>" + longitude)

            if (latitude != null && longitude != null && !latitude!!.equals(0.toDouble()) && !longitude!!.equals(0.toDouble())) {
                if (appType == APP_TYPE_1 || appType == APP_TYPE_2) {
                    punchDetails()
                } else {
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    intent.putExtra("android.intent.extras.IMAGE_CAPTURE", 1);
                    startActivityForResult(intent, CAMERA_REQUEST)
                }

            } else {
                Log.i(TAG, "No location found in onMarkAttendance")
                hideLoading()
                Toast.makeText(this, R.string.try_later, Toast.LENGTH_SHORT).show()
            }

        } else {
            ActivityCompat.requestPermissions(this, permissionList.toTypedArray
            (), REQUEST_ID_MULTIPLE_PERMISSIONS)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == CAMERA_REQUEST) {
            if (data != null) {
                val capturedImage: Bitmap = data.extras["data"] as Bitmap
                val resizeImage = getResizedBitmap(capturedImage, 600)
                val stream = ByteArrayOutputStream()
                resizeImage.compress(Bitmap.CompressFormat.JPEG, 90, stream)
                val imageBytes: ByteArray = stream.toByteArray()
                encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT)
                punchDetails()
            } else {
                hideLoading()
            }
        }
    }

    private fun hasPermissions(permissions: ArrayList<String>): Boolean {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && applicationContext != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(applicationContext, permission) !=
                        PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }

        return true
    }

    //reduce image size
    private fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
        var width = image.width
        var height = image.height

        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 1) {
            width = maxSize
            height = (width / bitmapRatio).toInt()
        } else {
            height = maxSize
            width = (height * bitmapRatio).toInt()
        }
        return Bitmap.createScaledBitmap(image, width, height, true)
    }

    private fun emojiPopUp() {
        val inflater = LayoutInflater.from(this)
        val dialogView = inflater.inflate(R.layout.emojis_layout, null)
        val builder = AlertDialog.Builder(this)
        builder.setView(dialogView)
        builder.setCancelable(true)
        val happyEmoji = dialogView.findViewById(R.id.happyEmoji) as ImageView
        val veryHappyEmoji = dialogView.findViewById(R.id.veryHappyEmoji) as ImageView
        val sadEmoji = dialogView.findViewById(R.id.sadEmoji) as ImageView
        val dispointedEmoji = dialogView.findViewById(R.id.dispointedEmoji) as ImageView
        val angreyEmoji = dialogView.findViewById(R.id.angreyEmoji) as ImageView

        val display: Display = windowManager.defaultDisplay
        val point = Point()
        display.getSize(point)
        val alert = builder.create()
        alert.window.attributes.gravity = Gravity.TOP or Gravity.START
        alert.window.attributes.x = 0
        alert.window.attributes.y = point.y / 5
        alert.window.decorView.setBackgroundColor(resources.getColor(R.color.transparent))
        if (!alert.isShowing)
            alert.show()
        happyEmoji.setOnClickListener()
        {
            defaultMood.setImageResource(R.drawable.ic_happy)
            Preferences.setEmojiName(this, R.drawable.ic_happy)
            alert.dismiss()
        }
        sadEmoji.setOnClickListener()
        {
            defaultMood.setImageResource(R.drawable.ic_sad)
            Preferences.setEmojiName(this, R.drawable.ic_sad)
            alert.dismiss()
        }
        veryHappyEmoji.setOnClickListener()
        {
            defaultMood.setImageResource(R.drawable.ic_very_happy)
            Preferences.setEmojiName(this, R.drawable.ic_very_happy)
            alert.dismiss()
        }
        angreyEmoji.setOnClickListener()
        {
            defaultMood.setImageResource(R.drawable.ic_angry)
            Preferences.setEmojiName(this, R.drawable.ic_angry)
            alert.dismiss()
        }
        dispointedEmoji.setOnClickListener()
        {
            defaultMood.setImageResource(R.drawable.ic_disappointed)
            Preferences.setEmojiName(this, R.drawable.ic_disappointed)
            alert.dismiss()
        }
    }

    private fun updateLastMark() {
        if (databaseHandler.getMaxDate() != null) {
            lastRecordDate = DateHelper.lastDateFormat(databaseHandler.getMaxDate().punchDateTime)
            lastPunchStatus = databaseHandler.getMaxDate().syncStatus
            addressLine = databaseHandler.getMaxDate().locationName!!
        }
        if (!lastRecordDate.isNullOrEmpty()) {
            lastMarking.text = "Last marking was $lastRecordDate ["
            if (databaseHandler.getMaxDate().isValid == 1) {
                lastMarkingStatus.setTextColor(resources.getColor(R.color.green))
            } else {
                lastMarkingStatus.setTextColor(resources.getColor(R.color.red))
            }
            lastMarkingStatus.text = "$lastPunchStatus"
            lastMarkingBraket.text = getString(R.string.bracket)
            idTvLocationName.text = addressLine

            var toggleText = false

            idTvLocationName.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    idTvLocationName.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    val lineCount = idTvLocationName.lineCount
                    if (lineCount < 3) {
                        showDetailAddress.visibility = GONE
                    } else {
                        showDetailAddress.visibility = VISIBLE
                    }
                }
            })

            //  idTvLocationName.setOnClickListener(null)

            showDetailAddress.setOnClickListener {
                if (toggleText) {
                    idTvLocationName.maxLines = 2
                    showDetailAddress.text = "show more"
                    toggleText = false
                } else {
                    idTvLocationName.maxLines = 10
                    showDetailAddress.text = "show less"
                    toggleText = true
                }
            }
        }
    }

    private fun mLocationCallBack(): LocationCallback {
        return object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                hideLoading()
                Log.i(TAG, "In mLocationCallBack method")
                Log.i(TAG, "p0 value ==>" + p0)
                Log.i(TAG, "p0 lastLocation ==>" + p0!!.lastLocation)
                //  change below condition to p0 != null && p0!!.lastlocation!=null
                if (p0 != null && p0.lastLocation != null) {
                    location = p0.lastLocation
                    latitude = location!!.latitude
                    longitude = location!!.longitude
                    Log.i(TAG, "Latitude ==>" + latitude)
                    Log.i(TAG, "Longitude ==>" + longitude)

                    val geocoder = Geocoder(applicationContext, Locale.getDefault())
                    val addresses: List<Address>
                    try {
                        addresses = geocoder.getFromLocation(latitude!!, longitude!!, 1)
                        addressLine = addresses[0].getAddressLine(0)
                    } catch (e: Exception) {
                    }

                    // Below code is used to get seperate address
                    /* val city = addresses[0].locality
                     val state = addresses[0].adminArea
                     val country = addresses[0].countryName
                     val postalCode = addresses[0].postalCode
                     val featureName = addresses[0].featureName
                     val premises = addresses[0].premises
                     val subAdminArea = addresses[0].subAdminArea
                     val subLocality = addresses[0].subLocality
                     val subThoroughfare = addresses[0].subThoroughfare
                     val thoroughfare = addresses[0].thoroughfare*/
                } else {
                    Log.i(TAG, "Not getting location")
                }
            }
        }
    }

    @SuppressLint("RestrictedApi")
    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = INTERVAL
        mLocationRequest.fastestInterval = FASTEST_INTERVAL
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun tabMenuUpdate(menuList: List<MenuEntity>) {
        menuTabLayout.tabMode = TabLayout.MODE_FIXED
        menuTabLayout.addTab(menuTabLayout.newTab().setCustomView(moodLayout), 0)
        var activeMenuCount = 0
        for (menu in menuList) {
            when (menu.menuName) {
                getString(R.string.attendance_flag) -> {
                    if (menu.menuFlag == 1) {
                        nav_view.menu.findItem(R.id.attendance_menu).isVisible = true
                        markAttendance.isEnabled = true
                        menuTabLayout.addTab(menuTabLayout.newTab().setCustomView(attendanceLayout))
                        activeMenuCount++
                    } else {
                        nav_view.menu.findItem(R.id.attendance_menu).isVisible = false
                        markAttendance.isEnabled = false
                    }
                }
                getString(R.string.expense_flag) -> {
                    if (menu.menuFlag == 1) {
                        nav_view.menu.findItem(R.id.claim_menu).isVisible = true
                        menuTabLayout.addTab(menuTabLayout.newTab().setCustomView(expenseLayout))
                        activeMenuCount++
                    } else {
                        nav_view.menu.findItem(R.id.claim_menu).isVisible = false
                    }
                }
            }

        }
        if (activeMenuCount > 1) {
            menuTabLayout.tabMode = TabLayout.MODE_SCROLLABLE
        }
    }

    private fun menuSetUp() {
        val layoutInflater: LayoutInflater = LayoutInflater.from(this)
        headerView = layoutInflater.inflate(R.layout.custom_tab, null, true)
        moodLayout = headerView!!.findViewById(R.id.moodLayout) as RelativeLayout
        attendanceLayout = headerView!!.findViewById(R.id.attendanceLayout) as RelativeLayout
        attendanceLog = headerView!!.findViewById(R.id.attendanceLog)
        expenseLayout = headerView!!.findViewById(R.id.expenseLayout) as RelativeLayout
        expenseLog = headerView!!.findViewById(R.id.expenseClaim)

    }


    fun showSnackbar(msg: String) {
        val responseSnackbar = Snackbar.make(relativeLayout, msg, Snackbar.LENGTH_INDEFINITE)
        responseSnackbar.setAction("Ok") {
            responseSnackbar.dismiss()
        }

        val snackBarView = responseSnackbar.view
        val snackTextView = snackBarView.findViewById(android.support.design.R.id.snackbar_text) as TextView
        snackTextView.maxLines = 3
        snackTextView.setTextColor(Color.WHITE)
        snackBarView.setBackgroundColor(resources.getColor(R.color.snackbar_color))
        responseSnackbar.setActionTextColor(Color.WHITE)
        responseSnackbar.show()
    }
}

