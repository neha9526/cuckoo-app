package com.coolappz.CuckooTechApp.dashboard

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeAttendanceRequest
import com.coolappz.CuckooTechApp.network.requestpojo.MenuRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.MenuResponse
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.SingleAttendanceResponse
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor

/**
 * Created by admin on 06-02-2018.
 */
interface DashboardInteractor {
    fun onPunchRequest(context: Context, employeeAttendanceRequest: EmployeeAttendanceRequest,
                       interfaceInteractor: InterfaceInteractor<SingleAttendanceResponse>)

    fun onMenuSync(context: Context, menuRequestPojo: MenuRequestPojo,
                   menuInteractor: MenuInterfaceInteractor, interfaceInteractor: InterfaceInteractor<SingleAttendanceResponse>)
}