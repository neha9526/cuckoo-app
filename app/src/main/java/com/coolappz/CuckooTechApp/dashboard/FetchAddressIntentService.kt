package com.coolappz.CuckooTechApp.dashboard

import android.app.IntentService
import android.content.ContentValues.TAG
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.ResultReceiver
import android.util.Log
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.utility.Constants
import com.coolappz.CuckooTechApp.utility.Constants.Constants.FAILURE_RESULT
import com.coolappz.CuckooTechApp.utility.Constants.Constants.RECEIVER
import com.coolappz.CuckooTechApp.utility.Constants.Constants.RESULT_DATA_KEY
import com.coolappz.CuckooTechApp.utility.Constants.Constants.SUCCESS_RESULT
import java.io.IOException
import java.util.*

/**
 * Created by admin on 28-03-2018.
 */
class FetchAddressIntentService : IntentService("GeoLocation") {
    private var receiver: ResultReceiver? = null
    private var lattitude: Double? =null
    private var longitude:Double? = null


    override fun onHandleIntent(p0: Intent?) {
       p0 ?: return
            val geoCoder = Geocoder(this, Locale.getDefault())
            var errorMessage = ""
            receiver=p0.getParcelableExtra(RECEIVER)


           lattitude= p0.getDoubleExtra(Constants.Constants.LOCATION_LATTITUDE,0.0)
           longitude =p0.getDoubleExtra(Constants.Constants.LOCATION_LONGITUDE,0.0)

            var addresses: List<Address> = emptyList()

            try {
                if (longitude!=null&&lattitude!=null) {
                    addresses = geoCoder.getFromLocation(lattitude!!, longitude!!,
                            1)
                }

            } catch (ioException: IOException) {
                // Catch network or other I/O problems.
                errorMessage = getString(R.string.location_name_update)
                Log.e(TAG, errorMessage, ioException)
            } catch (illegalArgumentException: IllegalArgumentException) {
                // Catch invalid latitude or longitude values.
                errorMessage = getString(R.string.invalid_lat_long_used)
                Log.e(TAG, "$errorMessage. Latitude =lattitude , " +
                        "Longitude =  longitude", illegalArgumentException)
            }

            // Handle case where no address was found.
            if (addresses.isEmpty()) {
                if (errorMessage.isEmpty()) {
                    errorMessage = getString(R.string.no_address_found)
                    Log.e(TAG, errorMessage)
                }
                deliverResultToReceiver(FAILURE_RESULT, errorMessage)
            } else {
                val address = addresses[0]
                val addressLine=address.getAddressLine(0)
                Log.i(TAG, getString(R.string.address_found))
                deliverResultToReceiver(SUCCESS_RESULT, addressLine)
            }

    }

    private fun deliverResultToReceiver(resultCode: Int, message: String) {
        val bundle = Bundle().apply { putString(RESULT_DATA_KEY, message) }
        if(receiver!=null)
        {
            receiver?.send(resultCode, bundle)
        }

    }


}
