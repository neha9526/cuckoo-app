package com.coolappz.CuckooTechApp.dashboard

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeAttendanceRequest
import com.coolappz.CuckooTechApp.network.requestpojo.MenuRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.SingleAttendanceResponse

/**
 * Created by admin on 06-02-2018.
 */
interface DashboardPresenter{

    fun onPunchRequest(context: Context,employeeAttendanceRequest: EmployeeAttendanceRequest)

    fun onMenuSyncRequest(context: Context, menuRequestPojo: MenuRequestPojo)

    fun onDestroyView()
}