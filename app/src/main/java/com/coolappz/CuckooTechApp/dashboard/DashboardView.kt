package com.coolappz.CuckooTechApp.dashboard

import com.coolappz.CuckooTechApp.network.BaseView
import com.coolappz.CuckooTechApp.network.responsepojo.MenuResponse
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.SingleAttendanceResponse

/**
 * Created by admin on 06-02-2018.
 */
interface DashboardView<T>:BaseView<T> {

  fun onMenuSyncSuccessful(response:MenuResponse)
}