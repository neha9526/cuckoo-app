package com.coolappz.CuckooTechApp.dashboard

import android.content.Context
import android.util.Log
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.network.Api
import com.coolappz.CuckooTechApp.network.ApiFail
import com.coolappz.CuckooTechApp.network.ApiSuccess
import com.coolappz.CuckooTechApp.network.HttpErrorResponse
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeAttendanceRequest
import com.coolappz.CuckooTechApp.network.requestpojo.MenuRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.MenuResponse
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.EmployeeAttendanceResponse
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.SingleAttendanceResponse
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by admin on 06-02-2018.
 */
class DashboardInteractorImpl:DashboardInteractor {
    override fun onMenuSync(context: Context, menuRequestPojo: MenuRequestPojo,
                            menuInteractor: MenuInterfaceInteractor,interfaceInteractor: InterfaceInteractor<SingleAttendanceResponse>) {
        Api.userManagement().getMenuDetails(menuRequestPojo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiSuccess<MenuResponse>() {
                    override fun call(response: MenuResponse) {
                        when {
                            response.response!!.isSucceeded!! -> {
                                menuInteractor.onFetchedMenuDetails(response)
                            }
                            else -> {
                                interfaceInteractor.failApi(response.response.responseMessage!!)
                            }
                        }
                    }

                }, object : ApiFail() {
                    override fun noNetworkError() {
                        interfaceInteractor.noInternet()
                    }

                    override fun unknownError(e: Throwable) {
                        Log.e("TAG","Unknown error "+e.printStackTrace())
                        interfaceInteractor.failApi(context.getString(R.string.unknown_error_msg))
                    }

                    override fun httpStatus(response: HttpErrorResponse) {
                        Log.e("TAG","HttpErrorResponse "+response.response.message())
                        interfaceInteractor.failApi(context.getString(R.string.http_status_error_msg))
                    }

                })


    }

    override fun onPunchRequest(context: Context, employeeAttendanceRequest: EmployeeAttendanceRequest,
                                interfaceInteractor: InterfaceInteractor<SingleAttendanceResponse>) {

        Api.userManagement().employeePunching(employeeAttendanceRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiSuccess<EmployeeAttendanceResponse>() {
                    override fun call(response: EmployeeAttendanceResponse) {
                        when {
                                response.response!!.isSucceeded!! -> {
                                interfaceInteractor.successApi(response.response)
                            }
                            else -> {
                                interfaceInteractor.failApi(response.response.responseMessage!!)
                            }
                        }
                    }

                }, object : ApiFail() {
                    override fun noNetworkError() {
                        interfaceInteractor.noInternet()
                    }

                    override fun unknownError(e: Throwable) {
                        Log.e("TAG","Unknown error "+e.printStackTrace())
                        interfaceInteractor.failApi(context.getString(R.string.unknown_error_msg))
                    }

                    override fun httpStatus(response: HttpErrorResponse) {
                        Log.e("TAG","HttpErrorResponse "+response.response.message())
                        interfaceInteractor.failApi(context.getString(R.string.http_status_error_msg))
                    }

                })



    }

}