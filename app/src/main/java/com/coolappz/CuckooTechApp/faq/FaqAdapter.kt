package com.coolappz.CuckooTechApp.faq

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter
import com.afollestad.sectionedrecyclerview.SectionedViewHolder
import com.coolappz.CuckooTechApp.R
import kotlinx.android.synthetic.main.adapter_faq_header.view.*
import kotlinx.android.synthetic.main.adapter_faq_item.view.*

/**
 * Created by admin on 18-05-2018.
 */
class FaqAdapter(private var questionAnswerList: ArrayList<FaqPojo>, private var onItemClick:FaqItemClickListener) : SectionedRecyclerViewAdapter<FaqAdapter.FaqViewHolder>() {

   // private var onClickFlag:Boolean=false
    override fun onBindHeaderViewHolder(holder: FaqViewHolder?, section: Int, expanded: Boolean) {

        holder!!.itemView.caret.setImageResource(if (expanded) R.drawable.ic_collapse else R.drawable.ic_expand)
        holder.itemView.faq_question.text=questionAnswerList[section].question
        holder.itemView.relativeQuestionLayout.setOnClickListener {
            onItemClick.onItemClick(section)

        }

    }

    override fun getSectionCount(): Int {
       return questionAnswerList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): FaqViewHolder {
        var layout: Int = 0
        layout = when (viewType) {
            VIEW_TYPE_HEADER -> R.layout.adapter_faq_header
            VIEW_TYPE_ITEM -> R.layout.adapter_faq_item
            else-> R.layout.adapter_faq_item
        }
        val layoutView = LayoutInflater.from(parent!!.context).inflate(layout, parent, false)
        return FaqViewHolder(layoutView)
    }

    override fun getItemCount(section: Int): Int {

        return  questionAnswerList[section].answerList.size
    }

    override fun onBindViewHolder(holder: FaqViewHolder?, section: Int, relativePosition: Int, absolutePosition: Int) {
        holder!!.itemView.ansFaq.text = questionAnswerList[section].answerList[relativePosition]

    }

    override fun onBindFooterViewHolder(holder: FaqViewHolder?, section: Int) {}


    class FaqViewHolder(itemView:View) : SectionedViewHolder(itemView)
    {

       /* var caret: ImageView = itemView.findViewById<ImageView>(R.id.caret)
        var textQuestion: TextView = itemView.findViewById<TextView>(R.id.faq_question)
        var questionLayout:RelativeLayout=itemView.findViewById<RelativeLayout>(R.id.relativeQuestionLayout)
        var answerLayout:CardView=itemView.findViewById<CardView>(R.id.answerLayout)
        var textAnswer: TextView = itemView.findViewById<TextView>(R.id.ansFaq)*/
    }
}
