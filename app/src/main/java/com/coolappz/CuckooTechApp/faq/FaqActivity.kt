package com.coolappz.CuckooTechApp.faq

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.utility.BaseAppCompatActivity
import com.coolappz.CuckooTechApp.utility.DefaultView
import kotlinx.android.synthetic.main.activity_base_app_compat.*
import kotlinx.android.synthetic.main.activity_faq.*


class FaqActivity : BaseAppCompatActivity<DefaultView, FaqPresenterImpl>(), DefaultView, FaqItemClickListener {


    override val layoutIdForInjection: Int = R.layout.activity_faq
    lateinit var faqRecycler: RecyclerView
    var questionAnswerList: ArrayList<FaqPojo> = ArrayList()
    var supportedDeviceList: ArrayList<String> = ArrayList()
    var multiDeviceLoginList: ArrayList<String> = ArrayList()
    var loginProcessList: ArrayList<String> = ArrayList()
    var markAttendanceList: ArrayList<String> = ArrayList()
    var locationAccessList: ArrayList<String> = ArrayList()
    var cameraAccessList: ArrayList<String> = ArrayList()
    var versionUpgradeList: ArrayList<String> = ArrayList()
    private var mAdapter: FaqAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenSubTitle.text = getString(R.string.faq)

        questionAnswerList.add(FaqPojo((getString(R.string.supported_devices)), supportedDeviceList))
        questionAnswerList.add(FaqPojo((getString(R.string.mult_device_login)), multiDeviceLoginList))
        questionAnswerList.add(FaqPojo((getString(R.string.login_process)), loginProcessList))
        questionAnswerList.add(FaqPojo((getString(R.string.mark_attendance)), markAttendanceList))
        questionAnswerList.add(FaqPojo((getString(R.string.location_permission_access)), locationAccessList))
        questionAnswerList.add(FaqPojo((getString(R.string.camera_permission_access)), cameraAccessList))
        questionAnswerList.add(FaqPojo((getString(R.string.version_upgrade)), versionUpgradeList))


        supportedDeviceList.add(getString(R.string.version_support))
        supportedDeviceList.add(getString(R.string.internet_connection_features))

        multiDeviceLoginList.add(getString(R.string.multi_device_login_1))
        multiDeviceLoginList.add(getString(R.string.multi_device_login_2))
        multiDeviceLoginList.add(getString(R.string.multi_device_login_3))


        loginProcessList.add(getString(R.string.login_process_1))
        loginProcessList.add(getString(R.string.login_process_2))

        markAttendanceList.add(getString(R.string.mark_attendance_1))
        markAttendanceList.add(getString(R.string.mark_attendance_2))
        markAttendanceList.add(getString(R.string.mark_attendance_3))
        markAttendanceList.add(getString(R.string.mark_attendance_4))

        locationAccessList.add(getString(R.string.location_access_1))
        locationAccessList.add(getString(R.string.location_access_2))

        cameraAccessList.add(getString(R.string.camera_access_1))
        cameraAccessList.add(getString(R.string.camera_access_2))

        versionUpgradeList.add(getString(R.string.version_upgrade_1))
        versionUpgradeList.add(getString(R.string.version_upgrade_2))
        versionUpgradeList.add(getString(R.string.version_upgrade_3))


        faqRecycler = faqRecyclerView
        mAdapter = FaqAdapter(questionAnswerList, this)
        val linearLayoutManager = LinearLayoutManager(this)
        faqRecycler.layoutManager = linearLayoutManager
        faqRecycler.adapter = mAdapter
        mAdapter!!.collapseAllSections()
    }

    override fun createPresenter(): FaqPresenterImpl {
        presenter = FaqPresenterImpl(this)
        return presenter
    }

    override fun onItemClick(sectionPosition: Int) {
        // holder.itemView.ansLayout
        mAdapter!!.toggleSectionExpanded(sectionPosition)
    }

    override fun onItemClick(holder: FaqAdapter.FaqViewHolder) {}
}

