package com.coolappz.CuckooTechApp.faq

/**
 * Created by admin on 19-05-2018.
 */
interface FaqItemClickListener {
    fun onItemClick(holder:FaqAdapter.FaqViewHolder)
    fun onItemClick(sectionPosition: Int)
}