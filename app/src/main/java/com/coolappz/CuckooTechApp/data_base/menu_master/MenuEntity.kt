package com.coolappz.CuckooTechApp.data_base.menu_master

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.jetbrains.annotations.NotNull

/**
 * Created by admin on 10-08-2018.
 */

const val TABLE_MENU_MASTER: String = "menuMaster"
const val MENU_ID: String = "menuId"
const val MENU_NAME: String = "moduleName"
const val MENU_FLAG: String = "isActiveFlag"

@Entity(tableName = TABLE_MENU_MASTER)
class MenuEntity(@ColumnInfo(name = MENU_NAME)
                 var menuName: String,
                 @ColumnInfo(name = MENU_FLAG)
                 var menuFlag: Int) {

    @ColumnInfo(name = MENU_ID)
    @PrimaryKey(autoGenerate = true)
    var menuId: Int = 0
}
