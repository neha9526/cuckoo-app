
package com.coolappz.CuckooTechApp.data_base.attendanceDatabse

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.jetbrains.annotations.NotNull

/**
 * Created by admin on 20-02-2018.
 */
const val ATTENDANCE_ID: String = "attendanceId"
const val PUNCH_TIME: String = "punchTime"
const val LOCATION_NAME: String = "locationName"
const val LONGITUDE: String = "longitude"
const val LATITUDE: String = "latitude"
const val SYNC_STATUS: String = "syncStatus"
const val ATTENDANCE_LOG: String = "attendanceLog"
const val EMP_IMG:String="employeeImage"
const val IS_VALID:String= "isValid"


@Entity(tableName = ATTENDANCE_LOG)
data class AttendanceEntity(

        @NotNull
        @ColumnInfo(name = PUNCH_TIME)
        var punchDateTime: String,
        @ColumnInfo(name = LOCATION_NAME)
        var locationName: String?,
        @NotNull
        @ColumnInfo(name = LONGITUDE)
        var longitude: Double,
        @NotNull
        @ColumnInfo(name = LATITUDE)
        var latitude: Double,
        @NotNull
        @ColumnInfo(name = SYNC_STATUS)
        var syncStatus: String,
        @NotNull
        @ColumnInfo(name = EMP_IMG)
        var empImage: String,
        @ColumnInfo(name = IS_VALID)
         var isValid:Int)

{

    @NotNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ATTENDANCE_ID)
    var attendanceId: Int = 0
}