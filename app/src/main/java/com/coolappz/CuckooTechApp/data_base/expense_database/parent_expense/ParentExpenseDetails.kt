package com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense

import android.arch.persistence.room.*
import org.jetbrains.annotations.NotNull

/**
 * Created by admin on 22-06-2018.
 */

const val CLAIM_ID: String = "claimId"
const val CLAIM_TOKEN: String = "claimToken"
const val CLAIM_CURRENCY_ID: String = "claimCurrencyId"
const val CLAIM_FOR: String = "claimFor"
const val CLAIM_DATE: String = "claimDate"
const val TOTAL_CLAIM_AMOUNT: String = "totalClaimAmount"
const val CLAIM_STATUS: String = "claimStatus"
const val CLAIM_TABLE: String = "parentExpenseLog"

@Entity(tableName = CLAIM_TABLE, indices = arrayOf(Index(value = [CLAIM_TOKEN], unique = true)))
class ParentExpenseDetails(@NotNull
                           @ColumnInfo(name = CLAIM_TOKEN)
                           var claimToken: String,
                           @ColumnInfo(name = CLAIM_FOR)
                           var claimFor: String,
                           @ColumnInfo(name = CLAIM_DATE)
                           var claimDate: String,
                           @ColumnInfo(name = CLAIM_CURRENCY_ID)
                           var claimCurrencyId: Int,
                           @ColumnInfo(name = TOTAL_CLAIM_AMOUNT)
                           var totalClaimAmt: Double,
                           @ColumnInfo(name = CLAIM_STATUS)
                           var claimStatus: String) {
    @NotNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = CLAIM_ID)
    var claimId: Int = 0
}