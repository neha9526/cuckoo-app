package com.coolappz.CuckooTechApp.data_base.loginDatabase

import android.arch.persistence.room.*


/**
 * Created by admin on 09-02-2018.
 */
@Dao
interface LoginDao {


    @Query("UPDATE loginDetails SET tokenNumber=:token, timeStamp=:time WHERE employeeCode=:empCode")
    fun updateTokenTime(token:String?,time:String?,empCode:String?)

    @Query("SELECT * FROM"+" "+TABLE_LOGIN_DETAILS)
    fun getAll(): LoginDetails

    @Insert
    fun insertAll(loginDetails: LoginDetails)

    @Delete
    fun delete(loginDetails: LoginDetails)

    @Query("UPDATE "+TABLE_LOGIN_DETAILS+" SET "+ APP_TYPE+" =:appType WHERE employeeCode=:empCode")
    fun updateAppType(appType:String?,empCode:String?)
}