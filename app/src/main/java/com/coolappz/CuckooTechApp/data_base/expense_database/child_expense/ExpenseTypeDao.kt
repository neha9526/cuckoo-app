package com.coolappz.CuckooTechApp.data_base.expense_database.child_expense

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

/**
 * Created by admin on 16-07-2018.
 */


@Dao
interface ExpenseTypeDao {

   @Insert
   fun insertExpenseTypes(expenseTypeEntity: ExpenseTypeEntity)

   @Query("SELECT * FROM  "+ EXPENSE_TABLE )
   fun fetchExpenseTypeDetails():List<ExpenseTypeEntity>

   @Query("SELECT "+ EXPENSE_TYPE+" FROM "+ EXPENSE_TABLE )
   fun fetchAllExpenseType():List<String>


   @Query("SELECT "+ EXPENSE_TYPE_ID +" FROM  "+ EXPENSE_TABLE +" WHERE "+ EXPENSE_TYPE +" = :expenseType")
   fun fetchExpenseTypeId(expenseType:String):Int

   @Query("SELECT "+ EXPENSE_TYPE +" FROM  "+ EXPENSE_TABLE +" WHERE "+ EXPENSE_TYPE_ID +" = :expenseTypeId")
   fun fetchExpenseType(expenseTypeId:Int):String

}
