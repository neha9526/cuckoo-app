package com.coolappz.CuckooTechApp.data_base.expense_database.child_expense

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import org.jetbrains.annotations.NotNull

/**
 * Created by admin on 18-06-2018.
 */

const val EXPENSE_ID: String = "expenseId"
const val EXPENSE_TYPE_ID: String = "expenseTypeId"
const val EXPENSE_TYPE : String = "expenseType"
const val EXPENSE_TABLE:String ="expenseLog"

@Entity(tableName = EXPENSE_TABLE,indices = arrayOf(Index(value = [EXPENSE_TYPE_ID, EXPENSE_TYPE], unique = true)))
class ExpenseTypeEntity( @NotNull
                         @ColumnInfo(name = EXPENSE_TYPE_ID)
                         var expenseTypeId: Int?,
                         @NotNull
                         @ColumnInfo(name = EXPENSE_TYPE)
                         var expenseType: String?
                         ) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = EXPENSE_ID)
    var expenseId: Int=0
}