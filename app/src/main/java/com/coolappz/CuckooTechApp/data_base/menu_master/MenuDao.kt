package com.coolappz.CuckooTechApp.data_base.menu_master

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

/**
 * Created by admin on 10-08-2018.
 */

@Dao
interface MenuDao {
    @Query("SELECT * FROM " + TABLE_MENU_MASTER + " GROUP BY " + MENU_NAME)
    fun getMenuDetails(): List<MenuEntity>

    @Insert
    fun insertMenuDetails(menuEntity: MenuEntity)

    @Query("SELECT $MENU_NAME FROM $TABLE_MENU_MASTER")
    fun getMenuNames(): List<String>

    @Query("UPDATE " + TABLE_MENU_MASTER + " SET " + MENU_FLAG + " =:menuFlag WHERE " + MENU_NAME + " =:menuName")
    fun updateMenuFlag(menuName: String, menuFlag: Int)
}