package com.coolappz.CuckooTechApp.data_base.attendanceDatabse

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query


/**
 * Created by admin on 20-02-2018.
 */

@Dao
interface AttendanceDao {
    @Query("SELECT * FROM" + " " + ATTENDANCE_LOG)
    fun getAllAttendance(): List<AttendanceEntity>

    @Query("SELECT * FROM $ATTENDANCE_LOG WHERE $IS_VALID= -1")
    fun getBulkPunchList(): List<AttendanceEntity>

    //fetch unsynch data
    @Query("UPDATE " + ATTENDANCE_LOG + " " + "SET " + SYNC_STATUS + "=:syncStatus," + IS_VALID + "= :isValid" +
            " WHERE " + ATTENDANCE_ID + "=:id")
    fun updateStatusOnSync(syncStatus: String, isValid: Int, id: Int)

    //last 30 days query
    @Query("DELETE  FROM "+ATTENDANCE_LOG+" WHERE strftime('%Y-%m-%d %H:%M:%S',"+ PUNCH_TIME+") <= " +
            " strftime('%Y-%m-%d %H:%M:%S', 'now','-30 days')")
    fun deleteExtraData(): Int

    //quey to fetch max and min date
    @Query("SELECT * FROM $ATTENDANCE_LOG ORDER BY $ATTENDANCE_ID DESC LIMIT 1")
    fun fetchMaxDate():AttendanceEntity

    @Query("SELECT * FROM $ATTENDANCE_LOG ORDER BY $ATTENDANCE_ID ASC LIMIT 1")
    fun fetchMinDate():AttendanceEntity

   //query for date filter
    @Query("SELECT * FROM" + " " + ATTENDANCE_LOG + " " + "WHERE" + " " + PUNCH_TIME + " >= :fromDate " +
            " AND " + PUNCH_TIME + " <= :toDate")
    fun sortByDate(fromDate: String, toDate: String): List<AttendanceEntity>

    //query to get count of records with different status
    @Query("SELECT COUNT($SYNC_STATUS) FROM $ATTENDANCE_LOG WHERE $IS_VALID=1")
    fun getValidRecordCount():Int

    @Query("SELECT COUNT($SYNC_STATUS) FROM $ATTENDANCE_LOG WHERE $IS_VALID=0")
    fun getInvalidRecordCount():Int

    @Query("SELECT COUNT($SYNC_STATUS) FROM $ATTENDANCE_LOG WHERE $IS_VALID=-1")
    fun getOfflineRecordCount():Int

    @Query("DELETE  FROM $ATTENDANCE_LOG")
    fun deleteAttendance():Int

    @Insert
    fun insertAllAttendance(attendanceEntity: AttendanceEntity)
}