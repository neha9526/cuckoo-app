package com.coolappz.CuckooTechApp.data_base.tokenTable

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.jetbrains.annotations.NotNull

/**
 * Created by admin on 07-04-2018.
 */

const val TOKEN_TABLE = "tokenDetails"
const val TOKEN_ID: String = "tokenId"
const val ACTIVITY_NAME: String = "activityName"
const val TOKEN_NUMBER: String = "tokenNumber"
const val TIME_STAMP: String = "timeStamp"

@Entity(tableName = TOKEN_TABLE)
class TokenEntity(
        @NotNull
        @ColumnInfo(name = ACTIVITY_NAME)
        var activityName: String,
        @NotNull
        @ColumnInfo(name = TOKEN_NUMBER)
        var tokenNumber: String,
        @NotNull
        @ColumnInfo(name = TIME_STAMP)
        var timeStamp: String) {

    @NotNull
    @ColumnInfo(name = TOKEN_ID)
    @PrimaryKey(autoGenerate = true)
    var tokenId: Int = 0
}