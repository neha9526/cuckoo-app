package com.coolappz.CuckooTechApp.data_base

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.migration.Migration
import android.content.Context
import com.coolappz.CuckooTechApp.data_base.attendanceDatabse.AttendanceDao
import com.coolappz.CuckooTechApp.data_base.attendanceDatabse.AttendanceEntity
import com.coolappz.CuckooTechApp.data_base.expense_database.*
import com.coolappz.CuckooTechApp.data_base.expense_database.child_expense.ChildExpenseDao
import com.coolappz.CuckooTechApp.data_base.expense_database.child_expense.ChildExpenseDetails
import com.coolappz.CuckooTechApp.data_base.expense_database.child_expense.ExpenseTypeDao
import com.coolappz.CuckooTechApp.data_base.expense_database.child_expense.ExpenseTypeEntity
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.ParentDao
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.ParentExpenseDetails
import com.coolappz.CuckooTechApp.data_base.loginDatabase.LoginDao
import com.coolappz.CuckooTechApp.data_base.loginDatabase.LoginDetails
import com.coolappz.CuckooTechApp.data_base.menu_master.MenuDao
import com.coolappz.CuckooTechApp.data_base.menu_master.MenuEntity
import com.coolappz.CuckooTechApp.data_base.tokenTable.TokenDao
import com.coolappz.CuckooTechApp.data_base.tokenTable.TokenEntity

/**
 * Created by admin on 09-02-2018.
 */
@Database(entities = [(LoginDetails::class),(AttendanceEntity::class),(TokenEntity::class),
    (CurrencyTypeEntity::class),(ExpenseTypeEntity::class),(ParentExpenseDetails::class),(ChildExpenseDetails::class),
    (MenuEntity::class)], version = 2,exportSchema = false)

abstract class  AppDatabase:RoomDatabase() {
    abstract fun loginDao(): LoginDao
    abstract fun attendanceDao(): AttendanceDao
    abstract fun tokenDao():TokenDao
    abstract fun currencyDao():CurrencyDao
    abstract fun expenseTypeDao(): ExpenseTypeDao
    abstract fun childExpenseDao(): ChildExpenseDao
    abstract fun parentExpenseDao():ParentDao
    abstract fun menuMasterDao():MenuDao

    object DatabaseProvider {

        private var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context,
                            AppDatabase::class.java,"cuckoo_db")
                            .allowMainThreadQueries()
                            .addMigrations(MIGRATION_1_2)
                            .build()
                }
            }
            return INSTANCE
        }

        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL( "CREATE TABLE IF NOT EXISTS childExpenseLog(claimToken TEXT NOT NULL,expenseCurrencyId INTEGER NOT NULL," +
                        "expenseTypeId INTEGER NOT NULL, toDate TEXT NOT NULL,fromDate TEXT NOT NULL,conversionRate REAL NOT NULL,expenseAmount REAL NOT NULL, " +
                        "convertedAmount REAL NOT NULL,description TEXT NOT NULL,uploadFiles TEXT NOT NULL,childExpenseId INTEGER PRIMARY KEY NOT NULL)")

                database.execSQL("CREATE TABLE IF NOT EXISTS parentExpenseLog(claimToken TEXT NOT NULL,claimCurrencyId INTEGER NOT NULL," +
                        "claimFor TEXT NOT NULL,claimDate TEXT NOT NULL,totalClaimAmount REAL NOT NULL," +
                        "claimStatus TEXT NOT NULL,claimId INTEGER PRIMARY KEY NOT NULL)")

                database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS index_parentExpenseLog_claimToken ON " +
                        "parentExpenseLog(claimToken)")

                database.execSQL("CREATE TABLE IF NOT EXISTS currencyLog(currencyTypeId INTEGER NOT NULL," +
                        "currencyType TEXT NOT NULL, conversionRate REAL NOT NULL,currencyId INTEGER PRIMARY KEY NOT NULL) ")

                database.execSQL("CREATE UNIQUE INDEX  IF NOT EXISTS index_currencyLog_currencyTypeId_currencyType ON currencyLog(currencyTypeId,currencyType)")

                database.execSQL("CREATE TABLE IF NOT EXISTS expenseLog(expenseTypeId INTEGER," +
                        "expenseType TEXT,expenseId INTEGER PRIMARY KEY NOT NULL ) ")

                database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS index_expenseLog_expenseTypeId_expenseType ON " +
                        "expenseLog(expenseTypeId, expenseType)")

                database.execSQL("CREATE TABLE IF NOT EXISTS menuMaster( moduleName TEXT  NOT NULL ,isActiveFlag INTEGER  NOT NULL," +
                        " menuId INTEGER PRIMARY KEY  NOT NULL) ")

            }
        }

    }


}