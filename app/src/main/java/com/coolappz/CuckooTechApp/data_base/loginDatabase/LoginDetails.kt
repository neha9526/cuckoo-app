package com.coolappz.CuckooTechApp.data_base.loginDatabase

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.jetbrains.annotations.NotNull


/**
 * Created by admin on 09-02-2018.
 */
const val TABLE_LOGIN_DETAILS:String="loginDetails"
const val EMPLOYEE_ID: String = "employeeId"
const val EMPLOYEE_CODE: String = "employeeCode"
const val COMPANY_CODE: String = "companyCode"
const val MAC_ADDRESS: String = "macAddress"
const val MOBILE_NUMBER:String="mobileNumber"
const val APP_TYPE: String = "appType"
const val TOKEN_NUMBER: String = "tokenNumber"
const val TIME_STAMP: String = "timeStamp"


@Entity(tableName = TABLE_LOGIN_DETAILS)
class LoginDetails(
                   @NotNull
                   @ColumnInfo(name = COMPANY_CODE)
                   var companyCode:String?,
                   @NotNull
                   @ColumnInfo(name = EMPLOYEE_CODE)
                   var employeeCode: String?,
                   @NotNull
                   @ColumnInfo(name = MOBILE_NUMBER)
                   var mobileNo:String?,
                   @NotNull
                   @ColumnInfo(name = MAC_ADDRESS)
                   var macAddress:String?,
                   @NotNull
                   @ColumnInfo(name = APP_TYPE)
                   var appType:String?,
                   @NotNull
                   @ColumnInfo(name = TOKEN_NUMBER)
                   var tokenNumber:String?,
                   @NotNull
                   @ColumnInfo(name = TIME_STAMP)
                   var  timeStamp:String?) {
    @NotNull
    @ColumnInfo(name = EMPLOYEE_ID)
    @PrimaryKey(autoGenerate = true)
    var employeeId: Int = 0
}