package com.coolappz.CuckooTechApp.data_base.expense_database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

/**
 * Created by admin on 16-07-2018.
 */
@Dao
interface CurrencyDao {
    @Insert
    fun insertAllCurrencies(currencyTypeEntity: CurrencyTypeEntity)

    @Query("SELECT "+ CURRENCY_TYPE_ID +" FROM  "+ CURRENCY_TABLE +" WHERE "+ CURRENCY_TYPE+" = :currencyType")
    fun fetchCurrencyTypeId(currencyType:String):Int

     @Query("SELECT "+ CURRENCY_TYPE +" FROM  "+ CURRENCY_TABLE +" WHERE "+ CURRENCY_TYPE_ID+" = :currencyTypeId")
     fun fetchCurrencyName(currencyTypeId:Int):String

     @Query("SELECT "+ CURRENCY_COUNTRY +" FROM  "+ CURRENCY_TABLE +" WHERE "+ CURRENCY_TYPE+" = :currencyType")
     fun  fetchCurrencyRate(currencyType:String):Double

}