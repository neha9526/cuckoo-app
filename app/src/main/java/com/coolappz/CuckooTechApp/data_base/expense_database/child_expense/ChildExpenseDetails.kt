package com.coolappz.CuckooTechApp.data_base.expense_database.child_expense

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.PrimaryKey
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.CLAIM_TOKEN
import com.coolappz.CuckooTechApp.data_base.expense_database.CURRENCY_COUNTRY
import org.jetbrains.annotations.NotNull


/**
 * Created by admin on 22-06-2018.
 */
const val CHILD_EXPENSE_ID: String = "childExpenseId"
const val EXPENSE_CURRENCY_ID :String ="expenseCurrencyId"
const val TO_DATE:String="toDate"
const val FROM_DATE:String ="fromDate"
const val CONVERSION_RATE:String ="conversionRate"
const val EXPENSE_AMOUNT:String ="expenseAmount"
const val CONVERTED_AMOUNT:String ="convertedAmount"
const val DESCRIPTION:String ="description"
const val UPLOADED_FILES:String ="uploadFiles"
const val CHILD_EXPENSE_TABLE :String ="childExpenseLog"


@Entity(tableName = CHILD_EXPENSE_TABLE)
class ChildExpenseDetails(@NotNull
                           @ColumnInfo(name = CLAIM_TOKEN)
                           var claimToken: String,
                           @NotNull
                           @ColumnInfo(name = EXPENSE_CURRENCY_ID)
                           var expenseCurrencyId: Int,
                           @NotNull
                           @ColumnInfo(name = EXPENSE_TYPE_ID)
                           var expenseTypeId: Int,
                           @NotNull
                           @ColumnInfo(name = TO_DATE)
                           var toDate: String,
                           @NotNull
                           @ColumnInfo(name = FROM_DATE)
                           var fromDate: String,
                           @NotNull
                           @ColumnInfo(name = CURRENCY_COUNTRY)
                           var conversionRate: Double,
                           @NotNull
                           @ColumnInfo(name = EXPENSE_AMOUNT)
                           var expenseAmt: Double,
                           @NotNull
                           @ColumnInfo(name = CONVERTED_AMOUNT)
                           var convertedAmt: Double,
                           @NotNull
                           @ColumnInfo(name = DESCRIPTION)
                           var description: String,
                           @NotNull
                           @ColumnInfo(name = UPLOADED_FILES)
                           var uploadFiles: String

)
{


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = CHILD_EXPENSE_ID)
    var childExpenseId: Int=0
}