package com.coolappz.CuckooTechApp.data_base.expense_database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import org.jetbrains.annotations.NotNull

/**
 * Created by admin on 18-06-2018.
 */


const val CURRENCY_ID: String = "currencyId"
const val CURRENCY_TYPE_ID: String = "currencyTypeId"
const val CURRENCY_TYPE : String = "currencyType"
const val CURRENCY_TABLE:String ="currencyLog"
const val CURRENCY_COUNTRY:String="conversionRate"

@Entity(tableName = CURRENCY_TABLE,indices = arrayOf(Index(value = [CURRENCY_TYPE_ID, CURRENCY_TYPE], unique = true)))
class CurrencyTypeEntity (@NotNull
                          @ColumnInfo(name = CURRENCY_TYPE_ID)
                          var currencyTypeId:Int,
                          @NotNull
                          @ColumnInfo(name = CURRENCY_TYPE)
                          var currencyType:String,
                          @NotNull
                          @ColumnInfo(name = CURRENCY_COUNTRY)
                          var conversionRate:Double


){
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = CURRENCY_ID )
    var currencyId: Int=0
}
