package com.coolappz.CuckooTechApp.data_base.expense_database.child_expense

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.CLAIM_TOKEN

/**
 * Created by admin on 16-07-2018.
 */

@Dao
interface ChildExpenseDao {

    @Insert
    fun  insertChildExpDetails(childExpenseDetails: ChildExpenseDetails)

    @Query("SELECT * FROM" + " " + CHILD_EXPENSE_TABLE)
    fun getAllChildExpDetails():List<ChildExpenseDetails>

    @Query("SELECT * FROM" + " " + CHILD_EXPENSE_TABLE +" WHERE "+ CLAIM_TOKEN +" =:claimToken")
    fun getSameParentChildDetails(claimToken:String):MutableList<ChildExpenseDetails>

    @Query("SELECT * FROM" + " " + CHILD_EXPENSE_TABLE +" WHERE "+ CHILD_EXPENSE_ID +" =:childId")
    fun getSelectedChildDetails(childId:Int): ChildExpenseDetails

    @Query("SELECT  SUM("+CONVERTED_AMOUNT+") FROM " + CHILD_EXPENSE_TABLE +" WHERE "+ CLAIM_TOKEN +" =:claimToken")
    fun fetchTotalAmount(claimToken:String):Double


    @Query("UPDATE "+ CHILD_EXPENSE_TABLE +" SET "+ EXPENSE_CURRENCY_ID +" =:expenseCurrencyId, "
            + EXPENSE_TYPE_ID +"  =:expenseTypeId, "+ TO_DATE +" =:toDate, "+ FROM_DATE +" =:fromDate, "
            + CONVERSION_RATE +" =:conversionRate, "+ EXPENSE_AMOUNT +" =:expenseAmt, "+ CONVERTED_AMOUNT +" =:convertedAmt, "
            + DESCRIPTION +" =:description, "+ UPLOADED_FILES +" =:uploadFiles  WHERE "+ CHILD_EXPENSE_ID +" =:childId")
    fun updateChildDetails(expenseCurrencyId: Int,expenseTypeId: Int,toDate: String,fromDate: String, conversionRate: Double,
                           expenseAmt: Double, convertedAmt: Double, description: String,uploadFiles: String,childId:Int)

    @Query("DELETE  FROM "+ CHILD_EXPENSE_TABLE +" WHERE "+ CLAIM_TOKEN +" =:claimToken")
    fun deleteExpense(claimToken:String)

 }