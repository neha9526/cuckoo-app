package com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query


/**
 * Created by admin on 17-07-2018.
 */


@Dao
interface ParentDao {

    @Insert
    fun insertAllParentDetails(parentDetails: ParentExpenseDetails)

    @Query("SELECT * FROM " + CLAIM_TABLE+ " ORDER BY "+ CLAIM_DATE+ " DESC" )
    fun fetchAllParentDetails(): MutableList<ParentExpenseDetails>

    @Query("SELECT * FROM " + CLAIM_TABLE + " WHERE " + CLAIM_TOKEN + " =:claimToken ")
    fun fetchSingleParentDetails(claimToken: String): ParentExpenseDetails

    @Query("UPDATE "+CLAIM_TABLE+ " SET "+ CLAIM_FOR + " =:claimFor, " +
            CLAIM_DATE + " =:claimDate, " + CLAIM_CURRENCY_ID + " =:claimCurrencyId, " + TOTAL_CLAIM_AMOUNT + " =:totalClaimAmt, " +
            "" + CLAIM_STATUS + " =:claimStatus  WHERE "+ CLAIM_TOKEN+" =:claimToken"  )
    fun updateParent(claimToken: String, claimFor: String, claimDate: String, claimCurrencyId: Int,
                     totalClaimAmt: Double, claimStatus: String)

    @Query("UPDATE $CLAIM_TABLE SET $CLAIM_STATUS =:claimStatus  WHERE $CLAIM_ID =:claimId")
    fun updateWithClaimId(claimId:Int ,claimStatus: String)

     @Query("SELECT * FROM $CLAIM_TABLE WHERE $CLAIM_STATUS =:claimStatus ORDER BY "+ CLAIM_DATE+ " DESC")
     fun getStatusWiseList(claimStatus:String):MutableList<ParentExpenseDetails>

    @Query("SELECT "+ CLAIM_DATE +" FROM $CLAIM_TABLE ORDER BY $CLAIM_DATE DESC LIMIT 1")
    fun fetchMaxDate(): String

    @Query("SELECT "+ CLAIM_DATE +" FROM $CLAIM_TABLE ORDER BY $CLAIM_DATE ASC LIMIT 1")
    fun fetchMinDate(): String

    //query for date filter
    @Query("SELECT * FROM " + CLAIM_TABLE  + " WHERE " + CLAIM_DATE + " >= :fromDate " +
            " AND " + CLAIM_DATE + " <= :toDate ORDER BY "+ CLAIM_DATE + " DESC" )
    fun sortExpenseByDate(fromDate: String, toDate: String): MutableList<ParentExpenseDetails>

    @Query("SELECT * FROM " + CLAIM_TABLE  + " WHERE " + CLAIM_DATE + " >= :fromDate " +
            " AND " + CLAIM_DATE + " <= :toDate AND "+ CLAIM_STATUS+" =:status ORDER BY "+ CLAIM_DATE + " DESC" )
    fun sortExpenseByStatus(fromDate: String, toDate: String,status:String): MutableList<ParentExpenseDetails>

    @Query("DELETE  FROM "+ CLAIM_TABLE +" WHERE "+ CLAIM_TOKEN +" =:claimToken")
    fun deleteClaim(claimToken:String)

}