package com.coolappz.CuckooTechApp.data_base.tokenTable

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

/**
 * Created by admin on 07-04-2018.
 */

@Dao
interface TokenDao {

    @Query("SELECT * FROM $TOKEN_TABLE")
    fun getTokenDetails():TokenEntity

    @Insert
    fun insertTokenDetails(tokenEntity: TokenEntity)

    @Delete
    fun deleteTokenDetails(tokenEntity: TokenEntity)
}