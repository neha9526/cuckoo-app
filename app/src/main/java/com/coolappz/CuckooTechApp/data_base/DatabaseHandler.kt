package com.coolappz.CuckooTechApp.data_base

import android.content.Context
import com.coolappz.CuckooTechApp.data_base.attendanceDatabse.AttendanceEntity
import com.coolappz.CuckooTechApp.data_base.expense_database.child_expense.ChildExpenseDetails
import com.coolappz.CuckooTechApp.data_base.expense_database.CurrencyTypeEntity
import com.coolappz.CuckooTechApp.data_base.expense_database.child_expense.ExpenseTypeEntity
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.ParentExpenseDetails
import com.coolappz.CuckooTechApp.data_base.loginDatabase.LoginDetails
import com.coolappz.CuckooTechApp.data_base.menu_master.MenuEntity
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.BulkPunchPojo

/**
 * Created by admin on 09-02-2018.
 */

 class DatabaseHandler(context: Context) {

    private var  appDatabase= AppDatabase.DatabaseProvider.getInstance(context)
    private var userDao= appDatabase!!.loginDao()
    private var attendanceDao= appDatabase!!.attendanceDao()
    private var currencyDao= appDatabase!!.currencyDao()
    private var expenseDao = appDatabase!!.expenseTypeDao()
    private var childExpenseDao= appDatabase!!.childExpenseDao()
    private var parentExpenseDao= appDatabase!!.parentExpenseDao()
    private var menuMasterDao = appDatabase!!.menuMasterDao()

fun updateTokenTime(tokenNumber:String,timeStamp:String,empCode:String)
{
  return userDao.updateTokenTime(tokenNumber,timeStamp,empCode)
}
    fun deleteAttendance():Int
    {
      return attendanceDao.deleteExtraData()
    }
  /*fun deleteAll()
  {
      attendanceDao.deleteAttendance()
  }*/
//currency table queries
    fun  insertCurrencies(currencyTypeEntity: CurrencyTypeEntity)
    {
         currencyDao.insertAllCurrencies(currencyTypeEntity)
    }
    fun getCurrencyId(currencyType:String):Int
    {
        return  currencyDao.fetchCurrencyTypeId(currencyType)
    }
    fun getCurrencyName(currencyId:Int):String?
    {
       return currencyDao.fetchCurrencyName(currencyId)
    }
    fun getCurrencyConRate(currencyType:String):Double
    {
        return currencyDao.fetchCurrencyRate(currencyType)
    }
    //expenseType table queries

   fun insertExpenseTypes(expenseTypeEntity: ExpenseTypeEntity)
   {
       expenseDao.insertExpenseTypes(expenseTypeEntity)
   }

    fun getExpenseId(expenseType:String):Int
    {
       return expenseDao.fetchExpenseTypeId(expenseType)
    }
   fun  getAllExpenseTypes():List<String>?
   {
       return expenseDao.fetchAllExpenseType()
   }

    fun getExpenseType(expenseTypeId:Int):String?
    {
        return expenseDao.fetchExpenseType(expenseTypeId)
    }
//childExpense table queries
    fun  insertAllChildDetails(childExpenseDetails: ChildExpenseDetails)
{
    childExpenseDao.insertChildExpDetails(childExpenseDetails)
}

    fun deleteChildExpense(claimToken:String)
    {
        childExpenseDao.deleteExpense(claimToken)
    }

    fun fetchExpenseSum(claimToken:String):Double
    {
        return childExpenseDao.fetchTotalAmount(claimToken)
    }
    fun getSingleChildDetails(childId:Int): ChildExpenseDetails
    {
        return childExpenseDao.getSelectedChildDetails(childId)
    }

    fun updateSingleChildDetails(expenseCurrencyId: Int,expenseTypeId: Int,toDate: String,fromDate: String, conversionRate: Double,
                           expenseAmt: Double, convertedAmt: Double, description: String,uploadFiles: String,childId:Int)
    {
        childExpenseDao.updateChildDetails(expenseCurrencyId,expenseTypeId,toDate,fromDate,conversionRate,expenseAmt,
                convertedAmt,description,uploadFiles,childId)
    }


    fun getChildDetailsSameProj(claimToken:String):MutableList<ChildExpenseDetails>
    {
        return childExpenseDao.getSameParentChildDetails(claimToken)
    }
    //ParentExpense table queries

    fun getSortedExpStatus(fromDate: String, toDate: String,status:String):MutableList<ParentExpenseDetails>
    {
        return parentExpenseDao.sortExpenseByStatus(fromDate,toDate,status)
    }
    fun getSortedExpense(fromDate: String, toDate: String):MutableList<ParentExpenseDetails>
    {
        return parentExpenseDao.sortExpenseByDate(fromDate,toDate)
    }
    fun insertParentDetails(parentExpenseDetails: ParentExpenseDetails)
    {
        parentExpenseDao.insertAllParentDetails(parentExpenseDetails)
    }
    fun getAllProjectList():MutableList<ParentExpenseDetails>
    {

        return parentExpenseDao.fetchAllParentDetails()
    }
    fun getStatusWiseList(claimStatus:String):MutableList<ParentExpenseDetails>
    {
        return parentExpenseDao.getStatusWiseList(claimStatus)
    }
    fun updateParent(claimToken: String, claimFor: String, claimDate: String, claimCurrencyId: Int,
                           totalClaimAmt: Double, claimStatus: String)
    {
        parentExpenseDao.updateParent(claimToken, claimFor, claimDate, claimCurrencyId,
                totalClaimAmt, claimStatus)
    }
    fun deleteClaimDetail(claimToken:String)
    {
        parentExpenseDao.deleteClaim(claimToken)
    }
    fun getSingleParentDetail(claimToken:String):ParentExpenseDetails
    {
        return parentExpenseDao.fetchSingleParentDetails(claimToken)
    }

    fun fetchMaxDate():String?
    {
        return parentExpenseDao.fetchMaxDate()
    }
    fun fetchMinDate():String?
    {
        return parentExpenseDao.fetchMinDate()
    }
    //Menu master queries
    fun fetchMenuDetails(): List<MenuEntity>
    {
        return menuMasterDao.getMenuDetails()
    }

    fun insertMenuDetails(menuEntity: MenuEntity)
    {
        menuMasterDao.insertMenuDetails(menuEntity)
    }

    fun fetchMenus():List<String>
    {
        return menuMasterDao.getMenuNames()
    }

    fun updateMenuFlag(menuName:String,menuFlag:Int)
    {
        menuMasterDao.updateMenuFlag(menuName,menuFlag)
    }

    //Attendance table queries
    fun getCountOfValid():Int
    {
        return attendanceDao.getValidRecordCount()
    }
    fun getCountOfInvalid():Int
    {
        return attendanceDao.getInvalidRecordCount()
    }
    fun getCountOfOffline():Int
    {
        return attendanceDao.getOfflineRecordCount()
    }
     fun updateAppType(appType:String,empCode:String)
     {
         userDao.updateAppType(appType,empCode)
     }
    fun addToDatabase(loginDetails: LoginDetails)
    {
        userDao.insertAll(loginDetails)
    }
     fun fetchFromDatabase(): LoginDetails?
     {
         return userDao.getAll()
     }
    fun getMaxDate():AttendanceEntity
    {
        return attendanceDao.fetchMaxDate()
    }
    fun getMinDate():AttendanceEntity
    {
        return attendanceDao.fetchMinDate()
    }
    fun addAttendanceDetails(attendanceEntity: AttendanceEntity)
    {
        attendanceDao.insertAllAttendance(attendanceEntity)
    }

    fun getAllAttendanceDetails(): List<AttendanceEntity>
    {
       return attendanceDao.getAllAttendance()
    }

   fun getBulkPunchDetails():List<AttendanceEntity>
   {
       return attendanceDao.getBulkPunchList()
   }
    fun updateSyncStatus(bulkSyncList: List<BulkPunchPojo>)
    {
        for(items in bulkSyncList)
        {
            attendanceDao.updateStatusOnSync(items.syncStatus!!, items.isValid!!, items.id)
        }

    }
    fun sortByGivenDate(fromDate:String,toDate:String):List<AttendanceEntity>
    {
       return attendanceDao.sortByDate(fromDate,toDate)
    }

}