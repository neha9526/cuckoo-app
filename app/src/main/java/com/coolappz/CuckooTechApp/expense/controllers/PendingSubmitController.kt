package com.coolappz.CuckooTechApp.expense.controllers

import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bluelinelabs.conductor.Controller
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.ParentExpenseDetails
import com.coolappz.CuckooTechApp.expense.addexpense.AddExpenseDetailsActivity
import com.coolappz.CuckooTechApp.expense.addexpense.OnItemClickListener
import com.coolappz.CuckooTechApp.expense.claim_expense.ExpenseAdapter
import com.coolappz.CuckooTechApp.utility.MessageEvent
import com.coolappz.CuckooTechApp.utility.PendingEvent
import kotlinx.android.synthetic.main.controller_approved.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


/**
 * Created by admin on 30-01-2018.
 */
class PendingSubmitController : Controller(), OnItemClickListener {
    var databaseHandler: DatabaseHandler? = null
    var pendingProjList: MutableList<ParentExpenseDetails>? = null
    var linearLayoutManager: LinearLayoutManager? = null
    var pendingListView: RecyclerView? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val rootView: View = inflater.inflate(R.layout.controller_approved, container, false)
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        return rootView
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        if (databaseHandler == null || linearLayoutManager == null || pendingListView == null) {
            databaseHandler = DatabaseHandler(applicationContext!!)
            pendingListView = view.pendingSubmitList
            linearLayoutManager = LinearLayoutManager(applicationContext)
            linearLayoutManager!!.isSmoothScrollbarEnabled = true
            pendingListView!!.layoutManager = linearLayoutManager
        }

        pendingProjList = databaseHandler!!.getStatusWiseList(applicationContext!!.getString(R.string.pending))
        val expenseAdapter = ExpenseAdapter(pendingProjList!!, this, databaseHandler!!, activity as Context)
        pendingListView!!.adapter = expenseAdapter
        expenseAdapter.notifyDataSetChanged()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onPendingDateFilter(pendingEvent: PendingEvent) {
        val adapterFilter = ExpenseAdapter(pendingEvent.pendingList, this, databaseHandler!!, activity as Context)
        adapterFilter.notifyDataSetChanged()
        pendingListView!!.adapter = adapterFilter
        if (pendingEvent.pendingList.isNotEmpty() && pendingEvent.from == applicationContext!!.getString(R.string.controller)) {

            Toast.makeText(applicationContext, applicationContext!!.getString(R.string.claim_list_updated),
                    Toast.LENGTH_SHORT).show()
        } else if( pendingEvent.from ==applicationContext!!.getString(R.string.controller)) {
            Toast.makeText(applicationContext, applicationContext!!.getString(R.string.data_not_available_for_dates),
                    Toast.LENGTH_SHORT).show()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }


    override fun onItemClick(position: Int) {
        val intent = Intent(applicationContext, AddExpenseDetailsActivity::class.java)
        EventBus.getDefault().postSticky(MessageEvent(pendingProjList!![position].claimStatus,
                pendingProjList!![position].claimToken))
        startActivity(intent)

    }

}