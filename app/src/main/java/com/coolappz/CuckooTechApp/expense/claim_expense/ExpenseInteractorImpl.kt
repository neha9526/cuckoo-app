package com.coolappz.CuckooTechApp.expense.claim_expense

import android.content.Context
import android.util.Log
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.network.Api
import com.coolappz.CuckooTechApp.network.ApiFail
import com.coolappz.CuckooTechApp.network.ApiSuccess
import com.coolappz.CuckooTechApp.network.HttpErrorResponse
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.SaveExpResPojo
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor
import okhttp3.MultipartBody
import okhttp3.RequestBody
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by admin on 10-03-2018.
 */
class ExpenseInteractorImpl: ExpenseInteractor {
    override fun saveExpenseDetails(context: Context, expDetailPojo: RequestBody, fileArray: ArrayList<MultipartBody.Part>,
                                    interfaceInteractor: InterfaceInteractor<SaveExpResPojo>) {
        Api.userManagement().submitExpenseClaim(expDetailPojo,fileArray)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiSuccess<SaveExpResPojo>() {
                    override fun call(response: SaveExpResPojo) {
                        when {response.response!!.isSuccessed!! -> {
                            interfaceInteractor.successApi(response)
                        }
                            else -> {
                                interfaceInteractor.failApi(response.response.errorMessage!!)
                            }
                        }
                    }

                }, object : ApiFail() {
                    override fun noNetworkError() {
                        interfaceInteractor.noInternet()
                    }

                    override fun unknownError(e: Throwable) {
                        Log.e("TAGError","unknown Error "+e.printStackTrace())
                        interfaceInteractor.failApi(context.getString(R.string.unknown_error_msg))
                    }

                    override fun httpStatus(response: HttpErrorResponse) {
                        Log.e("TAGError","HttpStatus "+response.error)
                        interfaceInteractor.failApi(context.getString(R.string.http_status_error_msg))
                    }

                })
    }


}