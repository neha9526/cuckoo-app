package com.coolappz.CuckooTechApp.expense.claim_expense

import android.content.Context
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.SaveExpResPojo
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor
import okhttp3.MultipartBody
import okhttp3.RequestBody

/**
 * Created by admin on 10-03-2018.
 */
interface ExpenseInteractor {

    fun saveExpenseDetails(context: Context, expDetailPojo: RequestBody, fileArray:ArrayList<MultipartBody.Part>,
                           interfaceInteractor: InterfaceInteractor<SaveExpResPojo>)
}