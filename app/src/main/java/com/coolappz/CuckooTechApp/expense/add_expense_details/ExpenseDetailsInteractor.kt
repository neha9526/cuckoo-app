package com.coolappz.CuckooTechApp.expense.add_expense_details

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo.ExpenseTypeRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.expense_type.ExpenseTypeResPojo
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor

/**
 * Created by admin on 30-06-2018.
 */
interface ExpenseDetailsInteractor {

    fun expenseTypeDetails(context: Context, expenseTypeRequestPojo: ExpenseTypeRequestPojo,
                           interfaceInteractor: InterfaceInteractor<ExpenseTypeResPojo>)
}