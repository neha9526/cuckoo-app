package com.coolappz.CuckooTechApp.expense.claim_expense

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.about_us.AboutUSActivity
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.expense_database.child_expense.ChildExpenseDetails
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.ParentExpenseDetails
import com.coolappz.CuckooTechApp.expense.addexpense.AddExpenseDetailsActivity
import com.coolappz.CuckooTechApp.expense.controllers.ExpenseController
import com.coolappz.CuckooTechApp.faq.FaqActivity
import com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo.ChildExpenseRequest
import com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo.ParentExpenseRequest
import com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo.SaveExpRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.SaveExpeResObj
import com.coolappz.CuckooTechApp.utility.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_base_app_compat.*
import kotlinx.android.synthetic.main.activity_expense.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class ExpenseActivity : BaseAppCompatActivity<ExpenseView<SaveExpeResObj>, ExpensePresenterImpl>(),
        ExpenseView<SaveExpeResObj>{
    override val layoutIdForInjection: Int = R.layout.activity_expense

    lateinit var databaseHandler: DatabaseHandler
    override fun createPresenter(): ExpensePresenterImpl {
        presenter = ExpensePresenterImpl(this)
        return presenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenSubTitle.text = getString(R.string.claim_exp)
        baseEmpCodeLayout.visibility = View.VISIBLE
        databaseHandler = DatabaseHandler(this)
        baseEmpCode.text = databaseHandler.fetchFromDatabase()!!.employeeCode
        offlineSync()
        val frameLayout: ViewGroup = expenseFramelayout
        val router: Router = Conductor.attachRouter(this, frameLayout, savedInstanceState)
        if (!router.hasRootController()) {
            router.setRoot(RouterTransaction.with(ExpenseController()))
        }


        expenseFaq.setOnClickListener {
            val intent = Intent(this, FaqActivity::class.java)
            startActivity(intent)
        }
        expenseAboutUs.setOnClickListener {
            val intent = Intent(this, AboutUSActivity::class.java)
            startActivity(intent)
        }
        fabAddExpense.setOnClickListener() {
            if (Preferences.getClaimToken(this) == "") {
                val currentTimeStamp = Date(System.currentTimeMillis())
                val projectToken: String = DateHelper.SAVE_FILE_FORMAT_FULL_DATE.format(currentTimeStamp)
                Preferences.setClaimToken(this, projectToken)
            }
            val intent = Intent(this, AddExpenseDetailsActivity::class.java)
            startActivity(intent)
        }
    }

    override fun noInternet() {}

    override fun success(response: SaveExpeResObj) {
        Toast.makeText(this,getString(R.string.offline_sync_claim),Toast.LENGTH_LONG).show()
        val status = if(response.isValid==0) getString(R.string.invalid) else getString(R.string.submitted)
        databaseHandler.updateParent(response.expAppId!!,response.claimFor!!,DateHelper.getDateTimeNew(System.currentTimeMillis()),
                response.claimCurrId!!,response.amount!!,status)
        val updatedList:MutableList<ParentExpenseDetails> =databaseHandler.getAllProjectList()
        EventBus.getDefault().post(EventDateFilter(updatedList,getString(R.string.activity)))
    }

    override fun fail(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        claimProgressLayout.visibility=View.VISIBLE
    }

    override fun hideLoading() {
        claimProgressLayout.visibility=View.GONE
    }


  private fun offlineSync()
  {
      if (CuckooUtil.Util.isNetworkAvailable(this)) {
          val loginDetails = databaseHandler.fetchFromDatabase()!!
          val offlineList: List<ParentExpenseDetails>? = databaseHandler.getStatusWiseList(getString(R.string.submitted_offline))
          if (offlineList!!.isNotEmpty()) {
              for (item in offlineList) {
                  val childEntryObj: ArrayList<ChildExpenseRequest> = ArrayList()
                  val requestObj: ArrayList<ParentExpenseRequest> = ArrayList()
                  val fileArray: ArrayList<MultipartBody.Part> = ArrayList()
                  val childList: List<ChildExpenseDetails> = databaseHandler.getChildDetailsSameProj(item.claimToken)

                  for (child in childList) {
                      val uploadFile = if (child.uploadFiles != "") child.childExpenseId.toString() else ""
                      childEntryObj.add(ChildExpenseRequest(child.childExpenseId.toString(),
                              child.expenseTypeId.toString(), child.description, child.fromDate, child.toDate, child.expenseCurrencyId.toString(),
                              child.expenseAmt.toString(), child.conversionRate.toString(), child.convertedAmt.toString(), uploadFile))
                      if (child.uploadFiles.isNotEmpty()) {
                          val docFile = File(child.uploadFiles)
                          val requestFile = RequestBody.create(MediaType.parse("application/octet-stream"), docFile)
                          fileArray.add(MultipartBody.Part.createFormData(child.childExpenseId.toString(), docFile.name, requestFile))
                      }
                  }

                  requestObj.add(ParentExpenseRequest(loginDetails.companyCode, loginDetails.appType, loginDetails.employeeCode,
                          loginDetails.tokenNumber, item.claimToken, "0", item.claimFor, item.claimCurrencyId.toString(),
                          item.totalClaimAmt.toString(), childEntryObj))
                  val expJsonBody = RequestBody.create(MediaType.parse("text/plain"),
                          Gson().toJson(SaveExpRequestPojo(requestObj)))
                  presenter.saveExpenseDetails(this,expJsonBody,fileArray)
              }
              Toast.makeText(this,getString(R.string.offline_sync_claim),Toast.LENGTH_LONG).show()
          }

      }
  }

}

