package com.coolappz.CuckooTechApp.expense.add_expense_details

import android.content.Context
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.network.Api
import com.coolappz.CuckooTechApp.network.ApiFail
import com.coolappz.CuckooTechApp.network.ApiSuccess
import com.coolappz.CuckooTechApp.network.HttpErrorResponse
import com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo.ExpenseTypeRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.expense_type.ExpenseTypeResPojo
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by admin on 30-06-2018.
 */
class ExpenseDetailsInteactorImpl: ExpenseDetailsInteractor {

    override fun expenseTypeDetails(context: Context, expenseTypeRequestPojo:
    ExpenseTypeRequestPojo, interfaceInteractor: InterfaceInteractor<ExpenseTypeResPojo>) {
        Api.userManagement().getExpenseTypeDetails(expenseTypeRequestPojo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiSuccess<ExpenseTypeResPojo>() {
                    override fun call(response: ExpenseTypeResPojo) {
                        when {
                            response.response!!.isSucceeded!! -> {
                                interfaceInteractor.successApi(response)

                            }
                            else -> {
                                interfaceInteractor.failApi(response.response.responseMessage!!)
                            }
                        }
                    }

                }, object : ApiFail() {
                    override fun noNetworkError() {
                        interfaceInteractor.noInternet()
                    }

                    override fun unknownError(e: Throwable) {
                        interfaceInteractor.failApi(context.getString(R.string.unknown_error_msg))
                    }

                    override fun httpStatus(response: HttpErrorResponse) {
                        interfaceInteractor.failApi(context.getString(R.string.http_status_error_msg))
                    }

                })
    }
}