package com.coolappz.CuckooTechApp.expense.controllers

import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bluelinelabs.conductor.Controller
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.ParentExpenseDetails
import com.coolappz.CuckooTechApp.expense.addexpense.AddExpenseDetailsActivity
import com.coolappz.CuckooTechApp.expense.addexpense.OnItemClickListener
import com.coolappz.CuckooTechApp.expense.claim_expense.ExpenseAdapter
import com.coolappz.CuckooTechApp.utility.InvalidEvent
import com.coolappz.CuckooTechApp.utility.MessageEvent
import kotlinx.android.synthetic.main.controller_rejected.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Created by admin on 30-01-2018.
 */
class InvalidController : Controller(), OnItemClickListener {
    var databaseHandler: DatabaseHandler? = null
    var invalidProjList: MutableList<ParentExpenseDetails>? = null
    var invalidRecyclerView: RecyclerView? = null
    var linearLayoutManager: LinearLayoutManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val rootView: View = inflater.inflate(R.layout.controller_rejected, container, false)
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        return rootView
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        if(databaseHandler==null ||invalidRecyclerView ==null || linearLayoutManager==null )
        {
            databaseHandler = DatabaseHandler(applicationContext!!)
            invalidRecyclerView = view.invalidList
            linearLayoutManager = LinearLayoutManager(applicationContext)
            linearLayoutManager!!.isSmoothScrollbarEnabled = true
            invalidRecyclerView!!.layoutManager = linearLayoutManager

        }

        invalidProjList = databaseHandler!!.getStatusWiseList(applicationContext!!.getString(R.string.invalid))
        invalidRecyclerView!!.adapter = ExpenseAdapter(invalidProjList!!, this, databaseHandler!!, activity as Context)

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onInvalidDateFilter(invalidEvent: InvalidEvent) {
        val adapterFilter = ExpenseAdapter(invalidEvent.invalidList, this, databaseHandler!!, activity as Context)
        adapterFilter.notifyDataSetChanged()
        invalidRecyclerView!!.adapter = adapterFilter
        if (invalidEvent.invalidList.isNotEmpty()) {
            Toast.makeText(applicationContext, applicationContext!!.getString(R.string.claim_list_updated),
                    Toast.LENGTH_SHORT).show()
        } else {

            Toast.makeText(applicationContext, applicationContext!!.getString(R.string.data_not_available_for_dates),
                    Toast.LENGTH_SHORT).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    override fun onItemClick(position: Int) {

        val intent = Intent(applicationContext, AddExpenseDetailsActivity::class.java)
        EventBus.getDefault().postSticky(MessageEvent(invalidProjList!![position].claimStatus,
                invalidProjList!![position].claimToken))
        startActivity(intent)

    }
}
