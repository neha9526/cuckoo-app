package com.coolappz.CuckooTechApp.expense.controllers

import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bluelinelabs.conductor.Controller
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.ParentExpenseDetails
import com.coolappz.CuckooTechApp.expense.claim_expense.ExpenseAdapter
import com.coolappz.CuckooTechApp.expense.addexpense.AddExpenseDetailsActivity
import com.coolappz.CuckooTechApp.expense.addexpense.OnItemClickListener
import com.coolappz.CuckooTechApp.utility.EventDateFilter
import com.coolappz.CuckooTechApp.utility.MessageEvent
import kotlinx.android.synthetic.main.controller_all.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Created by admin on 30-01-2018.
 */
class AllController : Controller(), OnItemClickListener {
    var databaseHandler: DatabaseHandler? =null
    lateinit var allProjList: MutableList<ParentExpenseDetails>
    var allRecyclerView: RecyclerView? = null
    var linearLayoutManager:LinearLayoutManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val rootView: View = inflater.inflate(R.layout.controller_all, container, false)
        if(!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this)
        }
        return rootView
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        if(databaseHandler==null || allRecyclerView == null || linearLayoutManager==null)
        {
            databaseHandler = DatabaseHandler(applicationContext!!)
            allProjList = databaseHandler!!.getAllProjectList()
            allRecyclerView = view.allList
            val linearLayoutManager = LinearLayoutManager(applicationContext)
            linearLayoutManager.isSmoothScrollbarEnabled = true
            allRecyclerView!!.layoutManager = linearLayoutManager
        }
        val allAdapter = ExpenseAdapter(allProjList, this,databaseHandler!!,activity as Context)
        allRecyclerView!!.adapter = allAdapter
        allAdapter.notifyDataSetChanged()


    }

    override fun setTargetController(target: Controller?) {
        super.setTargetController(target)
        Toast.makeText(applicationContext, applicationContext!!.getString(R.string.claim_list_updated),
                Toast.LENGTH_SHORT).show()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onAllDateFilter(eventDateFilter: EventDateFilter)
    {
        val adapterFilter= ExpenseAdapter(eventDateFilter.claimList, this,databaseHandler!!,activity as Context)
        adapterFilter.notifyDataSetChanged()
        allRecyclerView!!.adapter = adapterFilter
        if(eventDateFilter.claimList.isNotEmpty() &&
                eventDateFilter.from.contentEquals(applicationContext!!.getString(R.string.controller)))
        {
            Toast.makeText(applicationContext, applicationContext!!.getString(R.string.claim_list_updated),
                    Toast.LENGTH_SHORT).show()
        }
        else if(eventDateFilter.from.contentEquals(applicationContext!!.getString(R.string.controller))){
            Toast.makeText(applicationContext, applicationContext!!.getString(R.string.data_not_available_for_dates),
                    Toast.LENGTH_SHORT).show()
        }

    }



    override fun onDestroy() {
        super.onDestroy()
        if(EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().unregister(this)
        }
    }


    override fun onItemClick(position: Int) {

        val intent = Intent(applicationContext, AddExpenseDetailsActivity::class.java)
        EventBus.getDefault().postSticky(MessageEvent(allProjList[position].claimStatus, allProjList[position].claimToken))
        startActivity(intent)

    }
}
