package com.coolappz.CuckooTechApp.expense.add_expense_details

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo.ExpenseTypeRequestPojo

/**
 * Created by admin on 30-06-2018.
 */
interface ExpenseDetailsPresenter {

    fun getExpenseType(context: Context,expenseTypeRequestPojo: ExpenseTypeRequestPojo)
}