package com.coolappz.CuckooTechApp.expense.addexpense

import android.content.Context
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.SaveExpResPojo

import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.SaveExpeResObj
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import okhttp3.MultipartBody
import okhttp3.RequestBody
/**
 * Created by admin on 10-03-2018.
 */
class AddExpensePresenterImpl(var view: AddExpenseView<SaveExpeResObj>?):
        MvpBasePresenter<AddExpenseView<SaveExpeResObj>>(), AddExpensePresenter, InterfaceInteractor<SaveExpResPojo> {

    private var addExpenseInteractor=AddExpenseInteractorImpl()

    override fun onDestroyView() {
        view=null
    }

    override fun successApi(respone:SaveExpResPojo) {
        if(view!=null)
        {
            view!!.hideLoading()
            view!!.success(respone.response)
        }

    }
    override fun saveExpenseDetails(context: Context, expDetailPojo: RequestBody, docArray: ArrayList<MultipartBody.Part>) {
        view?.showLoading()
        addExpenseInteractor.saveExpenseDetails(context,expDetailPojo,docArray,this)
    }

    override fun failApi(message: String) {
        if(view!=null)
        {
            view?.hideLoading()
            view?.fail(message)
        }
    }

    override fun noInternet() {
        if(view!=null)
        {
            view?.hideLoading()
            view?.noInternet()
        }
    }

    override fun tokenExpire() {
        if(view!=null)
        {
            view?.hideLoading()

        }
    }

}
