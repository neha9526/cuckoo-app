package com.coolappz.CuckooTechApp.expense.claim_expense

import android.content.Context
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Button
import android.widget.TextView
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.ParentExpenseDetails
import com.coolappz.CuckooTechApp.expense.addexpense.OnItemClickListener
import com.coolappz.CuckooTechApp.utility.DateHelper
import com.coolappz.CuckooTechApp.utility.EventDateFilter
import com.coolappz.CuckooTechApp.utility.PendingEvent
import kotlinx.android.synthetic.main.adapter_expense.view.*
import org.greenrobot.eventbus.EventBus

/**
 * Created by admin on 30-01-2018.
 */
class ExpenseAdapter(var expenseList:MutableList<ParentExpenseDetails>,var onItemClimObj:OnItemClickListener,
                     var databaseHandler: DatabaseHandler,var context: Context)
    :RecyclerView.Adapter<ExpenseAdapter.MyViewHolder>() {

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        if(expenseList[position].claimStatus==context.getString(R.string.pending))
        {
            holder!!.expenseDelete.visibility=View.VISIBLE
        }
        holder!!.claimFor.text= expenseList[position].claimFor
        holder.claimAmount.text= expenseList[position].totalClaimAmt.toString()
        holder.status.text= expenseList[position].claimStatus
        holder.dateText.text = DateHelper.getFormattedDay(expenseList[position].claimDate)
        holder.monthText.text = DateHelper.getFormattedMonth(expenseList[position].claimDate)
        holder.yearText.text =  DateHelper.getFormattedYear(expenseList[position].claimDate)
        holder.linearLayout.setOnClickListener()
        {
          onItemClimObj.onItemClick(position)
        }
         holder.expenseDelete.setOnClickListener()
         {
             val inflater = LayoutInflater.from(context)
             val dialogView = inflater.inflate(R.layout.auto_date_time_popup, null)
             val alertTitle = dialogView.findViewById(R.id.alertTitle) as TextView
             val alertText = dialogView.findViewById(R.id.alertMsg) as TextView
             val btnAllow = dialogView.findViewById(R.id.btnAlertAllow) as Button
             val btnCancel = dialogView.findViewById(R.id.btnAlertCancel) as Button

             val dateBuilder = AlertDialog.Builder(context)
             dateBuilder.setView(dialogView)
             alertTitle.visibility = View.GONE
             alertText.gravity = Gravity.CENTER
             btnAllow.text = context.getString(R.string.yes)
             btnCancel.text = context.getString(R.string.no)
             alertText.text = context.getString(R.string.delete_warning)
             val alertDialog = dateBuilder.create()
             alertDialog.show()

             btnCancel.setOnClickListener()
             {
                 alertDialog.dismiss()
             }

             btnAllow.setOnClickListener()
             {
                 alertDialog.dismiss()
                 databaseHandler.deleteClaimDetail(expenseList[position].claimToken)
                 databaseHandler.deleteChildExpense(expenseList[position].claimToken)
                //for updating screens on Delete functionality
                 EventBus.getDefault()
                         .post(EventDateFilter(databaseHandler.getAllProjectList(),"Adapter"))
                 EventBus.getDefault()
                         .post(PendingEvent(databaseHandler.getStatusWiseList(context.getString(R.string.pending)),"Adapter"))
             }

         }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        val viewHolder = LayoutInflater.from(parent!!.context).inflate(R.layout.adapter_expense,
                parent, false)
        return MyViewHolder(viewHolder)
    }

    override fun getItemCount(): Int {
        return expenseList.size
    }

    class MyViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {
        var claimFor=itemView.mainClaimFor
        var claimAmount=itemView.claimAmount
        var status=itemView.expenseStatus
        var dateText= itemView.expenseDate
        var monthText = itemView.expenseMonth
        var yearText = itemView.expenseYear
        var linearLayout = itemView.containerLayout
        var expenseDelete = itemView.deleteExpense

    }

}