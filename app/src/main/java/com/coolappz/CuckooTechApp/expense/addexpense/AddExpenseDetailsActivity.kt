package com.coolappz.CuckooTechApp.expense.addexpense

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.expense_database.CurrencyTypeEntity
import com.coolappz.CuckooTechApp.data_base.expense_database.child_expense.ChildExpenseDetails
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.ParentExpenseDetails
import com.coolappz.CuckooTechApp.data_base.loginDatabase.LoginDetails
import com.coolappz.CuckooTechApp.expense.add_expense_details.AddExpenseActivity
import com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo.ChildExpenseRequest
import com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo.ParentExpenseRequest
import com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo.SaveExpRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.SaveExpeResObj
import com.coolappz.CuckooTechApp.utility.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_add_expense_claim.*
import kotlinx.android.synthetic.main.activity_base_app_compat.*
import kotlinx.android.synthetic.main.textviewforspinner.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class AddExpenseDetailsActivity : BaseAppCompatActivity<AddExpenseView<SaveExpeResObj>, AddExpensePresenterImpl>(),
        AddExpenseView<SaveExpeResObj>, OnItemClickListener, AdapterView.OnItemSelectedListener {

    override val layoutIdForInjection: Int = R.layout.activity_add_expense_claim

    private var chilExpenseList: MutableList<ChildExpenseDetails> = ArrayList()
    private lateinit var currencyTypeList: ArrayList<String>
    private lateinit var adapterAddExpense: AdapterAddExpense
    private var claimToken = ""
    private var currencyValue: String = ""
    private var isEventFired: Boolean = false
    private lateinit var logData: LoginDetails
    private val databaseHandler = DatabaseHandler(this)
    private var parentDetail:ParentExpenseDetails?=null


    override fun createPresenter(): AddExpensePresenterImpl {
        presenter = AddExpensePresenterImpl(this)
        return presenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        screenSubTitle.text = getString(R.string.add_expense_claim)
        addExpenseRecycler.layoutManager = LinearLayoutManager(this)
        if (databaseHandler.getCurrencyName(100).isNullOrEmpty()) {
            databaseHandler.insertCurrencies(CurrencyTypeEntity(100, "INR", 1.0))
        }

        logData = databaseHandler.fetchFromDatabase()!!
        tvClaimFor.text = CuckooUtil.Util.spannableTextView(getString(R.string.claim_for))
        tvCurrencySpinner.text = CuckooUtil.Util.spannableTextView(getString(R.string.claim_currency))

        /*   val requisitionIdList = arrayOf("Select Expense Type", "AFCCS", "SFHGAFH", "SXVHS")
           val requisitionIdAdapter = ArrayAdapter(this, R.layout.textviewforspinner, requisitionIdList)
           requisitionSpinner.adapter = requisitionIdAdapter*/
        currencyTypeList = arrayListOf("INR")
        val currencyAdapter = ArrayAdapter(this, R.layout.textviewforspinner, currencyTypeList)
        currencySpinner.adapter = currencyAdapter
        currencySpinner.onItemSelectedListener = this
        claimToken = Preferences.getClaimToken(this)
        etClaimFor.onFocusChangeListener = View.OnFocusChangeListener { view: View, b: Boolean ->
            Preferences.setChangeFlag(this, true)
        }
        fabExpense.setOnClickListener()
        {
            val intent = Intent(this, AddExpenseActivity::class.java)
            intent.putExtra(getString(R.string.claimToken), claimToken)
            startActivity(intent)
        }

        expenseSave.setOnClickListener() {
            saveSubmit(getString(R.string.save))
        }

        expenseSubmit.setOnClickListener()
        {
            saveSubmit(getString(R.string.submit))
        }
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onClaimDetailSelected(msgEvent: MessageEvent) {
        claimToken = msgEvent.claimToken
        parentDetail = databaseHandler.getSingleParentDetail(claimToken)
        etClaimFor.setText(parentDetail!!.claimFor)
        for ((index, value) in currencyTypeList.withIndex()) {
            if (value == databaseHandler.getCurrencyName(parentDetail!!.claimCurrencyId) && index != 0) {
                currencySpinner.setSelection(index)
            }
        }
        claimAmount.setText(parentDetail!!.totalClaimAmt.toString())
        chilExpenseList = databaseHandler.getChildDetailsSameProj(claimToken)
        noExpenseDetailView.visibility = View.GONE
        noExpenseDetailText.visibility = View.GONE
        addExpenseRecycler.visibility = View.VISIBLE
        when (msgEvent.status) {
            getString(R.string.submitted_offline), getString(R.string.submitted) -> {
                isEventFired = true
                fabExpense.visibility = View.GONE
                etClaimFor.isEnabled = false
                currencySpinner.isEnabled = false
                expenseButtonLayout.visibility = View.GONE
                val scale = resources.displayMetrics.density
                val dpAsPixels = (10 * scale + 0.5f).toInt()
                addExpenseRecycler.setPadding(0, 0, 0, dpAsPixels)
                val lp = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
                lp.setMargins(0, 0, 0, dpAsPixels)
                expenseScroll.layoutParams = lp
                adapterAddExpense = AdapterAddExpense(chilExpenseList, this, this, databaseHandler, isEventFired)

            }
            else -> {
                adapterAddExpense = AdapterAddExpense(chilExpenseList, this, this, databaseHandler, isEventFired)
            }
        }
        addExpenseRecycler.adapter = adapterAddExpense

    }

    //save submit function for calling API and storing database in DB
    private fun saveSubmit(funName: String) {
        var flag = true
        if (etClaimFor.text.toString().trim().isEmpty()) {
            flag = false
            etClaimFor.error = getString(R.string.enter_claim_for)
        }
        if (currencyValue == "") {
            flag = false
            Toast.makeText(this, getString(R.string.claim_currency_error), Toast.LENGTH_SHORT).show()
        }
        if (chilExpenseList.isEmpty()) {
            flag = false
            Toast.makeText(this, getString(R.string.enter_expense_details), Toast.LENGTH_SHORT).show()
        }
        if (flag) {
            val parentObj: ParentExpenseDetails? = databaseHandler.getSingleParentDetail(claimToken)
            val currencyId = databaseHandler.getCurrencyId(currencyValue)
            Preferences.setClaimToken(this, "")
            if (CuckooUtil.Util.isNetworkAvailable(this) && funName == getString(R.string.submit)) {
                postExpenseClaim(currencyId, parentObj)
            } else if (!CuckooUtil.Util.isNetworkAvailable(this) && funName == getString(R.string.submit)) {
                updateInsert(parentObj, currencyId, getString(R.string.submitted_offline))
            } else {
                updateInsert(parentObj, currencyId, getString(R.string.pending))
            }

        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        /* if (position == 0) {
             parent?.getChildAt(position)?.spinnerTextview?.setTextColor(ContextCompat.getColor(this, R.color.silver_grey))
         } else {*/
        parent?.getChildAt(position)?.spinnerTextview?.setTextColor(ContextCompat.getColor(this, R.color.black))
        currencyValue = parent!!.getItemAtPosition(position).toString()
        // }

    }

    override fun onResume() {
        super.onResume()
        if (claimToken.isNotEmpty() && !isEventFired) {
            chilExpenseList = databaseHandler.getChildDetailsSameProj(claimToken)
            if (chilExpenseList.isNotEmpty()) {
                noExpenseDetailView.visibility = View.GONE
                noExpenseDetailText.visibility = View.GONE
                addExpenseRecycler.visibility = View.VISIBLE
                layoutClaimAmt.visibility = View.VISIBLE
                adapterAddExpense = AdapterAddExpense(chilExpenseList, this, this,
                        databaseHandler, false)
                addExpenseRecycler.adapter = adapterAddExpense
                claimAmount.setText(databaseHandler.fetchExpenseSum(claimToken).toString())
                adapterAddExpense.notifyDataSetChanged()
            }
        }

    }

    override fun noInternet() {
    }

    override fun fail(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }



    override fun showLoading() {
        submitClaimProgress.visibility=View.VISIBLE
    }

    override fun hideLoading() {
        submitClaimProgress.visibility=View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        Preferences.setClaimToken(this, "")
        Preferences.setChangeFlag(this, false)
        presenter.onDestroyView()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
            EventBus.getDefault().removeAllStickyEvents()
        }
    }

    override fun onItemClick(position: Int) {

                val intent = Intent(this, AddExpenseActivity::class.java)
                intent.putExtra("Update Expense", position)
                startActivity(intent)
    }


    private fun postExpenseClaim(currencyId: Int, parentObj: ParentExpenseDetails?) {
        val childEntryObj: ArrayList<ChildExpenseRequest> = ArrayList()
        val requestObj: ArrayList<ParentExpenseRequest> = ArrayList()
        val fileArray: ArrayList<MultipartBody.Part> = ArrayList()

        for (child in chilExpenseList) {
            val uploadFile = if (child.uploadFiles != "") child.childExpenseId.toString() else ""
            childEntryObj.add(ChildExpenseRequest(child.childExpenseId.toString(),
                    child.expenseTypeId.toString(), child.description, child.fromDate, child.toDate, child.expenseCurrencyId.toString(),
                    child.expenseAmt.toString(), child.conversionRate.toString(), child.convertedAmt.toString(), uploadFile))
            if (child.uploadFiles.isNotEmpty()) {
                val docFile = File(child.uploadFiles)
                val requestFile = RequestBody.create(MediaType.parse("application/octet-stream"), docFile)
                fileArray.add(MultipartBody.Part.createFormData(child.childExpenseId.toString(), docFile.name, requestFile))
            }
        }
        if (parentObj != null) {
            requestObj.add(ParentExpenseRequest(logData.companyCode, logData.appType, logData.employeeCode,
                    logData.tokenNumber, parentObj.claimToken, "0", parentObj.claimFor,
                    parentObj.claimCurrencyId.toString(), parentObj.totalClaimAmt.toString(), childEntryObj))

        } else {
            requestObj.add(ParentExpenseRequest(logData.companyCode, logData.appType, logData.employeeCode,
                    logData.tokenNumber, "0", "0", etClaimFor.text.toString(), currencyId.toString(),
                    claimAmount.text.toString(), childEntryObj))
        }

        val expJsonBody = RequestBody.create(MediaType.parse("text/plain"), Gson().toJson(SaveExpRequestPojo(requestObj)))
        presenter.saveExpenseDetails(this, expJsonBody, fileArray)

    }

    override fun onPause() {
        super.onPause()
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(claimExpenseMain.windowToken, 0)

    }

    private fun updateInsert(parentObj: ParentExpenseDetails?, currencyId: Int, status: String) {
        if (parentObj != null) {
            databaseHandler.updateParent(parentObj.claimToken, etClaimFor.text.toString().trim(),
                    DateHelper.getDateTimeNew(System.currentTimeMillis()), currencyId,
                    claimAmount.text.toString().toDouble(), status)
        } else {
            databaseHandler.insertParentDetails(ParentExpenseDetails(claimToken, etClaimFor.text.toString().trim(),
                    DateHelper.getDateTimeNew(System.currentTimeMillis()), currencyId, claimAmount.text.toString().toDouble(),
                    status))
        }
        EventBus.getDefault().post(NavigationEvent(status))
        finish()
    }

    override fun onBackPressed() {
        if (Preferences.getChangeFlag(this)) {
            val inflater = LayoutInflater.from(this)
            val dialogView = inflater.inflate(R.layout.auto_date_time_popup, null)
            val dateBuilder = AlertDialog.Builder(this)
            val alertTitle = dialogView.findViewById(R.id.alertTitle) as TextView
            val alertText = dialogView.findViewById(R.id.alertMsg) as TextView
            val btnAllow = dialogView.findViewById(R.id.btnAlertAllow) as Button
            val btnCancel = dialogView.findViewById(R.id.btnAlertCancel) as Button
            dateBuilder.setView(dialogView)
            alertTitle.visibility = View.GONE
            alertText.gravity = Gravity.CENTER
            btnAllow.text = getString(R.string.ok)
            alertText.text = getString(R.string.save_changes_alert)
            val alertDialog = dateBuilder.create()
            alertDialog.show()
            btnCancel.setOnClickListener()
            {
                alertDialog.dismiss()
            }
            btnAllow.setOnClickListener()
            {
                if(parentDetail!=null)
                {
                    databaseHandler.deleteChildExpense(claimToken)
                }
                finish()
            }

        } else {
            finish()
        }

    }



    override fun success(response: SaveExpeResObj?) {

        Toast.makeText(this, response!!.errorMessage, Toast.LENGTH_LONG).show()
        val status = if (response.isValid == 0) getString(R.string.invalid) else getString(R.string.submitted)
        if (response.expAppId != "0") {
            databaseHandler.updateParent(response.expAppId!!, response.claimFor!!, DateHelper.getDateTimeNew(System.currentTimeMillis()),
                    response.claimCurrId!!, response.amount!!, status)

        } else {
            databaseHandler.insertParentDetails(ParentExpenseDetails
            (claimToken, response.claimFor!!, DateHelper.getDateTimeNew(System.currentTimeMillis()),
                    response.claimCurrId!!, response.amount!!, status))

        }
        EventBus.getDefault().post(NavigationEvent(status))
        finish()
    }
}
