package com.coolappz.CuckooTechApp.expense.add_expense_details

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import com.coolappz.CuckooTechApp.BuildConfig
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.expense_database.CurrencyTypeEntity
import com.coolappz.CuckooTechApp.data_base.expense_database.child_expense.ChildExpenseDetails
import com.coolappz.CuckooTechApp.data_base.expense_database.child_expense.ExpenseTypeEntity
import com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo.ExpenseTypeRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.expense_type.ExpenseResPojo
import com.coolappz.CuckooTechApp.utility.*
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.models.sort.SortingTypes
import droidninja.filepicker.utils.Orientation
import kotlinx.android.synthetic.main.activity_add_expense.*
import kotlinx.android.synthetic.main.activity_base_app_compat.*
import kotlinx.android.synthetic.main.textviewforspinner.view.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URISyntaxException
import java.util.*
import kotlin.collections.ArrayList

class AddExpenseActivity : BaseAppCompatActivity<ExpenseDetailsView<ExpenseResPojo>, ExpenseDetailsPresenterImpl>(),
        ExpenseDetailsView<ExpenseResPojo>, AdapterView.OnItemSelectedListener, DocumentListListener {
    override val layoutIdForInjection: Int = R.layout.activity_add_expense

    private val REQUEST_ID_MULTIPLE_PERMISSIONS = 1

    private var fromStr: String = ""
    private var toStr: String = ""
    private val defaultValue: Int = -1
    private val CAMERA_REQUEST: Int = 100
    private val SELECT_IMAGE: Int = 200
    private val MAX_FILE_SIZE: Int = 1000
    private val ONE_KB_SIZE: Int = 1024
    private var FILE_SELECT_CODE: Int = 300
    private var MAX_ATTACHMENT_COUNT: Int = 1
    private var mindate: Calendar? = null
    private val expenseTypeList: ArrayList<String> = ArrayList()
    private var currencyTypeList: ArrayList<String> = ArrayList()
    private var childExpenseId: Int? = defaultValue
    private val listPermissionsNeeded = ArrayList<String>()
    private var selectedDocument: String = ""
    private var expenseCurrencyValue: String = ""
    private var expenseTypeValue: String = ""
    private var documentList: ArrayList<String> = ArrayList()
    private lateinit var databaseHandler: DatabaseHandler
    private var claimToken: String? = null
    private lateinit var driveImage: Bitmap
    private lateinit var originalFilePath: String
    private var sourceUri: Uri? = null
    private var fileAuthority: String? = null

    override fun noInternet() {
        Toast.makeText(this, getString(R.string.poor_net_connection), Toast.LENGTH_LONG).show()
    }

    override fun success(response: ExpenseResPojo?) {
        for (data in response!!.dropdownData!!) {
            if (databaseHandler.getExpenseType(data.expenseTypeId).isNullOrEmpty()) {
                databaseHandler.insertExpenseTypes(ExpenseTypeEntity(data.expenseTypeId, data.expenseTypeName))
            }
        }
        expenseTypeList.clear()
        expenseTypeList.add(0, getString(R.string.select_expense_type))
        expenseTypeList.addAll(databaseHandler.getAllExpenseTypes()!!)
        Toast.makeText(this, response.responseMessage, Toast.LENGTH_LONG).show()
    }

    override fun fail(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        expenseTypeProgressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        expenseTypeProgressBar.visibility = View.GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenSubTitle.text = getString(R.string.expense_details_title)
        databaseHandler = DatabaseHandler(this)
        val loginData = databaseHandler.fetchFromDatabase()!!
        childExpenseId = intent.getIntExtra("Update Expense", defaultValue)
        expenseTypeList.add(0, getString(R.string.select_expense_type))
        tvExpenseType.text = CuckooUtil.Util.spannableTextView(getString(R.string.mandatory_exp_type))
        tvExpenseDes.text = CuckooUtil.Util.spannableTextView(getString(R.string.expense_description))
        fromDate.text = CuckooUtil.Util.spannableTextView(getString(R.string.from_date))
        toDate.text = CuckooUtil.Util.spannableTextView(getString(R.string.to_date))
        tvExpenseCurrency.text = CuckooUtil.Util.spannableTextView(getString(R.string.expense_currency))
        tvExpenseAmount.text = CuckooUtil.Util.spannableTextView(getString(R.string.expesne_amount))
        fileAuthority = applicationContext.packageName + ".fileprovider" // File provider authority

        etExpenseDesc.setOnClickListener()
        {
            etExpenseDesc.isCursorVisible = true
        }
        etExpenseAmount.setOnClickListener()
        {
            etExpenseAmount.isCursorVisible = true
        }

        val expenseAdapter = ArrayAdapter(this, R.layout.textviewforspinner, expenseTypeList)
        expenseTypeSpinner.adapter = expenseAdapter
        expenseTypeSpinner.onItemSelectedListener = this
        currencyTypeList = arrayListOf("INR")
        val currencyAdapter = ArrayAdapter(this, R.layout.textviewforspinner, currencyTypeList)
        expenseCurrencySpinner.adapter = currencyAdapter
        expenseCurrencySpinner.onItemSelectedListener = this
        etExpenseAmount.filters = arrayOf(CuckooUtil.Util.decimalInputFilter())

        if (databaseHandler.getAllExpenseTypes()!!.isEmpty()) {
            val expenseTypeRequestPojo = ExpenseTypeRequestPojo(loginData.appType,
                    loginData.tokenNumber, loginData.companyCode, loginData.employeeCode)
            presenter.getExpenseType(this, expenseTypeRequestPojo)
        } else {
            expenseTypeList.addAll(databaseHandler.getAllExpenseTypes()!!)
            onEditFunction(childExpenseId!!)
        }

        syc_expense_type.setOnClickListener()
        {
            val expenseTypeRequestPojo = ExpenseTypeRequestPojo(loginData.appType,
                    loginData.tokenNumber, loginData.companyCode, loginData.employeeCode)
            presenter.getExpenseType(this, expenseTypeRequestPojo)
        }

        listPermissionsNeeded.add(Manifest.permission.CAMERA)
        listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        uploadListView.layoutManager = LinearLayoutManager(this)
        claimToken = intent.getStringExtra(getString(R.string.claimToken))

        if (databaseHandler.getCurrencyName(100).isNullOrEmpty()) {
            databaseHandler.insertCurrencies(CurrencyTypeEntity(100, "INR", 1.0))
        }

        if (documentList.isNotEmpty()) {
            uploadListView.adapter = DocumentListAdapter(documentList, this)
            selectedDocument = documentList[0]
        }

        expenseDetailSave.setOnClickListener()
        {
            var flag = true
            Log.e("TAG", "expenseCurrencyValue " + expenseTypeValue)
            if (expenseCurrencyValue == "" || expenseTypeValue == "") {
                flag = false
                Toast.makeText(this, getString(R.string.exp_type_currency_error), Toast.LENGTH_SHORT).show()
            }
            if (popUpFromDate.text.isNullOrEmpty()) {
                flag = false
                popUpFromDate.error = getString(R.string.enter_from_date)
            }
            if (popUpToDate.text.isNullOrEmpty()) {
                flag = false
                popUpToDate.error = getString(R.string.enter_to_date)
            }
            if (etExpenseDesc.text.toString().trim().isEmpty()) {
                flag = false
                etExpenseDesc.error = getString(R.string.desc_error)
            }
            if (etExpenseAmount.text.isNullOrEmpty()) {
                flag = false
                etExpenseAmount.error = getString(R.string.amount_error)
            }
            if (flag) {
                Preferences.setChangeFlag(this, true)
                val expenseTypeId = databaseHandler.getExpenseId(expenseTypeValue)
                val expenseCurrencyId = databaseHandler.getCurrencyId(expenseCurrencyValue)
                val conversionRate = databaseHandler.getCurrencyConRate(expenseCurrencyValue)
                if (childExpenseId != defaultValue) {
                    databaseHandler.updateSingleChildDetails(expenseCurrencyId, expenseTypeId, popUpToDate.text.toString(),
                            popUpFromDate.text.toString(), conversionRate, etExpenseAmount.text.toString().toDouble(),
                            etConvertedAmount.text.toString().toDouble(), etExpenseDesc.text.toString().trim(),
                            selectedDocument, childExpenseId!!)
                } else if (claimToken!!.isNotEmpty()) {
                    val childExpObj = ChildExpenseDetails(claimToken!!, expenseCurrencyId, expenseTypeId, popUpToDate.text.toString(),
                            popUpFromDate.text.toString(), conversionRate, etExpenseAmount.text.toString().toDouble(),
                            etConvertedAmount.text.toString().toDouble(), etExpenseDesc.text.toString().trim(), selectedDocument)
                    databaseHandler.insertAllChildDetails(childExpObj)

                }
                finish()
            }
        }

        uploadReceipt.setOnClickListener()
        {
            if (hasPermissions(listPermissionsNeeded)) {
                if (documentList.isEmpty()) {
                    chooseImage()
                } else {
                    Toast.makeText(this, getString(R.string.one_file_selection), Toast.LENGTH_LONG).show()
                }
            } else {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toTypedArray
                (), REQUEST_ID_MULTIPLE_PERMISSIONS)
            }
        }

        etExpenseAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (!p0.isNullOrEmpty()) {
                    etExpenseAmount.gravity = Gravity.END
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                etExpenseAmount.gravity = Gravity.START
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                etConvertedAmount.text = p0 as Editable?
            }
        })

        expenseDetailCancel.setOnClickListener()
        {
            finish()
        }

        popUpFromDate.setOnClickListener()
        {
            DateDialogFragment(getString(R.string.select_date), mYear, mMonth, mDay,
                    DateHelper.todayCalender, null,
                    object : InterfaceOnDateClick {
                        override fun onDateClick(date: Int, month: Int, year: Int) {
                            fromStr = DateHelper.getYearMonthDateFormat(month, year, date)
                            popUpFromDate.text = DateHelper.getDateFilterFormat(month, year, date)
                        }
                    }).show(supportFragmentManager, "DatePicker")
        }

        popUpToDate.setOnClickListener()
        {
            if (!popUpFromDate.text.isNullOrEmpty()) {
                mindate = DateHelper.getDateCalender(fromStr)
            } else if (popUpFromDate.text.isNullOrEmpty()) {
                mindate = null
            }
            DateDialogFragment(getString(R.string.select_date), mYear, mMonth, mDay,
                    DateHelper.todayCalender, mindate,
                    object : InterfaceOnDateClick {
                        override fun onDateClick(date: Int, month: Int, year: Int) {
                            toStr = DateHelper.getYearMonthDateFormat(month, year, date)
                            popUpToDate.text = DateHelper.getDateFilterFormat(month, year, date)
                        }
                    }).show(supportFragmentManager, "DatePicker")
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS && grantResults.isNotEmpty()) {
            for ((index, value) in grantResults.withIndex()) {
                if (value == PackageManager.PERMISSION_DENIED) {
                    when {
                        ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[index]) ->
                            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toTypedArray
                            (), REQUEST_ID_MULTIPLE_PERMISSIONS)
                        else ->
                            Snackbar.make(this.findViewById(android.R.id.content),
                                    R.string.grant_permision_msg,
                                    Snackbar.LENGTH_LONG).setAction(R.string.enable,
                                    View.OnClickListener {
                                        val intent = Intent()
                                        intent.action = android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                        intent.addCategory(Intent.CATEGORY_DEFAULT)
                                        intent.data = Uri.parse("package:$packageName")
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                                        startActivity(intent)
                                    }).setActionTextColor(Color.WHITE).show()
                    }
                }
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    override fun createPresenter(): ExpenseDetailsPresenterImpl {
        presenter = ExpenseDetailsPresenterImpl(this)
        return presenter
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent!!.id) {
            expenseTypeSpinner.id -> if (position == 0) {
                view!!.spinnerTextview.setTextColor(ContextCompat.getColor(this, R.color.silver_grey))
                expenseTypeValue = ""
            } else {
                view!!.spinnerTextview.setTextColor(ContextCompat.getColor(this, R.color.black))
                expenseTypeValue = parent.getItemAtPosition(position).toString()
            }

            expenseCurrencySpinner.id -> {
                /* if (position == 0) {
                     view!!.spinnerTextview.setTextColor(ContextCompat.getColor(this, R.color.silver_grey))
                 } else {*/
                view!!.spinnerTextview.setTextColor(ContextCompat.getColor(this, R.color.black))
                expenseCurrencyValue = parent.getItemAtPosition(position).toString()
                etConversionRate.setText(databaseHandler.getCurrencyConRate(expenseCurrencyValue).toString())
                //}
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun hasPermissions(permissions: ArrayList<String>): Boolean {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && applicationContext != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(applicationContext, permission) !=
                        PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }

    private fun chooseImage() {
        run {
            val imgCamera: LinearLayout
            val imgGallery: LinearLayout
            val imgDocument: LinearLayout
            val inflater = LayoutInflater.from(this)
            val bottomSheetView = inflater.inflate(R.layout.dialoge_img_picker, null)
            val mBottomSheetDialog = BottomSheetDialog(this)
            mBottomSheetDialog.setContentView(bottomSheetView)
            imgCamera = bottomSheetView.findViewById(R.id.linearCamera) as LinearLayout
            imgGallery = bottomSheetView.findViewById(R.id.linearGallery) as LinearLayout
            imgDocument = bottomSheetView.findViewById(R.id.linearDocument) as LinearLayout
            mBottomSheetDialog.show()
            mBottomSheetDialog.setCancelable(true)

            imgCamera.setOnClickListener {
                //Save image before capture
                val originalImage: File = CuckooUtil.Util.getFile()
                originalFilePath = originalImage.absolutePath

                //open camera
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                sourceUri = FileProvider.getUriForFile(this, fileAuthority, originalImage)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, sourceUri)
                intent.putExtra("android.intent.extras.CAMERA_FACING", 1)
                startActivityForResult(intent, CAMERA_REQUEST)
                mBottomSheetDialog.dismiss()
            }

            imgGallery.setOnClickListener {
                // open gallery
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select image to upload"), SELECT_IMAGE)
                BuildConfig.APPLICATION_ID
                mBottomSheetDialog.dismiss()
            }

            imgDocument.setOnClickListener()
            {
                onPickDoc()
                mBottomSheetDialog.dismiss()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onPause() {
        super.onPause()
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(popUpContainer.windowToken, 0)
    }

    @Throws(IOException::class)
    private fun writeToDevice(sourceFile: File, sourceBitmap: Bitmap) {
        val outStream = FileOutputStream(sourceFile)
        sourceBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
        outStream.flush()
        outStream.close()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                CAMERA_REQUEST -> {

                    val uri: Uri? = sourceUri
                    var resultBitmap: Bitmap?
                    try {
                        resultBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri)
                        resultBitmap = CuckooUtil.Util.getRotation(resultBitmap, originalFilePath)
                        writeToDevice(File(originalFilePath), resultBitmap)
                        val newPath = FileManager.storeImage(this, originalFilePath)
                        documentList.add(newPath)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }

                SELECT_IMAGE -> {
                    if (data!!.data != null) {
                        val file: File = CuckooUtil.Util.getFile()
                        if (CuckooUtil.Util.getPathFromUri(this, data.data).isNullOrEmpty()) {
                            try {
                                driveImage = BitmapFactory.decodeStream(contentResolver.openInputStream(data.data!!))
                                writeToDevice(File(file.absolutePath), driveImage)
                                val newBit: Bitmap = CuckooUtil.Util.getRotation(driveImage, file.absolutePath)
                                writeToDevice(File(file.absolutePath), newBit)
                                val newPath = FileManager.storeImage(this, file.absolutePath)
                                documentList.add(newPath)
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        } else {
                            try {
                                driveImage = BitmapFactory.decodeStream(contentResolver.openInputStream(data.data!!))
                                writeToDevice(File(file.absolutePath), driveImage)
                                val newBit: Bitmap = CuckooUtil.Util.getRotation(driveImage, file.absolutePath)
                                writeToDevice(File(file.absolutePath), newBit)
                                val newPath = FileManager.storeImage(this, file.absolutePath)
                                documentList.add(newPath)
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }
                    }
                }

                FILE_SELECT_CODE ->
                    if (data != null) {
                        val file = File(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS)[0])
                        val fileSize = file.length() / ONE_KB_SIZE
                        if (fileSize > MAX_FILE_SIZE) {
                            Snackbar.make(this.findViewById(android.R.id.content),
                                    getString(R.string.max_file_size),
                                    Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show()
                        } else {
                            val newPath = FileManager.storeData(this, data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS)[0])
                            documentList.add(newPath)
                        }
                    }
            }
            if (documentList.isNotEmpty()) {
                val documentListAdapter = DocumentListAdapter(documentList, this)
                uploadListView.adapter = documentListAdapter
                documentListAdapter.notifyDataSetChanged()
                selectedDocument = documentList[0]
            }
        }
    }

    private fun getImageUri(inContext: Context, inImage: Bitmap, fileName: String): Uri {
        val imagePath: String = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, fileName, null)
        return Uri.parse(imagePath)
    }

    @Throws(URISyntaxException::class)
    private fun getPath(context: Context, uri: Uri): String? {
        var cursor: Cursor? = null
        try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = context.contentResolver.query(uri, proj, null, null, null)
            val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(column_index)
        } finally {
            if (cursor != null) {
                cursor.close()
            }
        }
    }

    private fun onEditFunction(childExpenseId: Int) {
        if (childExpenseId != defaultValue) {
            etExpenseAmount.gravity = Gravity.END
            val receivedExpenseObj: ChildExpenseDetails = databaseHandler.getSingleChildDetails(childExpenseId)
            for ((index, value) in expenseTypeList.withIndex()) {
                if (value == databaseHandler.getExpenseType(receivedExpenseObj.expenseTypeId)) {
                    expenseTypeSpinner.setSelection(index)
                }
            }
            for ((index, value) in currencyTypeList.withIndex()) {
                if (value == databaseHandler.getCurrencyName(receivedExpenseObj.expenseCurrencyId)) {
                    expenseCurrencySpinner.setSelection(index)
                }
            }
            etExpenseDesc.setText(receivedExpenseObj.description)
            etExpenseAmount.setText(receivedExpenseObj.expenseAmt.toString())
            etConversionRate.setText(receivedExpenseObj.conversionRate.toString())
            popUpToDate.text = receivedExpenseObj.toDate
            popUpFromDate.text = receivedExpenseObj.fromDate
            etConvertedAmount.setText(receivedExpenseObj.convertedAmt.toString())
            if (receivedExpenseObj.uploadFiles != "") {
                documentList.add(receivedExpenseObj.uploadFiles)
                uploadListView.adapter = DocumentListAdapter(documentList, this)
                selectedDocument = documentList[0]
            }
        }
    }

    override fun onItemClickListener(position: Int, item: String) {
        if (item.contentEquals(getString(R.string.close))) {
            File(documentList[position]).delete()
            documentList.removeAt(position)
            uploadListView.adapter = DocumentListAdapter(documentList, this)
            selectedDocument = ""
        }
        if (item.contentEquals(getString(R.string.name))) {
            if (getMimeType(documentList[position]) == "jpg" || getMimeType(documentList[position]) == "png"
                    || getMimeType(documentList[position]) == "jpeg") {
                val alertDialogBuilder = AlertDialog.Builder(this)
                val inflater = LayoutInflater.from(this)
                val view = inflater.inflate(R.layout.popup_layout, null)
                alertDialogBuilder.setView(view)
                alertDialogBuilder.setCancelable(true)
                val selectedImage = view.findViewById(R.id.uploadReceiptImage) as AppCompatImageView
                selectedImage.setImageURI(Uri.parse(documentList[position]))
                val dialog = alertDialogBuilder.create()
                if (!dialog.isShowing)
                    dialog.show()
            } else {
                val picUri = FileProvider.getUriForFile(applicationContext, BuildConfig.APPLICATION_ID +
                        ".fileprovider", File(documentList[position]))
                val newIntent = Intent(Intent.ACTION_VIEW)
                val mimeType = contentResolver.getType(picUri)
                newIntent.setDataAndType(picUri, mimeType)
                newIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                try {
                    startActivity(newIntent)
                } catch (e: ActivityNotFoundException) {
                    Log.e("TAG", "Activity not found " + e.message)
                }
            }
        }
    }

    private fun getMimeType(url: String): String {
        var extension = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension == "") {
            extension = url.substring(url.lastIndexOf(".") + 1)
        }
        return extension
    }


    private fun onPickDoc() {
        val zips = arrayOf(".zip", ".rar")
        val pdfs = arrayOf(".pdf")
        val allFiles = arrayOf(".xls", ".xlsx", ".docx", ".doc", ".txt")
        val maxCount = MAX_ATTACHMENT_COUNT - documentList.size
        if (documentList.size == MAX_ATTACHMENT_COUNT) {
            Toast.makeText(this, "Cannot select more than $MAX_ATTACHMENT_COUNT items",
                    Toast.LENGTH_LONG).show()
        } else {
            FilePickerBuilder.getInstance()
                    .setMaxCount(maxCount)
                    .setActivityTheme(R.style.FilePickerTheme)
                    .setActivityTitle("Please select doc")
                    .addFileSupport("All files", allFiles)
                    .addFileSupport("Zip", zips)
                    .addFileSupport("Pdf", pdfs)
                    .enableDocSupport(false)
                    .enableSelectAll(true)
                    .sortDocumentsBy(SortingTypes.name)
                    .withOrientation(Orientation.UNSPECIFIED)
                    .pickFile(this, FILE_SELECT_CODE)
        }
    }
}
