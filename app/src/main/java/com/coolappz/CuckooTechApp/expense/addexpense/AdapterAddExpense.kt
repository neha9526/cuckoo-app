package com.coolappz.CuckooTechApp.expense.addexpense

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.coolappz.CuckooTechApp.BuildConfig
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.expense_database.child_expense.ChildExpenseDetails
import com.coolappz.CuckooTechApp.utility.CuckooUtil
import java.io.File

/**
 * Created by admin on 15-03-2018.
 */
class AdapterAddExpense(private var addExpenseList: MutableList<ChildExpenseDetails>,
                        private var onItemClick: OnItemClickListener, var context: Context,
                        var databaseHandler: DatabaseHandler, var status:Boolean)
    : RecyclerView.Adapter<AdapterAddExpense.AddExpenseView>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): AddExpenseView {
        val viewHolder = LayoutInflater.from(parent!!.context).inflate(R.layout.adapter_add_expense,
                parent, false)
        return AddExpenseView(viewHolder)
    }

    override fun getItemCount(): Int {
        return addExpenseList.size
    }

    override fun onBindViewHolder(holder: AddExpenseView?, position: Int) {
        val attachedDoc= addExpenseList[position].uploadFiles
        if(status)
        {
            holder!!.editExpense.visibility=View.GONE
        }
        if( attachedDoc!="")
        {
            holder!!.uploadReceipt.visibility=View.VISIBLE
            holder.uploadReceipt.setOnClickListener()
            {
                if (CuckooUtil.Util.getMimeType(attachedDoc) == "jpg" || CuckooUtil.Util.getMimeType(attachedDoc) == "png"
                        ||CuckooUtil.Util.getMimeType(attachedDoc) == "jpeg") {
                    val alertDialogBuilder = AlertDialog.Builder(context)
                    val inflater = LayoutInflater.from(context)
                    val view = inflater.inflate(R.layout.popup_layout, null)
                    alertDialogBuilder.setView(view)
                    alertDialogBuilder.setCancelable(true)
                    val selectedImage = view.findViewById(R.id.uploadReceiptImage) as AppCompatImageView
                    selectedImage.setImageURI(Uri.parse(attachedDoc))
                    val dialog = alertDialogBuilder.create()
                    if (!dialog.isShowing)
                        dialog.show()
                } else {
                    Log.e("TAG","Attached file "+File(attachedDoc))
                    val picUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID +
                            context.getString(R.string.provider), File(attachedDoc))
                    val newIntent = Intent(Intent.ACTION_VIEW)
                    val mimeType = context.contentResolver.getType(picUri)
                    Log.e("TAG","Pic Uri "+picUri)
                    newIntent.setDataAndType(picUri, mimeType)
                    newIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                    try {
                        context.startActivity(newIntent)
                    } catch (e: ActivityNotFoundException) {
                        Log.e("TAG", "Activity not found " + e.message)
                    }
                }
            }

        }
        holder!!.expenseType.text = databaseHandler.getExpenseType(addExpenseList[position].expenseTypeId)
        holder.convertedAmt.text = addExpenseList[position].convertedAmt.toString()
        holder.description.text = addExpenseList[position].description



        holder.editExpense.setOnClickListener()
        {
            onItemClick.onItemClick(addExpenseList[position].childExpenseId)
        }
    }

    class AddExpenseView(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var expenseType = itemView.findViewById(R.id.listExpenseType) as AppCompatTextView
        var description = itemView.findViewById(R.id.listExpenseDesc) as AppCompatTextView
        //var expenseCurrency = itemView.findViewById(R.id.listExpenseCurrency) as AppCompatTextView
       // var conversionRate = itemView.findViewById(R.id.listConversionRate) as AppCompatTextView
        var convertedAmt = itemView.findViewById(R.id.listConvertedAmt) as AppCompatTextView
        var uploadReceipt = itemView.findViewById(R.id.attachment) as AppCompatImageView
        var editExpense = itemView.findViewById(R.id.editExpense) as AppCompatImageView
    }
}
