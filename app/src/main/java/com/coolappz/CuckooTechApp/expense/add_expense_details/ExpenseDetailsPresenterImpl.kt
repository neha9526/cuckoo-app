package com.coolappz.CuckooTechApp.expense.add_expense_details

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo.ExpenseTypeRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.expense_type.ExpenseResPojo
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.expense_type.ExpenseTypeResPojo
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter

/**
 * Created by admin on 30-06-2018.
 */
class ExpenseDetailsPresenterImpl(var view: ExpenseDetailsView<ExpenseResPojo>?) :
        MvpBasePresenter<ExpenseDetailsView<ExpenseResPojo>>(),ExpenseDetailsPresenter,InterfaceInteractor<ExpenseTypeResPojo> {
    private  var expenseDetailInteractorImpl =ExpenseDetailsInteactorImpl()


    override fun successApi(response: ExpenseTypeResPojo) {
        if(view!=null)
        {
            view?.hideLoading()
            view?.success(response.response)
        }
    }

    override fun failApi(message: String) {
        view?.hideLoading()
        view?.fail(message)
    }

    override fun noInternet() {
        view?.hideLoading()
        view?.noInternet()
    }

    override fun tokenExpire() {}



    override fun getExpenseType(context: Context, expenseTypeRequestPojo: ExpenseTypeRequestPojo) {
            view!!.showLoading()
            expenseDetailInteractorImpl.expenseTypeDetails(context,expenseTypeRequestPojo,this)


    }


}