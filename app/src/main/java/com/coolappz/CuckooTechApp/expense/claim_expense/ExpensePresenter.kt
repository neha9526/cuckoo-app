package com.coolappz.CuckooTechApp.expense.claim_expense

import android.content.Context
import okhttp3.MultipartBody
import okhttp3.RequestBody

/**
 * Created by admin on 07-02-2018.
 */
interface ExpensePresenter {
    fun saveExpenseDetails(context: Context, expDetailPojo: RequestBody, docArray:ArrayList<MultipartBody.Part>)
    fun onDestroyView()
}