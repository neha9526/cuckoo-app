package com.coolappz.CuckooTechApp.expense.addexpense

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by admin on 15-03-2018.
 */
 class AddExpensePojo(var expenseType:String,
                      var claimAmount:String,
                      var expenseFrom:String,
                      var expenseTo:String,
                      var description:String,
                      var expesneCurrency:String,
                      var conversionRate:String,
                      var convertedAmount:String,
                      var uploadedReceipts: String?):Parcelable {
 constructor(parcel: Parcel) : this(
         parcel.readString(),
         parcel.readString(),
         parcel.readString(),
         parcel.readString(),
         parcel.readString(),
         parcel.readString(),
         parcel.readString(),
         parcel.readString(),
         parcel.readString()
        ) {
 }

 override fun writeToParcel(dest: Parcel?, flags: Int) {
  dest!!.writeString(this.expenseType)
  dest.writeString(this.claimAmount)
  dest.writeString(this.expenseFrom)
  dest.writeString(this.expenseTo)
  dest.writeString(this.description)
  dest.writeString(this.expesneCurrency)
  dest.writeString(this.conversionRate)
  dest.writeString(this.convertedAmount)
  dest.writeString(this.uploadedReceipts)
 }

 override fun describeContents(): Int=0

 companion object CREATOR : Parcelable.Creator<AddExpensePojo> {
  override fun createFromParcel(parcel: Parcel): AddExpensePojo {
   return AddExpensePojo(parcel)
  }

  override fun newArray(size: Int): Array<AddExpensePojo?> {
   return arrayOfNulls(size)
  }
 }
}