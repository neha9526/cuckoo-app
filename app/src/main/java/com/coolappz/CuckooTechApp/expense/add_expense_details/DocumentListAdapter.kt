package com.coolappz.CuckooTechApp.expense.add_expense_details

import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.coolappz.CuckooTechApp.R

/**
 * Created by admin on 05-07-2018.
 */
class DocumentListAdapter(var documentList:ArrayList<String>,var onItemClick:DocumentListListener):
        RecyclerView.Adapter<DocumentListAdapter.DocumentListView>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): DocumentListView {
        val viewHolder = LayoutInflater.from(parent!!.context).inflate(R.layout.adapter_document_list,parent, false)
        return DocumentListView(viewHolder)
    }

    override fun getItemCount(): Int {
        return documentList.size
    }

    override fun onBindViewHolder(holder: DocumentListView?, position: Int) {
        val fileName=documentList[position].substring(documentList[position].lastIndexOf("/")+1)
        holder!!.documentName.text =  fileName

        holder.deleteDocument.setOnClickListener()
        {
          onItemClick.onItemClickListener(position,"close")
        }

        holder.documentName.setOnClickListener()
        {
            onItemClick.onItemClickListener(position,"name")
        }
    }

    class DocumentListView(itemView:View):RecyclerView.ViewHolder(itemView){

        var documentName=itemView.findViewById(R.id.document_name) as AppCompatTextView
        var deleteDocument=itemView.findViewById(R.id.delete_document) as AppCompatImageView

    }
}