package com.coolappz.CuckooTechApp.expense.add_expense_details

import com.coolappz.CuckooTechApp.network.BaseView
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.expense_type.ExpenseResPojo

/**
 * Created by admin on 30-06-2018.
 */
interface ExpenseDetailsView<T>:BaseView<T> {}