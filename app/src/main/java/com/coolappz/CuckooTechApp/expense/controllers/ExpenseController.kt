package com.coolappz.CuckooTechApp.expense.controllers

import android.content.Intent
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.bluelinelabs.conductor.support.RouterPagerAdapter
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.about_us.AboutUSActivity
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.ParentExpenseDetails
import com.coolappz.CuckooTechApp.faq.FaqActivity
import com.coolappz.CuckooTechApp.utility.*
import kotlinx.android.synthetic.main.controller_expense.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by admin on 09-01-2018.
 */
class ExpenseController( ) : Controller(), ViewPager.OnPageChangeListener {

    override fun onPageScrollStateChanged(state: Int) {}

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onPageSelected(position: Int) {

    }
    private  var routerPagerAdapter: RouterPagerAdapter?=null
    private  var controllerMap: MutableMap<Int, Controller>?=null
    private  var fromStr:String= ""
    private var toDate:String=""
    private var tabPosition:Int =0
    private var mindate:Calendar?=null
    private var sortedExpenseList:MutableList<ParentExpenseDetails> = ArrayList()
    private lateinit var databaseHandler:DatabaseHandler

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val rootView: View = inflater.inflate(R.layout.controller_expense, container, false)
        if(!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this)
        }
        return rootView
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        databaseHandler=DatabaseHandler(applicationContext!!)
        val mMonth= DateHelper.getMonth(DateHelper.todayCalender.timeInMillis).toInt()-1
        val mYear= DateHelper.getYear(DateHelper.todayCalender.timeInMillis).toInt()
        val mDay= DateHelper.getDay(DateHelper.todayCalender.timeInMillis).toInt()

        if(databaseHandler.fetchMaxDate()!=null && databaseHandler.fetchMinDate()!=null)
        {
            if(view.expenseFromDate.text.isEmpty()&& view.expenseToDate.text.isEmpty())
            {
                fromStr = DateHelper.getDatabaseFormat(databaseHandler.fetchMinDate()!!)
                toDate =  DateHelper.getDatabaseFormat(databaseHandler.fetchMaxDate()!!)
                view.expenseFromDate.text= DateHelper.dateFilterFormat(databaseHandler.fetchMinDate()!!)
                view.expenseToDate.text=DateHelper.dateFilterFormat( databaseHandler.fetchMaxDate()!!)
            }
        }


        tabPosition= view.tabLayout.selectedTabPosition
        if(controllerMap==null && routerPagerAdapter==null){
            controllerMap = HashMap()
            setUpControllers()
            routerPagerAdapter = object : RouterPagerAdapter(this) {
                override fun configureRouter(router: Router, position: Int) {
                    if(!router.hasRootController())
                    {
                        router.pushController(RouterTransaction
                                .with(controllerMap!![position]!!)
                                .pushChangeHandler(FadeChangeHandler())
                                .popChangeHandler(FadeChangeHandler()))
                    }
                }

                override fun getCount(): Int {
                    return controllerMap!!.size
                }
            }
            view.expenseViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(view.tabLayout))
            view.expenseViewPager.adapter = routerPagerAdapter
            view.tabLayout.tabMode=TabLayout.MODE_SCROLLABLE
        }


      val fragment=activity as AppCompatActivity
       view.expenseFromDate.setOnClickListener{
            DateDialogFragment(activity!!.getString(R.string.select_date), mYear, mMonth, mDay,
                    DateHelper.todayCalender,null,
                    object : InterfaceOnDateClick {
                        override fun onDateClick(date: Int, month: Int, year: Int) {
                            fromStr =  DateHelper.getYearMonthDateFormat(month, year, date)
                            view.expenseFromDate.text =DateHelper.getDateFilterFormat(month, year, date)
                        }
                    }).show(fragment.supportFragmentManager, "DatePicker")
        }

       view.expenseToDate.setOnClickListener{
           if(view.expenseFromDate.text.isNotEmpty()){
               mindate=DateHelper.getDateCalender(fromStr)
           }
           else{
               mindate=null
           }

           DateDialogFragment(activity!!.getString(R.string.select_date), mYear, mMonth, mDay,
                    DateHelper.todayCalender,mindate,
                    object : InterfaceOnDateClick {
                        override fun onDateClick(date: Int, month: Int, year: Int) {
                             toDate =  DateHelper.getYearMonthDateFormat(month, year, date)
                            view.expenseToDate.text = DateHelper.getDateFilterFormat(month, year, date)
                        }
                    }).show(fragment.supportFragmentManager, "DatePicker")
        }

        view.expenseFilter.setOnClickListener()
        {

            if (view.expenseToDate.text.isNotEmpty() && view.expenseFromDate.text.isNotEmpty()) {

                val fromDate = fromStr+" "+Constants.Constants.AM_MIN
                val toDate = toDate+" "+Constants.Constants.PM_MAX
                when(tabPosition)
                {
                   0 ->
                   {
                       sortedExpenseList= databaseHandler.getSortedExpense(fromDate,toDate)
                       EventBus.getDefault().post(EventDateFilter(sortedExpenseList,applicationContext!!.getString(R.string.controller)))
                   }
                   1 ->   {
                       sortedExpenseList= databaseHandler.getSortedExpStatus(fromDate,toDate,applicationContext!!.getString(R.string.pending))
                       EventBus.getDefault().post(PendingEvent(sortedExpenseList,applicationContext!!.getString(R.string.controller)))
                   }
                   2 ->   {

                       sortedExpenseList= databaseHandler.getSortedExpStatus(fromDate,toDate,applicationContext!!.getString(R.string.submitted_offline))
                       EventBus.getDefault().post(OfflineEvent(sortedExpenseList))
                   }
                   3 ->  {
                       sortedExpenseList= databaseHandler.getSortedExpStatus(fromDate,toDate,applicationContext!!.getString(R.string.submitted))
                       EventBus.getDefault().post(SubmittedEvent(sortedExpenseList))
                   }
                   4 ->   {
                       sortedExpenseList= databaseHandler.getSortedExpStatus(fromDate,toDate,applicationContext!!.getString(R.string.invalid))
                       EventBus.getDefault().post(InvalidEvent(sortedExpenseList))
                   }
                }

            }
            else{
                Toast.makeText(applicationContext, applicationContext!!.getString(R.string.enter_from_to),
                        Toast.LENGTH_SHORT).show()
            }
        }

        view.aboutUsClaim.setOnClickListener() {
            val intent = Intent(activity, AboutUSActivity::class.java)
            startActivity(intent)
        }

        view.faqClaim.setOnClickListener() {
            val intent = Intent(activity, FaqActivity::class.java)
            startActivity(intent)
        }

        view.tabLayout.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                view.expenseViewPager.currentItem = tab.position
                 tabPosition=tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun setNavigation(navigationEvent:NavigationEvent)
    {
        when(navigationEvent.tabName)
        {
            applicationContext?.getString(R.string.pending) -> {
                val  pager:ViewPager  = view!!.expenseViewPager;
                pager.setCurrentItem(1,true);
            }
            applicationContext?.getString(R.string.submitted_offline) -> {
                view?.expenseViewPager?.setCurrentItem(2,true);
            }
            applicationContext?.getString(R.string.submitted) -> {
                view?.expenseViewPager?.setCurrentItem(3,true);
            }
            applicationContext?.getString(R.string.invalid) -> {

                view?.expenseViewPager?.setCurrentItem(4,true);
            }
            else -> {
                view?.expenseViewPager?.setCurrentItem(0,true);
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }
       private fun setUpControllers() {
           controllerMap!![0] = AllController()
           controllerMap!![1] = PendingSubmitController()
           controllerMap!![2] = OfflineSubmitController()
           controllerMap!![3] = SubmittedController()
           controllerMap!![4] = InvalidController()
    }
}