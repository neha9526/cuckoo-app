package com.coolappz.CuckooTechApp.expense.addexpense

import android.content.Context
import okhttp3.MultipartBody
import okhttp3.RequestBody

/**
 * Created by admin on 10-03-2018.
 */
interface AddExpensePresenter {
    fun saveExpenseDetails(context: Context,expDetailPojo:RequestBody,docArray:ArrayList<MultipartBody.Part>)
    fun onDestroyView()
}