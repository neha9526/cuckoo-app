package com.coolappz.CuckooTechApp.splash

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.dashboard.DashboardDrawerActivity
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.loginDatabase.LoginDetails
import com.coolappz.CuckooTechApp.login.LoginActivity
import com.coolappz.CuckooTechApp.registration.RegisterActivity
import com.coolappz.CuckooTechApp.utility.CommonConstants.utility.APP_TYPE_1
import com.coolappz.CuckooTechApp.utility.CommonConstants.utility.APP_TYPE_3
import com.coolappz.CuckooTechApp.utility.CommonConstants.utility.isAutomaticTimeZoneEnable
import com.coolappz.CuckooTechApp.utility.CommonConstants.utility.isTimeAutomatic
import com.coolappz.CuckooTechApp.utility.CuckooUtil
import com.coolappz.CuckooTechApp.utility.DateHelper
import com.coolappz.CuckooTechApp.utility.PopUps
import com.coolappz.CuckooTechApp.utility.Preferences
import com.coolappz.CuckooTechApp.utility.Preferences.Companion.APP_VERSION
import io.fabric.sdk.android.Fabric


class SplashActivity : AppCompatActivity() {
    private var SPLASH_DISPLAY_LENGTH = 2000
    private var appType: String? = ""
    private var currentDate: String = ""
    private var storedTime: String = ""
    private lateinit var databaseHandler: DatabaseHandler
    private lateinit var popUps: PopUps
    private lateinit var locationManager: LocationManager
    private  var logData:LoginDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_splash)
        popUps = PopUps(this)
        databaseHandler = DatabaseHandler(this)
        currentDate = DateHelper.getDate(System.currentTimeMillis())
        locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    override fun onResume() {
        super.onResume()
        val isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        val isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (!isTimeAutomatic(this) || !isAutomaticTimeZoneEnable(this)) {
            popUps.autoTimePopUp(this)
        } else if (!isGPSEnabled && !isNetworkEnabled) {
            popUps.displayLocationSettingsRequest(this)

        } else {
            startSplashScreen()
        }
    }


    private fun startSplashScreen() {
        Handler().postDelayed(Runnable {
            val isRegisteredUser = Preferences.getAutoLogin(applicationContext)

            if (isRegisteredUser) {
                logData=databaseHandler.fetchFromDatabase()!!
                appType = logData!!.appType
                Log.e("TAG","AppType "+appType)
                storedTime = DateHelper.getFormattedDate(logData!!.timeStamp!!)
                intent = if (appType == APP_TYPE_1 || appType == APP_TYPE_3) {
                    Intent(applicationContext, DashboardDrawerActivity::class.java)
                } else  {
                    Log.e("TAG","Stored Date "+storedTime)
                    Log.e("TAG","Current Date "+currentDate)
                    if (storedTime != currentDate) {
                        Intent(applicationContext, LoginActivity::class.java)
                    } else {
                        Intent(applicationContext, DashboardDrawerActivity::class.java)
                    }
                }
                startActivity(intent)
                finish()
            } else {
                intent = Intent(this@SplashActivity, RegisterActivity::class.java)
                startActivity(intent)
                finish()
            }
        }, SPLASH_DISPLAY_LENGTH.toLong())
    }

}
