package com.coolappz.CuckooTechApp.profile

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.coolappz.CuckooTechApp.R
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.support.v7.widget.Toolbar
import android.util.Base64
import kotlinx.android.synthetic.main.activity_profile.*


class ProfileActivity : AppCompatActivity() {

    private lateinit var profileToolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        profileToolbar= findViewById(R.id.profileToolbar) as Toolbar
        setSupportActionBar(profileToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        profileToolbar.setNavigationOnClickListener {
            finish()
        }

        val encodedImage=intent.getStringExtra("EncodedImage")
        val decodedString = Base64.decode(encodedImage, Base64.DEFAULT)
        val decodedByte:Bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        profileImage.setImageBitmap(decodedByte)


    }
}
