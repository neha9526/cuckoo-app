package com.coolappz.CuckooTechApp.registration;

import com.coolappz.CuckooTechApp.network.BaseView;
import com.coolappz.CuckooTechApp.network.responsepojo.registrationpojo.RegistrationResponse;


/**
 * Created by Vikas Pawar on 05-12-2018.
 */

public interface RegistrationView<T> extends BaseView<T> { }
