package com.coolappz.CuckooTechApp.registration

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeRegistrationRequest
import com.coolappz.CuckooTechApp.network.responsepojo.registrationpojo.RegistrationResponse
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter

class EmployeeRegistrationPresenter(var view:RegistrationView<RegistrationResponse>?) :
        MvpBasePresenter<RegistrationView<RegistrationResponse>>(), RegistrationPresenter,
        InterfaceInteractor<RegistrationResponse> {




    override fun successApi(response: RegistrationResponse) {
        if(view!=null)
        {
            view?.hideLoading()
            view?.success(response)
        }
    }

    private var registrationInteractor=RegistrationInteractorImpl()

    override fun failApi(message: String) {
       if(view!=null)
       {
           view?.hideLoading()
           view?.fail(message)
       }
    }

    override fun noInternet() {
      if(view!=null)
      {
          view?.hideLoading()
          view?.noInternet()
      }
    }

    override fun tokenExpire() {
        if(view!=null)
        {
            view?.hideLoading()

        }
    }

    override fun onRegistration(context: Context,employeeRegistrationRequest: EmployeeRegistrationRequest) {
        if(view!=null)
        {
            view?.showLoading()
            registrationInteractor.onRegistration(context,employeeRegistrationRequest, this)
        }
    }

    override fun onDestroyView() {
       view=null
    }




}
