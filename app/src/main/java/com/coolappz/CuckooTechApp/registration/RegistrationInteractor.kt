package com.coolappz.CuckooTechApp.registration

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeRegistrationRequest
import com.coolappz.CuckooTechApp.network.responsepojo.registrationpojo.RegistrationResponse
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor

/**
 * Created by admin on 31-01-2018.
 */
interface RegistrationInteractor {
    fun onRegistration(context: Context, employeeRegistrationRequest: EmployeeRegistrationRequest,
                       interfaceInteractor: InterfaceInteractor<RegistrationResponse>)

}