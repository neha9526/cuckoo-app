package com.coolappz.CuckooTechApp.registration

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.dashboard.DashboardDrawerActivity
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.loginDatabase.LoginDetails
import com.coolappz.CuckooTechApp.data_base.menu_master.MenuEntity
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeRegistrationRequest
import com.coolappz.CuckooTechApp.network.responsepojo.registrationpojo.RegistrationResponse
import com.coolappz.CuckooTechApp.utility.*
import com.coolappz.CuckooTechApp.utility.CuckooUtil.Util.getMacAddress
import com.coolappz.CuckooTechApp.utility.CuckooUtil.Util.isNetworkAvailable
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import kotlinx.android.synthetic.main.activity_register.*


class RegisterActivity : MvpActivity<RegistrationView<RegistrationResponse>,
        EmployeeRegistrationPresenter>(), RegistrationView<RegistrationResponse> {

    private var macAddress: String = ""
    private var databaseHandler:DatabaseHandler?=null
    private var currentTimeString: String = ""
    private lateinit var popUps: PopUps

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        val longTime = System.currentTimeMillis()
        databaseHandler = DatabaseHandler(this)
        popUps = PopUps(this)
        currentTimeString = DateHelper.getDateTime(longTime)
        macAddress = getMacAddress().toLowerCase()


        btnRegister.setOnClickListener {
            var flag = true
            if (employeeCode.text.isNullOrEmpty()) {
                employeeCode.error = getString(R.string.enter_employee_code)
                flag = false
            }

            if (companyCode.text.isNullOrEmpty()) {
                companyCode.error = getString(R.string.pls_enter_company_code)
                flag = false
            }
            if (mobileNumber.text.isNullOrEmpty() || mobileNumber.text.length < 10) {
                mobileNumber.error = getString(R.string.enter_valid_mobile_number)
                flag = false
            }
            if (!isNetworkAvailable(this)) {
                Toast.makeText(this, getString(R.string.require_net_connection), Toast.LENGTH_SHORT).show()
                flag = false
            }

            if (flag) {

                    apiCall()
            }
        }

    }



    override fun createPresenter(): EmployeeRegistrationPresenter {
        presenter = EmployeeRegistrationPresenter(this)
        return presenter
    }

    override fun noInternet() {
      registerProgressBar.visibility = View.GONE
        Toast.makeText(this, getString(R.string.poor_net_connection), Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        if (!CommonConstants.utility.isTimeAutomatic(this)) {
            popUps.autoTimePopUp(this)
        }
    }


    override fun fail(message: String?) {
        registerProgressBar.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }


    override fun showLoading() {
        registerProgressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        registerProgressBar.visibility = View.GONE
    }

    override fun onDestroy() {
        presenter.onDestroyView()
        super.onDestroy()

    }
    private fun databaseOperations(response: RegistrationResponse)
    {
        val loginDetails = LoginDetails(companyCode.text.toString(), response.employeeCode,
                mobileNumber.text.toString(), macAddress, response.applicationtype, response.token, currentTimeString)
        databaseHandler!!.addToDatabase(loginDetails)
        //Updating menulist
        if(response.menuList!!.isNotEmpty())
        {
            for(menu in response.menuList!!)
            {
                if(databaseHandler!!.fetchMenus().contains(menu.moduleName))
                {
                    databaseHandler!!.updateMenuFlag(menu.moduleName!!,menu.isActive!!)
                }
                else{
                    databaseHandler!!.insertMenuDetails(MenuEntity(menu.moduleName!!,menu.isActive!!))
                }

            }
        }

    }
    override fun success(response: RegistrationResponse?) {
        Toast.makeText(this, response!!.responseMessage, Toast.LENGTH_SHORT).show()
        databaseOperations(response)
        val intent = Intent(this, DashboardDrawerActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun apiCall()
    {
        val employeeRegistrationRequest = EmployeeRegistrationRequest(employeeCode.text.toString(),
                mobileNumber.text.toString(), companyCode.text.toString(), macAddress)
        presenter.onRegistration(this, employeeRegistrationRequest)
    }

}
