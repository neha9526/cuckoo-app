package com.coolappz.CuckooTechApp.registration

import android.content.Context
import android.util.Log
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.network.Api
import com.coolappz.CuckooTechApp.network.ApiFail
import com.coolappz.CuckooTechApp.network.ApiSuccess
import com.coolappz.CuckooTechApp.network.HttpErrorResponse
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeRegistrationRequest
import com.coolappz.CuckooTechApp.network.responsepojo.registrationpojo.EmployeeRegistrationResponse
import com.coolappz.CuckooTechApp.network.responsepojo.registrationpojo.RegistrationResponse
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor
import com.coolappz.CuckooTechApp.utility.Preferences
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by admin on 31-01-2018.
 */
class RegistrationInteractorImpl() : RegistrationInteractor {
    override fun onRegistration(context: Context, employeeRegistrationRequest: EmployeeRegistrationRequest,
                                interfaceInteractor: InterfaceInteractor<RegistrationResponse>) {


        Api.userManagement().employeeRegistration(employeeRegistrationRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiSuccess<EmployeeRegistrationResponse>() {
                    override fun call(response: EmployeeRegistrationResponse) {
                        when {
                            response.response!!.isSucceeded!! -> {
                                val registerToken:String=employeeRegistrationRequest.mobileNumber.toString()
                                val employeeCode=employeeRegistrationRequest.employeeCode
                                Preferences.setAutoLogin(context,true)
                                Preferences.setEmployeeCode(context,employeeCode)
                                interfaceInteractor.successApi(response.response)

                                 }
                            else -> {
                                interfaceInteractor.failApi(response.response.responseMessage.toString())
                            }
                        }
                    }

                }, object : ApiFail() {
                    override fun noNetworkError() {
                        interfaceInteractor.noInternet()
                    }

                    override fun unknownError(e: Throwable) {
                        interfaceInteractor.failApi(context.getString(R.string.unknown_error_msg))
                        Log.d("Error=====>",e.message)

                    }

                    override fun httpStatus(response: HttpErrorResponse) {
                        interfaceInteractor.failApi(context.getString(R.string.http_status_error_msg))
                    }

                })


    }
}

