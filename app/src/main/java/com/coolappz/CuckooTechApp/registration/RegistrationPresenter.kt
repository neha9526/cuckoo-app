package com.coolappz.CuckooTechApp.registration

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeRegistrationRequest

/**
 * Created by admin on 02-02-2018.
 */
interface RegistrationPresenter {
    fun onRegistration(context: Context, employeeRegistrationRequest: EmployeeRegistrationRequest)
    fun onDestroyView()
}