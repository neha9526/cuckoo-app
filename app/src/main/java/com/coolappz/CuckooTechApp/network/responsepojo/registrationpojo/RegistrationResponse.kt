package com.coolappz.CuckooTechApp.network.responsepojo.registrationpojo

import com.coolappz.CuckooTechApp.network.responsepojo.MenuPojo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 28-04-2018.
 */
class RegistrationResponse(
        @SerializedName("IsSucceeded")
        @Expose
        var isSucceeded: Boolean? = null,
        @SerializedName("ResponseMessage")
        @Expose
        var responseMessage: String? = null,
        @SerializedName("EmployeeCode")
        @Expose
        var employeeCode: String? = null,
        @SerializedName("Sync")
        @Expose
        var syncStatus: String? = null,
        @SerializedName("AppType")
        @Expose
        var applicationtype: String? = null,
        @SerializedName("IsValid")
        @Expose
        var isValid: Int? = null,
        @SerializedName("TokenNumber")
        @Expose
        var token: String? = null,
        @SerializedName("MenuList")
        @Expose
        var menuList: HashSet<MenuPojo>? = null)