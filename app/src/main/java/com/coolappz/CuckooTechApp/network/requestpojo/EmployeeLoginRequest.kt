package com.coolappz.CuckooTechApp.network.requestpojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by admin on 03-02-2018.
 */
class EmployeeLoginRequest(  @SerializedName("EmployeeCode")
                             @Expose
                             private var employeecode: String?,
                             @SerializedName("Password")
                             @Expose
                             private var password: String?,
                             @SerializedName("CompanyCode")
                             @Expose
                             private var companyCode:String?,
                             @SerializedName("MacAddress")
                             @Expose
                             private var macAddress:String?,
                             @SerializedName("MobileNumber")
                             @Expose
                             private var mobileNumber:String?

)
{

    fun getEmployeecode(): String? {
        return employeecode
    }

    fun setEmployeecode(employeecode: String) {
        this.employeecode = employeecode
    }

    fun getPassword(): String? {
        return password
    }

    fun setPassword(password: String) {
        this.password = password
    }
}