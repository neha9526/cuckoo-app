package com.coolappz.CuckooTechApp.network.responsepojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MenuResponsePojo(@SerializedName("IsSucceeded")
                       @Expose
                       var isSucceeded: Boolean? = null,
                       @SerializedName("ResponseMessage")
                       @Expose
                       var responseMessage: String? = null,
                       @SerializedName("MenuList")
                       @Expose
                       var menuList: ArrayList<MenuPojo>? = null,
                       @SerializedName("AppType")
                       @Expose
                       var appType: Int = 0) {
}

class MenuResponse( @SerializedName("Response")
                    @Expose
                    val response: MenuResponsePojo? = null)
{

}