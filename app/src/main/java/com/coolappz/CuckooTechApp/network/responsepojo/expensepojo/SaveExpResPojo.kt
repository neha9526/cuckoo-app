package com.coolappz.CuckooTechApp.network.responsepojo.expensepojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 24-07-2018.
 */
class SaveExpResPojo {
    @SerializedName("Response")
    @Expose
    val response: SaveExpeResObj? = null
}