package com.coolappz.CuckooTechApp.network.responsepojo.loginpojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by admin on 03-02-2018.
 */
class EmployeeLoginResponse {
    @SerializedName("Response")
    @Expose
    val response: LoginResponse? = null
    }
