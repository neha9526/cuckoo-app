package com.coolappz.CuckooTechApp.network.requestpojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by admin on 10-03-2018.
 */
class EmployeeAttendanceRequest(@SerializedName("EmployeeCode")
                                @Expose
                                private var employeeCode: String?,
                                @SerializedName("CompanyCode")
                                @Expose
                                private var companyCode: String?,
                                @SerializedName("MobileNumber")
                                @Expose
                                private var mobileNumber: String?,
                                @SerializedName("TransnactionDateTime")
                                @Expose
                                private var transactionDateTime: String?,
                                @SerializedName("MacAddress")
                                @Expose
                                private var macAddress: String?,
                                @SerializedName("Latitude")
                                @Expose
                                private var latitude: String?,
                                @SerializedName("Longitude")
                                @Expose
                                private var longitude: String?,
                                @SerializedName("EmpImage")
                                @Expose
                                private var empImage: String?,
                                @SerializedName("TokenNumber")
                                @Expose
                                private var tokenNumber: String?,
                                @SerializedName("AppType")
                                @Expose
                                private var appType: String?,
                                @SerializedName("ID")
                                @Expose
                                private var id: Int,
                                @SerializedName("LocationName")
                                @Expose
                                private var LocationName: String?)
