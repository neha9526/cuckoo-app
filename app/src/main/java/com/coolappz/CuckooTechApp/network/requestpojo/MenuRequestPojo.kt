package com.coolappz.CuckooTechApp.network.requestpojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MenuRequestPojo(@SerializedName("employeeCode")
                      @Expose
                      private var employeeCode: String?,
                      @SerializedName("companyCode")
                      @Expose
                      private var companyCode: String?) {
}