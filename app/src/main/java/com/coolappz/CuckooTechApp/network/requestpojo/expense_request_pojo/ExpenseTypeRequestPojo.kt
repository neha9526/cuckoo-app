package com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 19-07-2018.
 */
class ExpenseTypeRequestPojo(@SerializedName("AppType")
                             @Expose
                             internal val appType: String?,
                             @SerializedName("TokenId")
                             @Expose
                             internal val tokenId: String?,
                             @SerializedName("CompanyCode")
                             @Expose
                             internal val companyCode: String?,
                             @SerializedName("EmployeeCode")
                             @Expose
                             internal val employeeCode: String?) {

}