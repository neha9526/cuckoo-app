package com.coolappz.CuckooTechApp.network.apis

import com.coolappz.CuckooTechApp.network.requestpojo.*
import com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo.ExpenseTypeRequestPojo
import com.coolappz.CuckooTechApp.network.responsepojo.MenuResponse
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.BulkSyncResponse
import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.EmployeeAttendanceResponse
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.SaveExpResPojo
import com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.expense_type.ExpenseTypeResPojo
import com.coolappz.CuckooTechApp.network.responsepojo.loginpojo.EmployeeLoginResponse
import com.coolappz.CuckooTechApp.network.responsepojo.registrationpojo.EmployeeRegistrationResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import rx.Observable

/**
 * Created by Vikas Pawar on 05-12-2018.
 */

interface UserManagement {

    @POST("Account/MobileUserRegistration")
    fun employeeRegistration(@Body employeeRegistrationRequest: EmployeeRegistrationRequest):
            Observable<EmployeeRegistrationResponse>

    @POST("Account/MobileUserLogin")
    fun employeeLogin(@Body employeeLoginRequest: EmployeeLoginRequest): Observable<EmployeeLoginResponse>


    @POST("Account/AddMobileEmployeeSyncPunches")
    fun employeeBulkPunch(@Body bulkSyncRequestPojo: BulkSyncRequestPojo):
            Observable<BulkSyncResponse>

    @POST("Account/AddMobileEmployeePunches")
    fun employeePunching(@Body employeeAttendanceRequest: EmployeeAttendanceRequest):
            Observable<EmployeeAttendanceResponse>

    @POST("Expense/GetExpenseType")
    fun getExpenseTypeDetails(@Body expenseTypeRequestPojo: ExpenseTypeRequestPojo):
            Observable<ExpenseTypeResPojo>

    @Multipart
    @POST("Expense/SubmitExpenseClaim")
    fun submitExpenseClaim(@Part("expenseObject") expObject: RequestBody,
                           @Part files:ArrayList<MultipartBody.Part>) : Observable<SaveExpResPojo>

    @POST("Account/GetMenuList")
    fun getMenuDetails(@Body menuRequestPojo: MenuRequestPojo):Observable<MenuResponse>
}
