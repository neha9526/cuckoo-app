package com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by admin on 10-04-2018.
 */
class BulkPunchPojo
(@SerializedName("TransnactionDateTime")
 @Expose
 val transactionDateTime: String? = null,
 @SerializedName("Latitude")
 @Expose
 val latitude: Double? = null,
 @SerializedName("Longitude")
 @Expose
 val longitude: Double? = null,
 @SerializedName("EmpImage")
 @Expose
 val empImage: String? = null,
 @SerializedName("IsValid")
 @Expose
 var isValid: Int? = null,
 @SerializedName("Sync")
 @Expose
 var syncStatus: String? = null,
 @SerializedName("ID")
 @Expose
 var id: Int,
 @SerializedName("LocationName")
 @Expose
 private var LocationName: String?)

