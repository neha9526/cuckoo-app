package com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by admin on 10-03-2018.
 */
class ParentExpenseRequest(
        @SerializedName("CompanyCode")
        @Expose
        private var comapnyCode: String?,
        @SerializedName(  "AppType")
        @Expose
        private var appType: String?,
        @SerializedName("ECode")
        @Expose
        private var empCode: String?,
        @SerializedName("TokenId")
        @Expose
        private var tokenId: String?,
        @SerializedName("ExpAppId")
        @Expose
        private var expAppId: String?,
        @SerializedName("ClaimId")
        @Expose
        private var claimId: String,
        @SerializedName("ClaimFor")
        @Expose
        private var claimFor: String?,
        @SerializedName("ClaimCurrId")
        @Expose
        private var claimCurrId: String,
        @SerializedName("Amount")
        @Expose
        private var claimAmt: String?,
        @SerializedName("ChildEntries")
        @Expose
        private var childEntryArr: ArrayList<ChildExpenseRequest>) {


}