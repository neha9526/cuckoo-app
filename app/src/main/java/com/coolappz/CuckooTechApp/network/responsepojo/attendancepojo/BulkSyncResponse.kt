package com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 10-04-2018.
 */
class BulkSyncResponse(    @SerializedName("Response")
                           @Expose
                           internal val response: BulkAttendanceResponse?) {
}