package com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.expense_type

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 20-07-2018.
 */
class ExpenseResPojo (@SerializedName("IsSucceeded")
                      @Expose
                      var isSucceeded: Boolean? = null,
                      @SerializedName("ErrorMessage")
                      @Expose
                      var responseMessage: String? = null,
                      @SerializedName("Data")
                      @Expose
                      var dropdownData: List<ExpenseTypeData>? = null){
}