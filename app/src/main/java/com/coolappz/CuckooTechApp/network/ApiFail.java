package com.coolappz.CuckooTechApp.network;

import java.io.IOException;

import retrofit2.adapter.rxjava.HttpException;
import rx.functions.Action1;

/**
 * Created by Vikas Pawar on 05-12-2018.
 */

public abstract class ApiFail implements Action1<Throwable> {
    @Override
    public void call(Throwable e) {
        if (e instanceof HttpException) {
            HttpException http = (HttpException) e;
            HttpErrorResponse response = new HttpErrorResponse(http.code(),http.response());
            response.setError(http.message());
            httpStatus(response);
        } else if (e instanceof IOException) {
            noNetworkError();
        } else {
            unknownError(e);
        }
    }

    public abstract void httpStatus(HttpErrorResponse response);

    public abstract void noNetworkError();

    public abstract void unknownError(Throwable e);
}
