package com.coolappz.CuckooTechApp.network.responsepojo.registrationpojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EmployeeRegistrationResponse(
        @SerializedName("Response")
        @Expose
        val response: RegistrationResponse? = null
)