package com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 24-07-2018.
 */
class SaveExpRequestPojo( @SerializedName("Request")
                          @Expose
                          private var expRequest: ArrayList<ParentExpenseRequest>) {

}