package com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by admin on 10-03-2018.
 */
class EmployeeAttendanceResponse(    @SerializedName("Response")
                                     @Expose
                                     internal val response: SingleAttendanceResponse?) {


}