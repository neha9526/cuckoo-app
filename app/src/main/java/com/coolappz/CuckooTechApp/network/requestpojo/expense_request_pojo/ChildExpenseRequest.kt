package com.coolappz.CuckooTechApp.network.requestpojo.expense_request_pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 19-07-2018.
 */
class ChildExpenseRequest(  @SerializedName("ExpenseClaimId")
                            @Expose
                            private var expenseClaimId:String,
                            @SerializedName("ExpenseTypeId")
                            @Expose
                            private var expenseTypeId: String,
                            @SerializedName("Description")
                            @Expose
                            private var description: String?,
                            @SerializedName("FromDate")
                            @Expose
                            private var fromDate: String?,
                            @SerializedName("ToDate")
                            @Expose
                            private var toDate: String?,
                            @SerializedName("ExpCurrId")
                            @Expose
                            private var expCurrId: String,
                            @SerializedName("ExpAmount")
                            @Expose
                            private var expAmount: String?,
                            @SerializedName("ConversionRate")
                            @Expose
                            private var conversionRate: String?,
                            @SerializedName("ConvertedAmt")
                            @Expose
                            private var convertedAmt: String?,
                            @SerializedName("UploadedFile")
                            @Expose
                            private var uploadedFile: String?) {


}