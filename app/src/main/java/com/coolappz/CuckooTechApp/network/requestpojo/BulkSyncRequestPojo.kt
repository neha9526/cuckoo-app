package com.coolappz.CuckooTechApp.network.requestpojo

import com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo.BulkPunchPojo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 10-04-2018.
 */
class BulkSyncRequestPojo(@SerializedName("employeeCode")
                          @Expose
                          private var employeeCode: String?,
                          @SerializedName("companyCode")
                          @Expose
                          private var companyCode: String?,
                          @SerializedName("mobileNumber")
                          @Expose
                          private var mobileNumber: String?,
                          @SerializedName("macAddress")
                          @Expose
                          private var macAddres: String,
                          @SerializedName("appType")
                          @Expose
                          private var appType: String,
                          @SerializedName("tokenNumber")
                          @Expose
                          private var tokenNumber:String,
                          @SerializedName("bulkEmployeePunch")
                          @Expose
                          private var bulkEmployeePunch:List<BulkPunchPojo>)

{
}