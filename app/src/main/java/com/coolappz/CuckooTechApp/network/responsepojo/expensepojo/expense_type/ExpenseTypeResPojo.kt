package com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.expense_type

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 16-07-2018.
 */
class ExpenseTypeResPojo(
        @SerializedName("Response")
        @Expose
        val response: ExpenseResPojo? = null ) {
}