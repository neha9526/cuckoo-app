package com.coolappz.CuckooTechApp.network;

import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Created by Vikas Pawar on 05-12-2018.
 */

public interface BaseView<T> extends MvpView {
    void noInternet();

    void success(T response);

    void fail(String message);

    void showLoading();

    void hideLoading();

}
