package com.coolappz.CuckooTechApp.network.responsepojo.expensepojo.expense_type

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 20-07-2018.
 */
class ExpenseTypeData(
        @SerializedName("DropDownId")
        @Expose
        val expenseTypeId: Int = 0,
        @SerializedName("DropDownName")
        @Expose
        val expenseTypeName: String? = null
) {
}