package com.coolappz.CuckooTechApp.network.responsepojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 10-08-2018.
 */
class MenuPojo(@SerializedName("ModuleName")
               @Expose
               var moduleName: String? = null,
               @SerializedName("IsActive")
               @Expose
               var isActive: Int? = null) {

}