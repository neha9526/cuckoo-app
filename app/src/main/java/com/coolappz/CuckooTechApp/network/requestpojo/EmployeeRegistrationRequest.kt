package com.coolappz.CuckooTechApp.network.requestpojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EmployeeRegistrationRequest(
        @SerializedName("EmployeeCode")
        @Expose
        internal val employeeCode: String?,
        @SerializedName("MobileNumber")
        @Expose
        internal val mobileNumber: String?,
        @SerializedName("CompanyCode")
        @Expose
        internal val companyCode: String?,
        @SerializedName("MacAddress")
        @Expose
        internal val MacAddress: String?
)