package com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 28-04-2018.
 */
class BulkAttendanceResponse(
        @SerializedName("IsSucceeded")
        @Expose
        var isSucceeded: Boolean? = null,
        @SerializedName("ResponseMessage")
        @Expose
        var responseMessage: String? = null,
        @SerializedName("BulkPunches")
        @Expose
        var bulkEmployeePunch: List<BulkPunchPojo>? = null,
        @SerializedName("AppType")
        @Expose
        var appType: String? = null)