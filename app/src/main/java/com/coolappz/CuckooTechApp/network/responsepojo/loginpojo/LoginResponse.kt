package com.coolappz.CuckooTechApp.network.responsepojo.loginpojo

import com.coolappz.CuckooTechApp.network.responsepojo.MenuPojo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 28-04-2018.
 */
class LoginResponse( @SerializedName("IsSucceeded")
                     @Expose
                     var isSucceeded: Boolean? = null,
                     @SerializedName("ResponseMessage")
                     @Expose
                     var responseMessage: String? = null,
                     @SerializedName("TokenNumber")
                     @Expose
                     var token: String? = null,
                     @SerializedName("MenuList")
                     @Expose
                     var menuList: ArrayList<MenuPojo>? = null)