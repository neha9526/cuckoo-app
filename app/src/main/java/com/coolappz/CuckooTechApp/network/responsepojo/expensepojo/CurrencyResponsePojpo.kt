package com.coolappz.CuckooTechApp.network.responsepojo.expensepojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 16-07-2018.
 */
class CurrencyResponsePojpo(@SerializedName("ExpCurrencyId")
                            @Expose
                            val currencyId: String? = null,
                            @SerializedName("ExpCurrencyName")
                            @Expose
                            val currencyName: String? = null) {
}