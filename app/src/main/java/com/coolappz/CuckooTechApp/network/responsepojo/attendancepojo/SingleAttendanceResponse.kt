package com.coolappz.CuckooTechApp.network.responsepojo.attendancepojo

import com.coolappz.CuckooTechApp.network.responsepojo.MenuPojo
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 28-04-2018.
 */
class SingleAttendanceResponse(@SerializedName("IsSucceeded")
                               @Expose
                               var isSucceeded: Boolean? = null,
                               @SerializedName("ResponseMessage")
                               @Expose
                               var responseMessage: String? = null,
                               @SerializedName("IsValid")
                               @Expose
                               var isValid: Int? = null,
                               @SerializedName("Sync")
                               @Expose
                               var syncStatus: String? = null,
                               @SerializedName("ID")
                               @Expose
                               var id: Int? = null,
                               @SerializedName("AppType")
                               @Expose
                               var appType: String? = null)
