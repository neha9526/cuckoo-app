package com.coolappz.CuckooTechApp.network.responsepojo.expensepojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by admin on 24-07-2018.
 */
class SaveExpeResObj(@SerializedName("IsSuccessed")
                     @Expose
                     var isSuccessed: Boolean? = null,
                     @SerializedName("ErrorMessage")
                     @Expose
                     var errorMessage: String? = null,
                     @SerializedName("IsValid")
                     @Expose
                     var isValid: Int? = null,
                     @SerializedName("ExpAppId")
                     @Expose
                     var expAppId: String? = null,
                     @SerializedName("ClaimFor")
                     @Expose
                     var claimFor: String? = null,
                     @SerializedName("ClaimCurrId")
                     @Expose
                     var claimCurrId: Int? = null,
                     @SerializedName("Amount")
                     @Expose
                     var amount: Double? = null) {
}
