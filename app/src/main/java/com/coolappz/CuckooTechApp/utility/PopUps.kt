package com.coolappz.CuckooTechApp.utility

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.app.ActivityCompat.finishAffinity
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import com.coolappz.CuckooTechApp.R
import android.content.ContentValues.TAG
import com.google.android.gms.location.LocationSettingsStatusCodes
import android.content.IntentSender
import android.util.Log
import com.google.android.gms.location.LocationSettingsResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback


/**
 * Created by admin on 04-04-2018.
 */
class PopUps(context: Context) {

   // val dateBuilder = AlertDialog.Builder(context)
    var alertDate:AlertDialog? = null

    fun autoTimePopUp(activity: Activity) {
        val inflater = LayoutInflater.from(activity)
        val dialogView = inflater.inflate(R.layout.auto_date_time_popup, null)
        val dateBuilder = AlertDialog.Builder(activity)
        val alertTitle = dialogView.findViewById(R.id.alertTitle) as TextView
        val alertText = dialogView.findViewById(R.id.alertMsg) as TextView
        val btnAllow = dialogView.findViewById(R.id.btnAlertAllow) as  Button
        val btnCancel = dialogView.findViewById(R.id.btnAlertCancel) as Button

        dateBuilder.setView(dialogView)
        dateBuilder.setCancelable(false)
        alertTitle.setText(R.string.auto_date_time)
        alertText.setText(R.string.auto_date_time_msg)
        if (alertDate == null) {
            alertDate = dateBuilder.create()
        }
        if (!alertDate!!.isShowing)
            alertDate!!.show()

        btnCancel.setOnClickListener()
        {
            alertDate!!.dismiss()
            finishAffinity(activity)
        }

        btnAllow.setOnClickListener()
        {
            alertDate!!.dismiss()
            val intent = Intent(android.provider.Settings.ACTION_DATE_SETTINGS)
            activity.startActivity(intent)
        }
    }

    fun enableSimCard(activity: Activity) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(activity.getString(R.string.no_sim_available))
        builder.setMessage(activity.getString(R.string.insert_sim_msg))
        builder.setCancelable(false)
        builder.setPositiveButton("Ok", { dialogInterface, i
            ->
            dialogInterface.dismiss()
        })
        val alert = builder.create()
        if (!alert.isShowing)
            alert.show()
    }

    fun createGpsCameraAlert(context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(context.getString(R.string.camera_gps_permission))
        builder.setMessage(context.getString(R.string.enable_camera_Gps_msg))
        builder.setCancelable(false)
        builder.setNegativeButton(context.getString(R.string.cancel), { dialogInterface, i
            ->
            dialogInterface.dismiss()
        })
        builder.setPositiveButton(context.getString(R.string.allow), { dialogInterface, i
            ->
            dialogInterface.dismiss()
            val intent = Intent()
            intent.action = android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS;
            intent.addCategory(Intent.CATEGORY_DEFAULT)
            intent.data = Uri.parse("package:" + context.packageName)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
            context.startActivity(intent)

        })
        val alert = builder.create()
        if (!alert.isShowing)
            alert.show()
    }

  fun displayLocationSettingsRequest(activity: Activity) {
        val REQUEST_CHECK_SETTINGS:Int=100
        val googleApiClient = GoogleApiClient.Builder(activity)
                .addApi(LocationServices.API).build()
        googleApiClient.connect()

        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 0
        locationRequest.fastestInterval = (0).toLong()

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)

        val result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback(object : ResultCallback<LocationSettingsResult> {
            override fun onResult(result: LocationSettingsResult) {
                val status = result.status
                when (status.statusCode) {
                    LocationSettingsStatusCodes.SUCCESS -> Log.i(TAG, "All location settings are satisfied.")
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ")

                        try {
                            status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS)
                        } catch (e: IntentSender.SendIntentException) {
                            Log.i(TAG, "PendingIntent unable to execute request.")
                        }

                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.")
                }
            }
        })
    }

}

