package com.coolappz.CuckooTechApp.utility

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication

/**
 * Created by admin on 12-02-2018.
 */
public class MyApp: MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
   TypefaceUtil.overrideFont(applicationContext,"SERIF","fonts/lato-regular.ttf")
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(base)
    }
}