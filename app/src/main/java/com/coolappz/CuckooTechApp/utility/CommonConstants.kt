package com.coolappz.CuckooTechApp.utility

import android.content.Context
import android.os.Build
import android.provider.Settings
import android.telephony.TelephonyManager



/**
 * Created by admin on 03-04-2018.
 */
class CommonConstants {
    object utility {
        const val APP_TYPE_1: String = "1"
        const val APP_TYPE_2: String = "2"
        const val APP_TYPE_3: String = "3"
        const val APP_TYPE_4: String = "4"

        public fun isTimeAutomatic(context: Context): Boolean {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                Settings.Global.getInt(context.contentResolver, Settings.Global.AUTO_TIME, 0) == 1
            } else {
                Settings.System.getInt(context.contentResolver, Settings.System.AUTO_TIME, 0) == 1
            }
        }

            fun isAutomaticTimeZoneEnable(context: Context): Boolean {
                return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    Settings.Global.getInt(context.contentResolver, Settings.Global.AUTO_TIME_ZONE, 0) == 1
                } else {
                    Settings.System.getInt(context.contentResolver, Settings.System.AUTO_TIME_ZONE, 0) == 1
                }
            }

            fun isSimSupport(context: Context): Boolean {
                val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager  //gets the current TelephonyManager
                return tm.simState != TelephonyManager.SIM_STATE_ABSENT

            }



    }

}