package com.coolappz.CuckooTechApp.utility

import android.content.Context
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.Uri
import java.net.NetworkInterface
import java.util.*
import android.content.pm.PackageManager
import android.os.Environment
import android.provider.MediaStore
import android.provider.DocumentsContract
import android.content.ContentUris
import android.database.Cursor
import android.graphics.Color
import android.graphics.Matrix
import android.media.ExifInterface
import android.os.Build
import android.text.InputFilter
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.webkit.MimeTypeMap
import droidninja.filepicker.utils.Orientation
import java.io.File
import java.net.URI
import java.util.regex.Pattern


/**
 * Created by admin on 06-04-2018.
 */
class CuckooUtil {
    object Util {

        val currentTimeString: String = DateHelper.getDateTime(System.currentTimeMillis())
        fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

        fun getMimeType(url: String): String {
            val extension = MimeTypeMap.getFileExtensionFromUrl(url)
            return extension
        }

        fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
            var width = image.width
            var height = image.height

            val bitmapRatio = width.toFloat() / height.toFloat()
            if (bitmapRatio > 1) {
                width = maxSize
                height = (width / bitmapRatio).toInt()
            } else {
                height = maxSize
                width = (height * bitmapRatio).toInt()
            }
            return Bitmap.createScaledBitmap(image, width, height, true)
        }

        fun getMacAddress(): String {
            try {
                val all = Collections.list(NetworkInterface.getNetworkInterfaces())
                for (nif in all) {
                    if (!nif.name.equals("wlan0", true)) continue

                    val macBytes = nif.hardwareAddress ?: return ""

                    val res1 = StringBuilder()
                    for (b in macBytes) {
                        res1.append(String.format("%02X:", b))
                    }

                    if (res1.isNotEmpty()) {
                        res1.deleteCharAt(res1.length - 1)
                    }
                    return res1.toString()
                }
            } catch (ex: Exception) {
            }

            return "02:00:00:00:00:00"
        }

        fun getPathFromUri(context: Context, uri: Uri): String? {

            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

            // DocumentProvider
            if (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        isKitKat && DocumentsContract.isDocumentUri(context, uri)
                    } else {
                        TODO("VERSION.SDK_INT < KITKAT")
                    }) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    val docId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        DocumentsContract.getDocumentId(uri)
                    } else {
                        TODO("VERSION.SDK_INT < KITKAT")
                    }
                    val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]

                    if ("primary".equals(type, ignoreCase = true)) {
                        return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                    }

                    // TODO handle non-primary volumes
                } else if (isDownloadsDocument(uri)) {

                    val id = DocumentsContract.getDocumentId(uri)
                    val contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)!!)

                    return getDataColumn(context, contentUri, null, null)
                } else if (isMediaDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]

                    var contentUri: Uri? = null
                    if ("image" == type) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    } else if ("video" == type) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    } else if ("audio" == type) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }

                    val selection = "_id=?"
                    val selectionArgs = arrayOf(split[1])

                    return getDataColumn(context, contentUri, selection, selectionArgs)
                }// MediaProvider
                // DownloadsProvider
            } else if ("content".equals(uri.scheme, ignoreCase = true)) {

                // Return the remote address
                return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context, uri, null, null)

            } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                return uri.path
            }// File
            // MediaStore (and general)

            return null
        }

        fun decimalInputFilter(): InputFilter {
            val inputFilter = InputFilter { source, start, end, dest, dstart, dend ->
                val matches = Pattern.compile("^\\d{0,5}([\\.,](\\d{0,2})?)?$")

                val newString = (dest.toString().substring(0, dstart) + source.toString().substring(start, end)
                        + dest.toString().substring(dend, dest.toString().length))

                val matcher = matches.matcher(newString)
                return@InputFilter if (!matcher.matches()) {
                    ""
                } else null
            }
            return inputFilter
        }

        fun getDataColumn(context: Context, uri: Uri?, selection: String?,
                          selectionArgs: Array<String>?): String? {

            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(column)

            try {
                cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
                if (cursor != null && cursor.moveToFirst()) {
                    val index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(index)
                }
            } finally {
                if (cursor != null)
                    cursor.close()
            }
            return null
        }


        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is ExternalStorageProvider.
         */
        fun isExternalStorageDocument(uri: Uri): Boolean {
            return "com.android.externalstorage.documents" == uri.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is DownloadsProvider.
         */
        fun isDownloadsDocument(uri: Uri): Boolean {
            return "com.android.providers.downloads.documents" == uri.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is MediaProvider.
         */
        fun isMediaDocument(uri: Uri): Boolean {
            return "com.android.providers.media.documents" == uri.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is Google Photos.
         */
        fun isGooglePhotosUri(uri: Uri): Boolean {
            return "com.google.android.apps.photos.content" == uri.authority
        }

        fun getVersionName(context: Context): String {
            return try {
                context.packageManager.getPackageInfo(context.packageName, 0).versionName
            } catch (e: PackageManager.NameNotFoundException) {
                "0.0"
            }

        }

        fun spannableTextView(text: String): Spannable {
            val startIndex: Int = text.indexOf("*")
            val lastIndex: Int = text.lastIndexOf("*")
            val wordToSpan: Spannable = SpannableString(text);
            wordToSpan.setSpan(ForegroundColorSpan(Color.RED), startIndex, lastIndex + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return wordToSpan
        }

        fun getFile(): File {
            val imageFileName = getFileName()
            return File(Environment.getExternalStorageDirectory(), "$imageFileName.jpg")
        }

        fun getFileName(): String {
            val currentTime = Date(System.currentTimeMillis())
            return "img_${DateHelper.SAVE_FILE_FORMAT_FULL_DATE.format(currentTime)}"
        }

        fun getRotation(resultBitmap: Bitmap, originalFilePath: String): Bitmap {
            val exifInterface = ExifInterface(originalFilePath)
            val rotation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
            val matrix = Matrix()
            matrix.postRotate(returnRotation(rotation))
            val scaledBitmap = Bitmap.createScaledBitmap(resultBitmap, Constants.Constants.MAX_WIDTH, Constants.Constants.MAX_HEIGHT, true)
            val rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)
            return rotatedBitmap
        }

        fun returnRotation(orientation: Int): Float {
            when (orientation) {
                3 -> return 180f
                6 -> return 90f
                8 -> return -90f
            }
            return 0f
        }
    }
}