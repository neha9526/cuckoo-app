package com.coolappz.CuckooTechApp.utility

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.content.res.ResourcesCompat
import android.util.TypedValue
import android.view.Gravity
import android.widget.DatePicker
import android.widget.RelativeLayout
import android.widget.TextView
import com.coolappz.CuckooTechApp.R
import java.util.*

/**
 * Created by admin on 31-01-2018.
 */
@SuppressLint("ValidFragment")
class DateDialogFragment(
        private var strTitle: String,
        private val year: Int,
        private val month: Int,
        private val day: Int,
        private val maxDate: Calendar?,
        private val minDate: Calendar?,
        private var interfacOnDateClick: InterfaceOnDateClick) : DialogFragment(), DatePickerDialog.OnDateSetListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null) strTitle = savedInstanceState.getString("dateFragmentTitle")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val datePickerDialog = DatePickerDialog(getActivity()!!, R.style.Theming, this, year, month, day)

        if (maxDate != null) {
            with(maxDate) {
                set(Calendar.HOUR_OF_DAY, 23)
                set(Calendar.MINUTE, 59)
                set(Calendar.SECOND, 59)
            }
            datePickerDialog.datePicker.maxDate = maxDate.timeInMillis
        }

        if (minDate != null) {
            datePickerDialog.datePicker.minDate = minDate.getTimeInMillis()
        }

        // Create a TextView programmatically.
        val tv = TextView(activity)

        // Create a TextView programmatically
        val lp = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, // Width of TextView
                RelativeLayout.LayoutParams.WRAP_CONTENT) // Height of TextView
        tv.layoutParams = lp
        tv.setPadding(10, 10, 10, 10)
        tv.gravity = Gravity.CENTER
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18f)
        tv.text = strTitle
        tv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null))
        tv.setBackgroundColor(ResourcesCompat.getColor(getResources(), android.R.color.white, null))
        datePickerDialog.setCustomTitle(tv)

        return datePickerDialog
    }

    override fun onDateSet(view: DatePicker, yy: Int, mm: Int, dd: Int) {
        interfacOnDateClick.onDateClick(dd, mm, yy)
    }
}




