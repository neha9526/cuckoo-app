package com.coolappz.CuckooTechApp.utility

import android.content.Context
import android.os.Environment
import com.coolappz.CuckooTechApp.R
import java.io.*


/**
 * Created by admin on 17-08-2018.
 */
object FileManager {
    fun storeData(context: Context, docPath: String): String {
        val receivedFile = File(docPath)
        val filePath = Environment.getExternalStorageDirectory().absolutePath + context.getString(R.string.cuckoo_folder)
        val docFolder = File(filePath)
        if (!docFolder.exists()) {
            docFolder.mkdir()
        }
        val fileName = File(docFolder, docPath.substring(docPath.lastIndexOf("/")))
        if (!fileName.exists()) {
            fileName.createNewFile()
            receivedFile.copyTo(fileName, true)
        }
        return fileName.path
    }

    fun storeImage(context: Context, imagePath: String): String {
        val receivedFile = File(imagePath)
        val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), context.getString(R.string.cuckoo_folder))
        val imageFolder = File(file.absolutePath)
        if (!imageFolder.exists()) {
            imageFolder.mkdir()
        }
        val fileName = File(imageFolder, imagePath.substring(imagePath.lastIndexOf("/")))
        if (!fileName.exists()) {
            fileName.createNewFile()
            receivedFile.copyTo(fileName, true)
        }
        receivedFile.delete()// Delete original file
        return fileName.path
    }
}