package com.coolappz.CuckooTechApp.utility

import android.content.Context
import com.coolappz.CuckooTechApp.R

/**
 * Created by admin on 02-02-2018.
 */
class Preferences {


    companion object {
        private val AUTO_LOGIN = "authentication"
        private val IS_REGISTERED = "is_Registered"
        private val USER_TOKEN = "user_token"
        private val REGISTER_TOKEN = "register_token"
        private val REGISTER_USER_TOKEN = "register_user_token"
        const val EMOJI_NAME = "emojiName"
        private const  val EMOJI_VALUE="emojiValue"
        const val EMPLOYEE_CODE = "employee_code"
        private const val AUTO_EMPCODE = "share_empcode"
        private const val FIREBASE_SESSION="fireBase_session"
        private const val FIREBASE_TOKEN="fireBase_token"
        private const val CLAIM_SESSION="claim_session"
        private const val CLAIM_TOKEN="claim_token"
        private const val CHANGE_FLAG_SESSION="change_flag_session"
        private const val CHANGE_FLAG="change_flag"
        const val APP_UPDATE_SESSION ="app_update"
        const val APP_VERSION ="app_version"

      /*  fun setFirebaseToken(context: Context, firebaseToken: String) {
            val sharedPreferences = context.getSharedPreferences(FIREBASE_SESSION, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(FIREBASE_TOKEN, firebaseToken)
            editor.apply()
        }
        fun getFirebaseToken(context: Context): String {
            val sharedPreferences = context.getSharedPreferences(FIREBASE_SESSION, Context.MODE_PRIVATE)
            return sharedPreferences.getString(FIREBASE_TOKEN, "")
        }*/


        fun setEmojiName(context: Context, emojiId: Int) {
            val sharedPreferences = context.getSharedPreferences(EMOJI_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putInt(EMOJI_VALUE, emojiId)
            editor.apply()
        }

        fun getEmojiName(context: Context): Int {
            val sharedPreferences = context.getSharedPreferences(EMOJI_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getInt(EMOJI_VALUE, R.drawable.ic_very_happy)
        }
//shared preferences for expense module
         fun setClaimToken(context: Context,claimToken:String)
         {
             val sharedPreferences = context.getSharedPreferences(CLAIM_SESSION, Context.MODE_PRIVATE)
             val editor = sharedPreferences.edit()
             editor.putString(CLAIM_TOKEN, claimToken)
             editor.apply()
         }

        fun getClaimToken(context: Context):String
        {
            val sharedPreferences = context.getSharedPreferences(CLAIM_SESSION, Context.MODE_PRIVATE)
            return sharedPreferences.getString(CLAIM_TOKEN,"")
        }

        fun setChangeFlag(context: Context,changeFlag:Boolean)
        {
            val sharedPreferences = context.getSharedPreferences(CHANGE_FLAG_SESSION, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putBoolean(CHANGE_FLAG, changeFlag)
            editor.apply()
        }

        fun getChangeFlag(context: Context):Boolean
        {
            val sharedPreferences = context.getSharedPreferences(CHANGE_FLAG_SESSION, Context.MODE_PRIVATE)
            return sharedPreferences.getBoolean(CHANGE_FLAG,false)
        }


//shared preferences for auto login
        fun getAutoLogin(context: Context): Boolean {
            val sharedPreferences = context.getSharedPreferences(AUTO_LOGIN, Context.MODE_PRIVATE)
            return sharedPreferences.getBoolean(IS_REGISTERED, false)
        }

        fun setAutoLogin(context: Context, loginFlag: Boolean) {
            val sharedPreferences = context.getSharedPreferences(AUTO_LOGIN, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putBoolean(IS_REGISTERED, loginFlag)
            editor.apply()
        }

        fun setEmployeeCode(context: Context, employeeCode: String?) {
            val sharedPreferences = context.getSharedPreferences(AUTO_EMPCODE, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(EMPLOYEE_CODE, employeeCode)
            editor.apply()

        }

        fun getEmployeeCode(context: Context): String? {
            val sharedPreferences = context.getSharedPreferences(AUTO_EMPCODE, Context.MODE_PRIVATE)
            return sharedPreferences.getString(EMPLOYEE_CODE, "")
        }

      //App update session

        fun setAppVersion(context: Context,appVersion:String)
        {
            val sharedPreferences = context.getSharedPreferences(APP_UPDATE_SESSION, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(APP_VERSION, appVersion)
            editor.apply()
        }

        fun getAppVersion(context: Context):String
        {
            val sharedPreferences = context.getSharedPreferences(APP_UPDATE_SESSION, Context.MODE_PRIVATE)
            return sharedPreferences.getString(APP_VERSION, "")
        }

    }


}
