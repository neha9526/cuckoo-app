package com.coolappz.CuckooTechApp.utility

/**
 * Created by admin on 29-03-2018.
 */
class Constants {
    object Constants {
        const val SUCCESS_RESULT = 0
        const val FAILURE_RESULT = 1
        private const val PACKAGE_NAME = "package com.cuckoo.attendanceapp.map"
        const val RECEIVER = "$PACKAGE_NAME.RECEIVER"
        const val RESULT_DATA_KEY = "$PACKAGE_NAME.RESULT_DATA_KEY"
        const val LOCATION_DATA_EXTRA = "$PACKAGE_NAME.LOCATION_DATA_EXTRA"
        const val LOCATION_LONGITUDE = "$PACKAGE_NAME.LOCATION_LONGITUDE"
        const val LOCATION_LATTITUDE = "$PACKAGE_NAME.LOCATION_LATTITUDE"
        const val AM_MIN = "00:00:00"
        const val PM_MAX = "23:59:59"
        const val MAX_WIDTH = 900
        const val MAX_HEIGHT = 1200
    }
}