package com.coolappz.CuckooTechApp.utility

/**
 * Created by admin on 31-01-2018.
 */
interface InterfaceOnDateClick {
    fun onDateClick(date: Int, month: Int, year: Int)
}