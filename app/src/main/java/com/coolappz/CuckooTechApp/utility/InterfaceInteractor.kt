package com.coolappz.CuckooTechApp.utility

/**
 * Created by admin on 31-01-2018.
 */
interface InterfaceInteractor<T> {
    fun successApi(response:T)
    fun failApi(message:String)
    fun noInternet()
    fun tokenExpire()
}