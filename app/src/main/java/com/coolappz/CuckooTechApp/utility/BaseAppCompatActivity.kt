package com.coolappz.CuckooTechApp.utility

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatTextView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.expense_database.parent_expense.ParentExpenseDetails
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import kotlinx.android.synthetic.main.activity_base_app_compat.*

abstract class BaseAppCompatActivity<V : MvpView, P : MvpPresenter<V>> : MvpActivity<V, P>() {
    abstract val layoutIdForInjection: Int

    private lateinit var subTitle: AppCompatTextView
    private lateinit var popUps: PopUps
    val mMonth = DateHelper.getMonth(DateHelper.todayCalender.timeInMillis).toInt() - 1
    val mYear = DateHelper.getYear(DateHelper.todayCalender.timeInMillis).toInt()
    val mDay = DateHelper.getDay(DateHelper.todayCalender.timeInMillis).toInt()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_app_compat)
        setSupportActionBar(baseToolbar)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        screenTitle.text = getString(R.string.cuckoo_title)


        val databaseHandler=DatabaseHandler(this)
        val parentDetail:ParentExpenseDetails? = databaseHandler.getSingleParentDetail(Preferences.getClaimToken(this))
        popUps=PopUps(this)
        syncImg.visibility= View.GONE


        val lytId = layoutIdForInjection
        if (lytId != -1) {
            LayoutInflater.from(this).inflate(lytId, container)
        }
        with(supportActionBar!!) {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            baseToolbar.setNavigationOnClickListener {
                if(screenSubTitle.text.toString().contentEquals(getString(R.string.add_expense_claim))
                        && Preferences.getChangeFlag(applicationContext) )
                {
                    val inflater = LayoutInflater.from(this.themedContext)
                    val dialogView = inflater.inflate(R.layout.auto_date_time_popup, null)
                    val dateBuilder = AlertDialog.Builder(this.themedContext)
                    val alertTitle = dialogView.findViewById(R.id.alertTitle) as TextView
                    val alertText = dialogView.findViewById(R.id.alertMsg) as TextView
                    val btnAllow = dialogView.findViewById(R.id.btnAlertAllow) as Button
                    val btnCancel = dialogView.findViewById(R.id.btnAlertCancel) as Button
                    dateBuilder.setView(dialogView)
                    alertTitle.visibility=View.GONE
                    alertText.gravity= Gravity.CENTER
                    btnAllow.text = getString(R.string.ok)
                    alertText.text = getString(R.string.save_changes_alert)
                    val alertDialog = dateBuilder.create()
                    if (!alertDialog!!.isShowing)
                        alertDialog.show()

                    btnCancel.setOnClickListener()
                    {
                        alertDialog.dismiss()
                    }

                    btnAllow.setOnClickListener()
                    {
                        if(parentDetail!=null)
                        {
                            databaseHandler.deleteChildExpense(Preferences.getClaimToken(applicationContext))
                        }
                        Preferences.setChangeFlag(applicationContext,false)
                        Preferences.setClaimToken(applicationContext,"")
                        finish()
                    }
                }
                else{
                  finish()
                }
            }

        }

    }


    fun showProgress() {
        if (progressBar.visibility != View.VISIBLE) {
            container.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        }
    }

    fun hideProgress() {
        if (progressBar.visibility != View.GONE) {
            container.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
    }
    fun edittextLimiter(editText: EditText){
        editText.maxLines=2
    }



}
