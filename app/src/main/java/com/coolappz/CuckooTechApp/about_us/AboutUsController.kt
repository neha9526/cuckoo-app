package com.coolappz.CuckooTechApp.about_us

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller
import com.coolappz.CuckooTechApp.R

/**
 * Created by Vikas Pawar on 22-01-2018.
 */

class AboutUsController : Controller() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val rootView: View = inflater.inflate(R.layout.controller_about_us, container, false)
        return rootView
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
    }
}