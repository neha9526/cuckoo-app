package com.coolappz.CuckooTechApp.about_us

import android.os.Bundle
import com.bluelinelabs.conductor.Router
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.utility.BaseAppCompatActivity
import com.coolappz.CuckooTechApp.utility.CuckooUtil.Util.getVersionName
import com.coolappz.CuckooTechApp.utility.DefaultView
import kotlinx.android.synthetic.main.activity_base_app_compat.*
import kotlinx.android.synthetic.main.controller_about_us.*

class AboutUSActivity : BaseAppCompatActivity<DefaultView, AboutUsPresenterImpl>(), DefaultView {
    override val layoutIdForInjection: Int = R.layout.controller_about_us

    override fun createPresenter(): AboutUsPresenterImpl {
        presenter = AboutUsPresenterImpl(this)
        return presenter
    }


    var routerAboutUs: Router? = null
    private lateinit var aboutUsController: AboutUsController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         versionName.text=getString(R.string.version_name)+" "+ getVersionName(this)
         screenSubTitle.text = getString(R.string.about_us)
          aboutUsController = AboutUsController()
    }




}
