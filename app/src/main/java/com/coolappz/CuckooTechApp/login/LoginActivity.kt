package com.coolappz.CuckooTechApp.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.about_us.AboutUSActivity
import com.coolappz.CuckooTechApp.cryptographic.TrippleDe
import com.coolappz.CuckooTechApp.dashboard.DashboardDrawerActivity
import com.coolappz.CuckooTechApp.data_base.DatabaseHandler
import com.coolappz.CuckooTechApp.data_base.loginDatabase.LoginDetails
import com.coolappz.CuckooTechApp.data_base.menu_master.MenuEntity
import com.coolappz.CuckooTechApp.faq.FaqActivity
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeLoginRequest
import com.coolappz.CuckooTechApp.network.responsepojo.loginpojo.EmployeeLoginResponse
import com.coolappz.CuckooTechApp.network.responsepojo.loginpojo.LoginResponse
import com.coolappz.CuckooTechApp.utility.CuckooUtil
import com.coolappz.CuckooTechApp.utility.CuckooUtil.Util.currentTimeString
import com.coolappz.CuckooTechApp.utility.Preferences
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : MvpActivity<LoginView<LoginResponse>, LoginPresenterImpl>(), LoginView<LoginResponse> {
    private val databaseHandler = DatabaseHandler(this)

    private lateinit var loginDetails: LoginDetails
    private var employeeCode: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        loginDetails = databaseHandler.fetchFromDatabase()!!
        employeeCode = Preferences.getEmployeeCode(this)
        etEmpcode.setText(employeeCode)
        val encryption = TrippleDe()

        btnLogin.setOnClickListener {
            if (etEmpcode.text.isNullOrEmpty()) {
                etEmpcode.error = getString(R.string.validate_empcode)
                return@setOnClickListener
            }
            if (etPassword.text.isNullOrEmpty()) {
                etPassword.error = getString(R.string.validate_password)
                return@setOnClickListener
            }
            if (!CuckooUtil.Util.isNetworkAvailable(this)) {
                Toast.makeText(this, getString(R.string.require_net_connection), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else {
                loginProgressBar.visibility = View.VISIBLE
                val encryptedPassword = encryption._encrypt(etPassword.text.toString())
                val employeeLoginRequest = EmployeeLoginRequest(etEmpcode.text.toString(),
                        encryptedPassword, loginDetails.companyCode,
                        loginDetails.macAddress, loginDetails.mobileNo)
                presenter.onLogin(this, employeeLoginRequest)
            }
        }

        aboutTab.setOnClickListener()
        {
            val intent = Intent(this, AboutUSActivity::class.java)
            startActivity(intent)

        }
        faqTab.setOnClickListener()
        {
            val intent = Intent(this, FaqActivity::class.java)
            startActivity(intent)
        }


    }

    override fun onDestroy() {
        presenter.onDestroyView()
        super.onDestroy()

    }

    override fun createPresenter(): LoginPresenterImpl {
        presenter = LoginPresenterImpl(this)
        return presenter
    }

    override fun noInternet() {
        Toast.makeText(this, getString(R.string.poor_net_connection), Toast.LENGTH_SHORT).show()
    }


    override fun fail(message: String?) {

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        loginProgressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loginProgressBar.visibility = View.GONE
    }

    private fun databaseOperations(response: LoginResponse) {
        databaseHandler.updateTokenTime(response.token!!, currentTimeString, loginDetails.employeeCode!!)
        if (response.menuList!!.isNotEmpty()) {
            for (menu in response.menuList!!) {
                if (databaseHandler.fetchMenus().contains(menu.moduleName)) {
                    databaseHandler.updateMenuFlag(menu.moduleName!!, menu.isActive!!)
                } else {
                    databaseHandler.insertMenuDetails(MenuEntity(menu.moduleName!!, menu.isActive!!))
                }

            }
        }

    }

    override fun success(response: LoginResponse?) {
        loginProgressBar.visibility = View.GONE
        Toast.makeText(this, response!!.responseMessage, Toast.LENGTH_SHORT).show()
        databaseOperations(response)
        val intent = Intent(this, DashboardDrawerActivity::class.java)
        startActivity(intent)
        finish()
    }


}