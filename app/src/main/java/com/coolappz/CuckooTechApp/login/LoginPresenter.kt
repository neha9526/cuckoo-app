package com.coolappz.CuckooTechApp.login

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeLoginRequest

/**
 * Created by admin on 03-02-2018.
 */
interface LoginPresenter {
    fun onLogin(context: Context, employeeLoginRequest: EmployeeLoginRequest)
    fun onDestroyView()
}