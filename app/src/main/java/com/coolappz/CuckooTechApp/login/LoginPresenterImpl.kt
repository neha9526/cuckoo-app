package com.coolappz.CuckooTechApp.login

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeLoginRequest
import com.coolappz.CuckooTechApp.network.responsepojo.loginpojo.EmployeeLoginResponse
import com.coolappz.CuckooTechApp.network.responsepojo.loginpojo.LoginResponse
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter

/**
 * Created by admin on 03-02-2018.
 */
class LoginPresenterImpl(var view:LoginView<LoginResponse>?):MvpBasePresenter<LoginView<LoginResponse>>(),
        LoginPresenter,InterfaceInteractor<LoginResponse>{
    override fun successApi(response: LoginResponse) {
        if(view!=null)
        {
            view?.hideLoading()
            view?.success(response)
        }

    }

    private var loginInteractor= LoginInteractorImpl()


    override fun failApi(message: String) {
        if(view!=null)
        {
            view?.hideLoading()
            view?.fail(message)
        }
    }

    override fun noInternet() {
        if(view!=null)
        {
            view?.hideLoading()
            view?.noInternet()
        }
    }

    override fun tokenExpire() {

        if(view!=null)
        {
            view?.hideLoading()
        }
    }
    override fun onLogin(context: Context, employeeLoginRequest: EmployeeLoginRequest) {
        if(view!=null)
        {
            view?.showLoading()
            loginInteractor.onLogin(context,employeeLoginRequest, this)
        }
    }



    override fun onDestroyView() {
       view=null

    }
}