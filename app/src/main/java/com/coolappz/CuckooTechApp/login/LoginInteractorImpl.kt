package com.coolappz.CuckooTechApp.login

import android.content.Context
import com.coolappz.CuckooTechApp.R
import com.coolappz.CuckooTechApp.network.Api
import com.coolappz.CuckooTechApp.network.ApiFail
import com.coolappz.CuckooTechApp.network.ApiSuccess
import com.coolappz.CuckooTechApp.network.HttpErrorResponse
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeLoginRequest
import com.coolappz.CuckooTechApp.network.responsepojo.loginpojo.EmployeeLoginResponse
import com.coolappz.CuckooTechApp.network.responsepojo.loginpojo.LoginResponse
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by admin on 03-02-2018.
 */
class LoginInteractorImpl:LoginInteractor {
    override fun onLogin(context: Context, employeeLoginRequest: EmployeeLoginRequest,
                         interfaceInteractor: InterfaceInteractor<LoginResponse>) {

        Api.userManagement().employeeLogin(employeeLoginRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiSuccess<EmployeeLoginResponse>() {
                    override fun call(response: EmployeeLoginResponse) {
                        when {
                            response.response!!.isSucceeded!! -> {
                                interfaceInteractor.successApi(response.response)
                            }
                            else -> {
                                interfaceInteractor.failApi(response.response.responseMessage!!)
                            }
                        }
                    }
                }, object : ApiFail() {
                        override fun noNetworkError() {
                            interfaceInteractor.noInternet()
                        }

                        override fun unknownError(e: Throwable) {
                            interfaceInteractor.failApi(context.getString(R.string.unknown_error_msg))
                        }

                        override fun httpStatus(response: HttpErrorResponse) {
                            interfaceInteractor.failApi(context.getString(R.string.http_status_error_msg))
                        }

                    })

                }
}