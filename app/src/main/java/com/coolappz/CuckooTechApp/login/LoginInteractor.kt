package com.coolappz.CuckooTechApp.login

import android.content.Context
import com.coolappz.CuckooTechApp.network.requestpojo.EmployeeLoginRequest
import com.coolappz.CuckooTechApp.network.responsepojo.loginpojo.EmployeeLoginResponse
import com.coolappz.CuckooTechApp.network.responsepojo.loginpojo.LoginResponse
import com.coolappz.CuckooTechApp.utility.InterfaceInteractor

/**
 * Created by admin on 03-02-2018.
 */
interface LoginInteractor {
    fun onLogin(context: Context,employeeLoginRequest: EmployeeLoginRequest,
                interfaceInteractor: InterfaceInteractor<LoginResponse>)
}